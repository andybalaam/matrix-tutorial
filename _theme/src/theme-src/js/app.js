import "../css/styles.css";
import "@fontsource/inter/latin-200.css";
import "@fontsource/inter/latin-400.css";
import "@fontsource/inter/latin-700.css";
import "@fontsource/fira-mono/latin-400.css";

import { Application } from "@hotwired/stimulus";
import { definitionsFromContext } from "@hotwired/stimulus-webpack-helpers";

const app = Application.start();
const ctx = require.context("./controllers", true, /.js$/);

app.load(definitionsFromContext(ctx));
