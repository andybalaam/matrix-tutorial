const colors = require("tailwindcss/colors");

module.exports = {
  corePlugins: {
    animation: false,
  },
  content: ["../sphinxawesome_theme/*.html", "./js/**/*.js"],
  safelist: ["lead", "rubric", "centered"],
  theme: {
    fontFamily: {
      sans: ["Inter", "sans-serif"],
      mono: ["Fira Mono", "monospace"],
    },
    fontSize: {
      xs: [".75rem", "1.25rem"],
      sm: [".875rem", "1.25rem"],
      code: [".9375em", "1.25rem"],
      base: ["16px", "1.25rem"],
      m: ["1.125rem", "1.25rem"],
      lg: ["1.375rem", "2.5rem"],
      xl: ["1.5rem", "2.5rem"],
      "2xl": ["1.75rem", "2.5rem"],
      "3xl": ["2.5rem", "3.75rem"],
      "4xl": ["3rem", "3.75rem"],
      "5xl": ["3.125rem", "3.75rem"],
      "6xl": ["3.75rem", "5rem"],
    },
    extend: {
      listStyleType: {
        latin: "lower-latin",
      },
      screens: {
        print: { raw: "print" },
        xs: "400px",
      },
      maxWidth: {
        prose: "760px",
      },
      lineHeight: {
        14: "3.5rem",
      },
      spacing: {
        15: "3.75rem",
      },
      letterSpacing: {
        extended: "0.017em",
      },
      margin: {
        fluid: "var(--fluid-margin)",
      },
      padding: {
        fluid: "var(--fluid-margin)",
      },
      width: {
        sidebar: "max(var(--sidebar-width), 17%)",
      },
      colors: {
        blue: colors.sky,
        brand: "var(--color-brand)",
        link: "var(--color-link)",
        gray: {
          light: "#616161",
          DEFAULT: "#424242",
          dark: "#212121",
        },
      },
      borderRadius: {
        xs: "1px",
      },
      backgroundOpacity: {
        2: "0.02",
      },
    },
  },
};
