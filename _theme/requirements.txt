alabaster==0.7.13; python_version >= "3.8" and python_version < "4.0"
attrs==22.2.0; python_version >= "3.8" and python_version < "4.0"
babel==2.12.1; python_version >= "3.8" and python_version < "4.0"
beautifulsoup4==4.11.2; python_version >= "3.8" and python_version < "4.0"
black==23.1.0; python_version >= "3.8" and python_version < "4.0"
certifi==2022.12.7; python_version >= "3.8" and python_version < "4"
cfgv==3.3.1; python_version >= "3.8" and python_version < "4.0"
charset-normalizer==3.0.1; python_version >= "3.8" and python_version < "4"
click==8.1.3; python_version >= "3.8" and python_version < "4.0"
colorama==0.4.6; python_version >= "3.8" and python_version < "4.0"
coverage==7.2.1; python_version >= "3.8" and python_version < "4.0"
distlib==0.3.6; python_version >= "3.8" and python_version < "4.0"
docutils==0.19; python_version >= "3.8" and python_version < "4.0"
exceptiongroup==1.1.0; python_version >= "3.8" and python_version < "3.11"
filelock==3.9.0; python_version >= "3.8" and python_version < "4.0"
identify==2.5.18; python_version >= "3.8" and python_version < "4.0"
idna==3.4; python_version >= "3.8" and python_version < "4"
imagesize==1.4.1; python_version >= "3.8" and python_version < "4.0"
importlib-metadata==6.0.0; python_version >= "3.8" and python_version < "3.10"
iniconfig==2.0.0; python_version >= "3.8" and python_version < "4.0"
isort==5.12.0; python_version >= "3.8" and python_version < "4.0"
jinja2==3.1.2; python_version >= "3.8" and python_version < "4.0"
livereload==2.6.3; python_version >= "3.8" and python_version < "4.0"
markdown-it-py==2.2.0; python_version >= "3.8" and python_version < "4.0"
markupsafe==2.1.2; python_version >= "3.8" and python_version < "4.0"
mdit-py-plugins==0.3.5; python_version >= "3.8" and python_version < "4.0"
mdurl==0.1.2; python_version >= "3.8" and python_version < "4.0"
mypy-extensions==1.0.0; python_version >= "3.8" and python_version < "4.0"
mypy==1.0.1; python_version >= "3.8" and python_version < "4.0"
myst-parser==0.19.1; python_version >= "3.8" and python_version < "4.0"
nodeenv==1.7.0; python_version >= "3.8" and python_version < "4.0"
packaging==23.0; python_version >= "3.8" and python_version < "4.0"
pathspec==0.11.0; python_version >= "3.8" and python_version < "4.0"
platformdirs==3.1.0; python_version >= "3.8" and python_version < "4.0"
pluggy==1.0.0; python_version >= "3.8" and python_version < "4.0"
pre-commit==3.1.1; python_version >= "3.8" and python_version < "4.0"
pygments==2.14.0; python_version >= "3.8" and python_version < "4.0"
pytest-cov==4.0.0; python_version >= "3.8" and python_version < "4.0"
pytest==7.2.2; python_version >= "3.8" and python_version < "4.0"
python-dotenv==1.0.0; python_version >= "3.8" and python_version < "4.0"
pytz==2022.7.1; python_version >= "3.8" and python_version < "3.9"
pyyaml==6.0; python_version >= "3.8" and python_version < "4.0"
requests==2.28.2; python_version >= "3.8" and python_version < "4"
ruff==0.0.254; python_version >= "3.8" and python_version < "4.0"
setuptools==67.4.0; python_version >= "3.8" and python_version < "4.0"
six==1.16.0; python_version >= "3.8" and python_version < "4.0"
snowballstemmer==2.2.0; python_version >= "3.8" and python_version < "4.0"
soupsieve==2.4; python_version >= "3.8" and python_version < "4.0"
sphinx-autobuild==2021.3.14; python_version >= "3.8" and python_version < "4.0"
sphinx-sitemap==2.5.0; python_version >= "3.8" and python_version < "4.0"
sphinx==6.1.3; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-applehelp==1.0.4; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-devhelp==1.0.2; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-htmlhelp==2.0.1; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-jsmath==1.0.1; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-qthelp==1.0.3; python_version >= "3.8" and python_version < "4.0"
sphinxcontrib-serializinghtml==1.1.5; python_version >= "3.8" and python_version < "4.0"
tomli==2.0.1; python_version >= "3.8" and python_full_version <= "3.11.0a6"
tornado==6.2; python_version >= "3.8" and python_version < "4.0"
typing-extensions==4.5.0; python_version >= "3.8" and python_version < "4.0"
urllib3==1.26.14; python_version >= "3.8" and python_version < "4"
virtualenv==20.20.0; python_version >= "3.8" and python_version < "4.0"
zipp==3.15.0; python_version >= "3.8" and python_version < "3.10"