# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Perform discovery on a Matrix server name"""

import asyncio
import json
import sys

from matrixlib import client


if len(sys.argv) != 2:
    print(__doc__)
    print()
    print(f"Usage: {sys.argv[0]} <servername>")
    exit(1)


async def main():
    print(f"Looking for Matrix server for {sys.argv[1]}...")
    res = await client.discover(sys.argv[1])
    if res == None:
        print("No Matrix server found")
    else:
        base, well_known, versions = res
        print(f"Base URL: {base}")
        print("Well-known data:")
        print(json.dumps(well_known, indent=2))
        print(f"Supported API versions: {versions['versions']}")


asyncio.run(main())
