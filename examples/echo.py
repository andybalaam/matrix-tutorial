# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Echo events"""

import asyncio
import json
import sys
import typing

from matrixlib import client
from matrixlib import events


class JsonFileStorage:
    def __init__(self):
        try:
            self.file = open("data.json", "r+")
            self.data = json.load(self.file)
        except FileNotFoundError:
            self.file = open("data.json", "x")
            self.file.write("{}")
            self.data = {}

    def __getitem__(self, key: str) -> typing.Any:
        return self.data[key]

    def __setitem__(self, key: str, value: typing.Any) -> None:
        self.data[key] = value
        self.file.seek(0)
        self.file.truncate(0)
        json.dump(self.data, self.file)

    def __delitem__(self, key: str) -> None:
        del self.data[key]
        self.file.seek(0)
        self.file.truncate(0)
        json.dump(self.data, self.file)

    def __contains__(self, key: str) -> bool:
        return key in self.data

    def get(self, key: str, default: typing.Any = None) -> typing.Any:
        return self.data.get(key, default)

    def clear(self) -> None:
        self.data.clear()
        self.file.seek(0)
        self.file.truncate(0)
        self.file.write("{}")


async def main() -> None:
    global clientref
    async with client.Client(storage=JsonFileStorage()) as c:

        async def timeline_subscriber(updates: client.RoomTimelineUpdates) -> None:
            for event in updates.events:
                if (
                    isinstance(event, events.StateEvent)
                    or event.type != "m.room.message"
                    or "body" not in event.content
                    or event.content.get("msgtype") != "m.text"
                    or event.sender == c.user_id
                ):
                    continue
                if event.content["body"] == "!quit":
                    c.stop_sync()
                    return

                content = {"body": event.content["body"], "msgtype": "m.notice"}
                await c.send_event(updates.room_id, "m.room.message", content)

        c.publisher.subscribe(client.RoomTimelineUpdates, timeline_subscriber)
        c.start_sync()

        await typing.cast(asyncio.Task[None], c.sync_task)


asyncio.run(main())
