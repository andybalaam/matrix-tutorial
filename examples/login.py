# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Log in as a user to a homeserver"""

import argparse
import asyncio
import getpass
import json
import typing
from urllib.parse import urljoin

from matrixlib import client


class JsonFileStorage:
    def __init__(self):
        try:
            self.file = open("data.json", "r+")
            self.data = json.load(self.file)
        except FileNotFoundError:
            self.file = open("data.json", "x")
            self.file.write("{}")
            self.data = {}

    def __getitem__(self, key: str) -> typing.Any:
        return self.data[key]

    def __setitem__(self, key: str, value: typing.Any) -> None:
        self.data[key] = value
        self.file.seek(0)
        self.file.truncate(0)
        json.dump(self.data, self.file)

    def __delitem__(self, key: str) -> None:
        del self.data[key]
        self.file.seek(0)
        self.file.truncate(0)
        json.dump(self.data, self.file)

    def __contains__(self, key: str) -> bool:
        return key in self.data

    def get(self, key: str, default: typing.Any = None) -> typing.Any:
        return self.data.get(key, default)

    def clear(self) -> None:
        self.data.clear()
        self.file.seek(0)
        self.file.truncate(0)
        self.file.write("{}")


async def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("user_id")
    parser.add_argument(
        "-H",
        "--homeserver-url",
        dest="hsurl",
        help="The base URL of the homeserver to connect to",
    )

    args = parser.parse_args()

    if args.hsurl == None:
        [_, servername] = args.user_id.split(":", maxsplit=1)
        print(servername)
        res = await client.discover(servername)
        if res == None:
            print("No Matrix server found")
            exit(1)
        else:
            base, well_known, versions = res
            c = client.Client(
                storage=JsonFileStorage(),
                base_client_url=base,
                well_known=well_known,
                versions=versions,
            )
    else:
        c = client.Client(
            storage=JsonFileStorage(),
            base_client_url=urljoin(args.hsurl, "_matrix/client/"),
        )

    async with c:
        if "m.login.password" not in await c.login_methods():
            print("Error: server does not support logging in with password")
            exit(1)

        password = getpass.getpass()
        await c.log_in_with_password(args.user_id, password)
        print(f"logged in as {c.user_id} with device ID {c.device_id}")


asyncio.run(main())
