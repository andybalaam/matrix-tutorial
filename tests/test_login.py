# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import pytest

from matrixlib import client
from matrixlib import error


@pytest.mark.asyncio
async def test_login_methods(mock_aioresponse):
    async with client.Client(
        storage={},
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/login",
            status=200,
            body='{"flows":[{"type":"m.login.password"}]}',
            headers={
                "content-type": "application/json",
            },
        )
        assert await c.login_methods() == ["m.login.password"]

        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/login",
            status=200,
            body="{}",
            headers={
                "content-type": "application/json",
            },
        )
        with pytest.raises(error.InvalidResponseError):
            await c.login_methods()


@pytest.mark.asyncio
async def test_log_in_with_password(mock_aioresponse):
    async with client.Client(
        storage={},
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/login",
            status=200,
            payload={
                "user_id": "@alice:example.org",
                "device_id": "DEVICEID",
                "access_token": "anopaquestring",
            },
            headers={
                "content-type": "application/json",
            },
        )
        await c.log_in_with_password("@alice:example.org", "password")
        mock_aioresponse.assert_called_with(
            "https://matrix-client.example.org/_matrix/client/v3/login",
            method="POST",
            json={
                "type": "m.login.password",
                "identifier": {
                    "type": "m.id.user",
                    "user": "@alice:example.org",
                },
                "password": "password",
                "refresh_token": True,
            },
        )
