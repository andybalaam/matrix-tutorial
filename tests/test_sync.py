# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import asyncio
import json
import pytest
import unittest.mock as mock

from matrixlib import client
from matrixlib.events import Event, RoomEvent, StateEvent
from matrixlib import rooms


@pytest.mark.asyncio
async def test_sync_retry(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?timeout=30000",
            status=500,
            body="Error",
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?timeout=30000",
            status=200,
            body='{"next_batch":"token"}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?since=token&timeout=30000",
            status=200,
            body='{"next_batch":"token"}',
            headers={
                "content-type": "application/json",
            },
            repeat=True,
        )

        messages = []

        def subscriber(msg) -> None:
            messages.append(msg)
            if isinstance(msg, client.SyncResumed):
                c.stop_sync()

        c.publisher.subscribe((client.SyncFailed, client.SyncResumed), subscriber)

        with mock.patch("asyncio.sleep") as sleep:
            c.start_sync()

            try:
                await c.sync_task
            except asyncio.CancelledError:
                pass

            assert messages == [client.SyncFailed(2), client.SyncResumed()]
            assert sleep.call_args == mock.call(2)


@pytest.mark.asyncio
async def test_sync_room_events(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?timeout=30000",
            status=200,
            body=json.dumps(
                {
                    "rooms": {
                        "join": {
                            "!roomid": {
                                "state": {
                                    "events": [
                                        {
                                            "type": "m.room.name",
                                            "state_key": "",
                                            "sender": "@alice:example.org",
                                            "content": {
                                                "name": "Jabberwocky",
                                            },
                                            "event_id": "$event1",
                                            "origin_server_ts": 1234567890123,
                                            "unsigned": {
                                                "age": 4321,
                                            },
                                        },
                                    ],
                                },
                                "timeline": {
                                    "events": [
                                        {
                                            "type": "m.room.message",
                                            "sender": "@alice:example.org",
                                            "content": {
                                                "body": "'Twas brillig, and the slithy toves",
                                            },
                                            "event_id": "$event2",
                                            "origin_server_ts": 1234567892123,
                                            "unsigned": {
                                                "age": 2321,
                                            },
                                        },
                                    ],
                                    "limited": True,
                                    "prev_batch": "prev1",
                                },
                            },
                        },
                    },
                    "next_batch": "token1",
                }
            ),
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?since=token1&timeout=30000",
            status=200,
            body=json.dumps(
                {
                    "rooms": {
                        "leave": {
                            "!roomid": {
                                "timeline": {
                                    "events": [
                                        {
                                            "type": "m.room.message",
                                            "sender": "@alice:example.org",
                                            "content": {
                                                "body": "Did gyre and gimble in the wabe",
                                            },
                                            "event_id": "$event3",
                                            "origin_server_ts": 1234567894123,
                                            "unsigned": {
                                                "age": 1234,
                                            },
                                        },
                                    ],
                                    "prev_batch": "prev2",
                                },
                            },
                        },
                    },
                    "next_batch": "token2",
                }
            ),
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?since=token2&timeout=30000",
            status=200,
            body='{"next_batch":"token2"}',
            headers={
                "content-type": "application/json",
            },
            repeat=True,
        )

        messages = []

        def subscriber(msg) -> None:
            messages.append(msg)
            if isinstance(msg, client.LeftRoom) or isinstance(msg, client.SyncFailed):
                c.stop_sync()

        c.publisher.subscribe(
            (
                client.RoomStateUpdates,
                client.RoomTimelineUpdates,
                client.LeftRoom,
                client.SyncFailed,
            ),
            subscriber,
        )

        c.start_sync()

        try:
            await c.sync_task
        except asyncio.CancelledError:
            pass

        assert messages == [
            client.RoomStateUpdates(
                "!roomid",
                [
                    StateEvent(
                        room_id="!roomid",
                        type="m.room.name",
                        state_key="",
                        sender="@alice:example.org",
                        content={"name": "Jabberwocky"},
                        event_id="$event1",
                        origin_server_ts=1234567890123,
                        unsigned={"age": 4321},
                    )
                ],
            ),
            client.RoomTimelineUpdates(
                "!roomid",
                [
                    RoomEvent(
                        room_id="!roomid",
                        type="m.room.message",
                        sender="@alice:example.org",
                        content={"body": "'Twas brillig, and the slithy toves"},
                        event_id="$event2",
                        origin_server_ts=1234567892123,
                        unsigned={"age": 2321},
                    )
                ],
                True,
                "prev1",
            ),
            client.RoomTimelineUpdates(
                "!roomid",
                [
                    RoomEvent(
                        room_id="!roomid",
                        type="m.room.message",
                        sender="@alice:example.org",
                        content={"body": "Did gyre and gimble in the wabe"},
                        event_id="$event3",
                        origin_server_ts=1234567894123,
                        unsigned={"age": 1234},
                    )
                ],
                False,
                "prev2",
            ),
            client.LeftRoom("!roomid"),
        ]


@pytest.mark.asyncio
async def test_room_state_tracking(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        tracker = rooms.RoomStateTracker(c)

        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?timeout=30000",
            status=200,
            body=json.dumps(
                {
                    "rooms": {
                        "join": {
                            "!roomid": {
                                "state": {
                                    "events": [
                                        {
                                            "type": "m.room.name",
                                            "state_key": "",
                                            "sender": "@alice:example.org",
                                            "content": {
                                                "name": "Room name",
                                            },
                                            "event_id": "$event1",
                                            "origin_server_ts": 1234567890123,
                                            "unsigned": {
                                                "age": 4321,
                                            },
                                        },
                                        {
                                            "type": "m.room.topic",
                                            "state_key": "",
                                            "sender": "@alice:example.org",
                                            "content": {
                                                "topic": "Room about stuff",
                                            },
                                            "event_id": "$event2",
                                            "origin_server_ts": 1234567890123,
                                            "unsigned": {
                                                "age": 4321,
                                            },
                                        },
                                    ],
                                },
                                "timeline": {
                                    "events": [
                                        {
                                            "type": "m.room.member",
                                            "state_key": "@bob:example.org",
                                            "sender": "@bob:example.org",
                                            "content": {
                                                "membership": "join",
                                            },
                                            "event_id": "$event3",
                                            "origin_server_ts": 1234567892123,
                                            "unsigned": {
                                                "age": 2321,
                                            },
                                        },
                                        {
                                            "type": "m.room.name",
                                            "state_key": "",
                                            "sender": "@alice:example.org",
                                            "content": {
                                                "name": "New room name",
                                            },
                                            "event_id": "$event4",
                                            "origin_server_ts": 1234567893123,
                                            "unsigned": {
                                                "age": 1321,
                                            },
                                        },
                                    ],
                                    "limited": True,
                                    "prev_batch": "prev1",
                                },
                            },
                        },
                    },
                    "next_batch": "token1",
                }
            ),
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?since=token1&timeout=30000",
            status=200,
            body='{"next_batch":"token1"}',
            headers={
                "content-type": "application/json",
            },
            repeat=True,
        )

        def subscriber(msg) -> None:
            c.stop_sync()

        c.publisher.subscribe(
            (client.RoomTimelineUpdates, client.SyncFailed), subscriber
        )

        c.start_sync()

        try:
            await c.sync_task
        except asyncio.CancelledError:
            pass

        assert tracker.get_state("!roomid", "m.room.name") == StateEvent(
            room_id="!roomid",
            type="m.room.name",
            state_key="",
            sender="@alice:example.org",
            content={"name": "New room name"},
            event_id="$event4",
            origin_server_ts=1234567893123,
            unsigned={"age": 1321},
        )

        assert tracker.get_state("!roomid", "m.room.topic") == StateEvent(
            room_id="!roomid",
            type="m.room.topic",
            state_key="",
            sender="@alice:example.org",
            content={"topic": "Room about stuff"},
            event_id="$event2",
            origin_server_ts=1234567890123,
            unsigned={"age": 4321},
        )

        assert (
            tracker.get_state("!roomid", "m.room.member", "@carol:example.org") == None
        )

        assert tracker.get_state("!another_room", "m.room.name") == None

        assert tracker.get_all_state_for_type("!roomid", "m.room.member") == {
            "@bob:example.org": StateEvent(
                room_id="!roomid",
                type="m.room.member",
                state_key="@bob:example.org",
                sender="@bob:example.org",
                content={"membership": "join"},
                event_id="$event3",
                origin_server_ts=1234567892123,
                unsigned={"age": 2321},
            ),
        }


@pytest.mark.asyncio
async def test_receive_to_device_events(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?timeout=30000",
            status=200,
            body=json.dumps(
                {
                    "to_device": {
                        "events": [
                            {
                                "sender": "@alice:example.org",
                                "type": "m.room.message",
                                "content": {
                                    "body": "Hello world!",
                                },
                            },
                        ],
                    },
                    "next_batch": "token1",
                }
            ),
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?since=token1&timeout=30000",
            status=200,
            body='{"next_batch":"token1"}',
            headers={
                "content-type": "application/json",
            },
            repeat=True,
        )

        def subscriber(msg) -> None:
            assert msg.events == [
                Event(
                    sender="@alice:example.org",
                    type="m.room.message",
                    content={"body": "Hello world!"},
                )
            ]
            c.stop_sync()

        c.publisher.subscribe(client.ToDeviceEvents, subscriber)

        c.start_sync()

        try:
            await c.sync_task
        except asyncio.CancelledError:
            pass
