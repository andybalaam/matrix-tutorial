# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import asyncio
import aioresponses
import json
import pytest
import re
import typing
from unittest.mock import Mock
import unittest.mock as mock
import urllib
import vodozemac

from matrixlib import client
from matrixlib.client import Client
from matrixlib import devices
from matrixlib import events
from matrixlib import error
from matrixlib import megolm
from matrixlib import olm
from matrixlib import rooms


ROOM_ENCRYPTION_EVENT = {
    "room_id": "!room_id",
    "type": "m.room.encryption",
    "state_key": "",
    "sender": "@alice:example.org",
    "event_id": "$encryption_event",
    "origin_server_ts": 1234567890123,
    "content": {
        "algorithm": megolm.MEGOLM_ALGORITHM,
    },
}

ALICE_ROOM_MEMBERSHIP = {
    "room_id": "!room_id",
    "type": "m.room.member",
    "state_key": "@alice:example.org",
    "sender": "@alice:example.org",
    "event_id": "$alice_event",
    "origin_server_ts": 1234567890123,
    "content": {"membership": "join"},
}

BOB_ROOM_MEMBERSHIP = {
    "room_id": "!room_id",
    "type": "m.room.member",
    "state_key": "@bob:example.org",
    "sender": "@bob:example.org",
    "event_id": "$bob_event",
    "origin_server_ts": 1234567890123,
    "content": {"membership": "join"},
}

ALICE_DEVICE_KEY = {
    "algorithms": [megolm.MEGOLM_ALGORITHM],
    "device_id": "ABCDEFG",
    "keys": {
        "curve25519:ABCDEFG": "some+key",
    },
    "user_id": "@alice:example.org",
}

BOB_DEVICE_KEY = {
    "algorithms": [megolm.MEGOLM_ALGORITHM],
    "device_id": "HIJKLMN",
    "keys": {
        "curve25519:HIJKLMN": "some+other+key",
    },
    "user_id": "@bob:example.org",
}

BOB_DEVICE_KEY2 = {
    "algorithms": [megolm.MEGOLM_ALGORITHM],
    "device_id": "OPQRSTU",
    "keys": {
        "curve25519:OPQRSTU": "yet+another+key",
    },
    "user_id": "@bob:example.org",
}


async def _create_device_keys(
    mock_aioresponse, client: Client
) -> typing.Tuple[dict, dict, devices.DeviceKeysManager]:
    device_keys = {}
    otks = {}

    ev = asyncio.Event()

    def callback(url, **kwargs):
        nonlocal device_keys, otks
        device_keys = kwargs["json"]["device_keys"]
        otks = kwargs["json"]["one_time_keys"]
        ev.set()

        return aioresponses.CallbackResult(
            status=200,
            body='{"one_time_key_counts":{"signed_curve25519":100}}',
            headers={
                "Content-Type": "application/json",
            },
        )

    mock_aioresponse.post(
        "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
        callback=callback,
    )
    device_keys_manager = devices.DeviceKeysManager(client, b"\x00" * 32)
    await ev.wait()

    return (device_keys, otks, device_keys_manager)


@pytest.mark.asyncio
async def test_megolm_get_session_key(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
            "device_tracker.cache.@alice:example.org": {
                "device_keys": {
                    "ABCDEFG": ALICE_DEVICE_KEY,
                },
            },
            "device_tracker.cache.@bob:example.org": {
                "device_keys": {
                    "HIJKLMN": BOB_DEVICE_KEY,
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        room_state_tracker = rooms.RoomStateTracker(c)
        device_tracker = devices.DeviceTracker(c)
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            status=200,
            body='{"one_time_key_counts":{"signed_curve25519":100}}',
            headers={
                "Content-Type": "application/json",
            },
        )
        device_keys_manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        outbound_session = megolm.OutboundMegolmSession(
            c,
            "!room_id",
            room_state_tracker,
            device_tracker,
            device_keys_manager,
            b"\x00" * 32,
        )

        [
            (bob_device_key, bob_room_key)
        ] = await outbound_session.get_session_key_for_sending()

        assert bob_device_key == {
            "algorithms": [megolm.MEGOLM_ALGORITHM],
            "device_id": "HIJKLMN",
            "keys": {
                "curve25519:HIJKLMN": "some+other+key",
            },
            "user_id": "@bob:example.org",
        }

        [
            (bob_device_key_again, bob_room_key_again)
        ] = await outbound_session.get_session_key_for_sending()

        assert bob_device_key_again == bob_device_key
        assert bob_room_key_again == bob_room_key

        outbound_session.mark_as_sent([bob_device_key])

        assert await outbound_session.get_session_key_for_sending() == []


@pytest.mark.asyncio
async def test_megolm_get_session_key_rotation_by_time(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": {
                            "room_id": "!room_id",
                            "type": "m.room.encryption",
                            "state_key": "",
                            "sender": "@alice:example.org",
                            "event_id": "$encryption_event",
                            "origin_server_ts": 1234567890123,
                            "content": {
                                "algorithm": megolm.MEGOLM_ALGORITHM,
                                "rotation_period_ms": 5,
                            },
                        },
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                    },
                },
            },
            "device_tracker.cache.@alice:example.org": {
                "device_keys": {
                    "ABCDEFG": ALICE_DEVICE_KEY,
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        room_state_tracker = rooms.RoomStateTracker(c)
        device_tracker = devices.DeviceTracker(c)
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            status=200,
            body='{"one_time_key_counts":{"signed_curve25519":100}}',
            headers={
                "Content-Type": "application/json",
            },
        )
        device_keys_manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        outbound_session = megolm.OutboundMegolmSession(
            c,
            "!room_id",
            room_state_tracker,
            device_tracker,
            device_keys_manager,
            b"\x00" * 32,
        )

        await asyncio.sleep(0.01)  # sleep for 10ms to make sure session has expired

        with pytest.raises(megolm.SessionExpiredException):
            await outbound_session.get_session_key_for_sending()


@pytest.mark.asyncio
async def test_megolm_get_session_key_rotation_by_membership(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
            "device_tracker.cache.@alice:example.org": {
                "device_keys": {
                    "ABCDEFG": ALICE_DEVICE_KEY,
                },
            },
            "device_tracker.cache.@bob:example.org": {
                "device_keys": {
                    "HIJKLMN": BOB_DEVICE_KEY,
                    "OPQRSTU": BOB_DEVICE_KEY2,
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        room_state_tracker = rooms.RoomStateTracker(c)
        device_tracker = devices.DeviceTracker(c)
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            status=200,
            body='{"one_time_key_counts":{"signed_curve25519":100}}',
            headers={
                "Content-Type": "application/json",
            },
        )
        device_keys_manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        outbound_session = megolm.OutboundMegolmSession(
            c,
            "!room_id",
            room_state_tracker,
            device_tracker,
            device_keys_manager,
            b"\x00" * 32,
        )

        await outbound_session.get_session_key_for_sending()

        # simulate Bob logging out a device
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/query",
            status=200,
            body=json.dumps(
                {
                    "device_keys": {
                        "@bob:example.org": {
                            "HIJKLMN": BOB_DEVICE_KEY,
                        },
                    },
                }
            ),
            headers={
                "Content-Type": "application/json",
            },
        )
        await c.publisher.publish(client.DeviceChanges(["@bob:example.org"], []))

        with pytest.raises(megolm.SessionExpiredException):
            await outbound_session.get_session_key_for_sending()

        outbound_session2 = megolm.OutboundMegolmSession(
            c,
            "!room_id",
            room_state_tracker,
            device_tracker,
            device_keys_manager,
            b"\x00" * 32,
        )

        await outbound_session2.get_session_key_for_sending()

        # simulate Bob leaving
        await c.publisher.publish(
            client.RoomTimelineUpdates(
                "!room_id",
                [
                    events.StateEvent(
                        room_id="!room_id",
                        type="m.room.member",
                        state_key="@bob:example.org",
                        sender="@bob:example.org",
                        content={
                            "membership": "leave",
                        },
                        event_id="$bob_leave_event",
                        origin_server_ts=1234567890123,
                    )
                ],
                False,
                "",
            )
        )

        with pytest.raises(megolm.SessionExpiredException):
            await outbound_session2.get_session_key_for_sending()


@pytest.mark.asyncio
async def test_megolm_get_session_key_rotation_by_number(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": {
                            "room_id": "!room_id",
                            "type": "m.room.encryption",
                            "state_key": "",
                            "sender": "@alice:example.org",
                            "event_id": "$encryption_event",
                            "origin_server_ts": 1234567890123,
                            "content": {
                                "algorithm": megolm.MEGOLM_ALGORITHM,
                                "rotation_period_msgs": 2,
                            },
                        },
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                    },
                },
            },
            "device_tracker.cache.@alice:example.org": {
                "device_keys": {
                    "ABCDEFG": ALICE_DEVICE_KEY,
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        room_state_tracker = rooms.RoomStateTracker(c)
        device_tracker = devices.DeviceTracker(c)
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            status=200,
            body='{"one_time_key_counts":{"signed_curve25519":100}}',
            headers={
                "Content-Type": "application/json",
            },
        )
        device_keys_manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        outbound_session = megolm.OutboundMegolmSession(
            c,
            "!room_id",
            room_state_tracker,
            device_tracker,
            device_keys_manager,
            b"\x00" * 32,
        )

        await outbound_session.get_session_key_for_sending()
        outbound_session.encrypt("m.room.message", {"body": "one"})

        await outbound_session.get_session_key_for_sending()
        outbound_session.encrypt("m.room.message", {"body": "two"})

        with pytest.raises(megolm.SessionExpiredException):
            await outbound_session.get_session_key_for_sending()


@pytest.mark.asyncio
async def test_megolm_decryption(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
            "device_tracker.cache.@alice:example.org": {
                "device_keys": {
                    "ABCDEFG": ALICE_DEVICE_KEY,
                },
            },
            "device_tracker.cache.@bob:example.org": {
                "device_keys": {
                    "HIJKLMN": BOB_DEVICE_KEY,
                },
            },
            "device_tracker.cache.@carol:example.org": {
                "device_keys": {
                    "OPQRSTU": {
                        "algorithms": [megolm.MEGOLM_ALGORITHM],
                        "device_id": "OPQRSTU",
                        "keys": {
                            "curve25519:OPQRSTU": "yet+another+key",
                        },
                        "user_id": "@carol:example.org",
                    },
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            async with client.Client(
                storage={
                    "access_token": "anaccesstoken",
                    "user_id": "@carol:example.org",
                    "device_id": "OPQRSTU",
                },
                callbacks={},
                base_client_url="https://matrix-client.example.org/_matrix/client/",
            ) as carol:
                room_state_tracker = rooms.RoomStateTracker(alice)
                device_tracker = devices.DeviceTracker(alice)
                mock_aioresponse.post(
                    "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )
                device_keys_manager = devices.DeviceKeysManager(alice, b"\x00" * 32)
                outbound_session = megolm.OutboundMegolmSession(
                    alice,
                    "!room_id",
                    room_state_tracker,
                    device_tracker,
                    device_keys_manager,
                    b"\x00" * 32,
                )
                [
                    (bob_device_key, bob_room_key)
                ] = await outbound_session.get_session_key_for_sending()

                alice_inbound_session = (
                    megolm.InboundMegolmSession.from_outbound_session(
                        alice,
                        outbound_session,
                        b"\x00" * 32,
                    )
                )
                assert alice_inbound_session.authenticated
                assert (
                    alice_inbound_session.sender_key == device_keys_manager.identity_key
                )

                bob_inbound_session = megolm.InboundMegolmSession.from_room_key(
                    bob,
                    "@alice:example.org",
                    device_keys_manager.identity_key,
                    bob_room_key,
                    b"\x00" * 32,
                )
                assert bob_inbound_session.authenticated
                assert (
                    bob_inbound_session.sender_key == device_keys_manager.identity_key
                )

                outbound_session.mark_as_sent([bob_device_key])

                encrypted_content1 = outbound_session.encrypt(
                    "m.room.message",
                    {"body": "Hello World!", "msgtype": "m.text"},
                )
                encrypted1 = events.RoomEvent(
                    sender="@alice:example.org",
                    event_id="$event1",
                    type="m.room.encrypted",
                    room_id="!room_id",
                    content=encrypted_content1,
                    origin_server_ts=1234567890000,
                )

                assert alice_inbound_session.decrypt(encrypted1) == {
                    "room_id": "!room_id",
                    "type": "m.room.message",
                    "content": {"body": "Hello World!", "msgtype": "m.text"},
                }
                assert bob_inbound_session.decrypt(encrypted1) == {
                    "room_id": "!room_id",
                    "type": "m.room.message",
                    "content": {"body": "Hello World!", "msgtype": "m.text"},
                }

                bob_loaded_inbound_session = megolm.InboundMegolmSession.from_storage(
                    bob,
                    "!room_id",
                    "@alice:example.org",
                    encrypted_content1["session_id"],
                    b"\x00" * 32,
                )
                assert bob_loaded_inbound_session.authenticated
                assert (
                    bob_loaded_inbound_session.sender_key
                    == device_keys_manager.identity_key
                )
                assert bob_loaded_inbound_session.decrypt(encrypted1) == {
                    "room_id": "!room_id",
                    "type": "m.room.message",
                    "content": {"body": "Hello World!", "msgtype": "m.text"},
                }

                loaded_outbound_session = megolm.OutboundMegolmSession(
                    alice,
                    "!room_id",
                    room_state_tracker,
                    device_tracker,
                    device_keys_manager,
                    b"\x00" * 32,
                    outbound_session.session_id,
                )

                await alice.publisher.publish(
                    client.RoomTimelineUpdates(
                        "!room_id",
                        [
                            events.StateEvent(
                                room_id="!room_id",
                                type="m.room.member",
                                state_key="@carol:example.org",
                                sender="@carol:example.org",
                                content={
                                    "membership": "join",
                                },
                                event_id="$carol_event",
                                origin_server_ts=1234567890123,
                            )
                        ],
                        False,
                        "",
                    )
                )

                [
                    (carol_device_key, carol_room_key)
                ] = await outbound_session.get_session_key_for_sending()
                carol_inbound_session = megolm.InboundMegolmSession.from_room_key(
                    carol,
                    "@alice:example.org",
                    device_keys_manager.identity_key,
                    carol_room_key,
                    b"\x00" * 32,
                )

                outbound_session.mark_as_sent([carol_device_key])

                encrypted_content2 = outbound_session.encrypt(
                    "m.room.message",
                    {"body": "Bonjour!", "msgtype": "m.text"},
                )
                encrypted2 = events.RoomEvent(
                    sender="@alice:example.org",
                    event_id="$event1",
                    type="m.room.encrypted",
                    room_id="!room_id",
                    content=encrypted_content2,
                    origin_server_ts=1234567890000,
                )

                assert alice_inbound_session.decrypt(encrypted2) == {
                    "room_id": "!room_id",
                    "type": "m.room.message",
                    "content": {"body": "Bonjour!", "msgtype": "m.text"},
                }
                assert bob_inbound_session.decrypt(encrypted2) == {
                    "room_id": "!room_id",
                    "type": "m.room.message",
                    "content": {"body": "Bonjour!", "msgtype": "m.text"},
                }
                assert carol_inbound_session.decrypt(encrypted2) == {
                    "room_id": "!room_id",
                    "type": "m.room.message",
                    "content": {"body": "Bonjour!", "msgtype": "m.text"},
                }

                with pytest.raises(vodozemac.MegolmDecryptionException):
                    carol_inbound_session.decrypt(encrypted1)


@pytest.mark.asyncio
async def test_replay_detection(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
            "device_tracker.cache.@alice:example.org": {
                "device_keys": {
                    "ABCDEFG": ALICE_DEVICE_KEY,
                },
            },
            "device_tracker.cache.@bob:example.org": {
                "device_keys": {
                    "HIJKLMN": BOB_DEVICE_KEY,
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            room_state_tracker = rooms.RoomStateTracker(alice)
            device_tracker = devices.DeviceTracker(alice)
            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                status=200,
                body='{"one_time_key_counts":{"signed_curve25519":100}}',
                headers={
                    "Content-Type": "application/json",
                },
            )
            device_keys_manager = devices.DeviceKeysManager(alice, b"\x00" * 32)
            outbound_session = megolm.OutboundMegolmSession(
                alice,
                "!room_id",
                room_state_tracker,
                device_tracker,
                device_keys_manager,
                b"\x00" * 32,
            )
            [
                (bob_device_key, bob_room_key)
            ] = await outbound_session.get_session_key_for_sending()

            bob_inbound_session = megolm.InboundMegolmSession.from_room_key(
                bob,
                "@alice:example.org",
                device_keys_manager.identity_key,
                bob_room_key,
                b"\x00" * 32,
            )

            outbound_session.mark_as_sent([bob_device_key])

            encrypted_content1 = outbound_session.encrypt(
                "m.room.message",
                {"body": "Hello World!", "msgtype": "m.text"},
            )
            encrypted1 = events.RoomEvent(
                sender="@alice:example.org",
                event_id="$event1",
                type="m.room.encrypted",
                room_id="!room_id",
                content=encrypted_content1,
                origin_server_ts=1234567890000,
            )

            assert bob_inbound_session.decrypt(encrypted1) == {
                "room_id": "!room_id",
                "type": "m.room.message",
                "content": {"body": "Hello World!", "msgtype": "m.text"},
            }
            assert bob_inbound_session.decrypt(encrypted1) == {
                "room_id": "!room_id",
                "type": "m.room.message",
                "content": {"body": "Hello World!", "msgtype": "m.text"},
            }

            encrypted2 = events.RoomEvent(
                sender="@alice:example.org",
                event_id="$event2",
                type="m.room.encrypted",
                room_id="!room_id",
                content=encrypted_content1,
                origin_server_ts=1234567890123,
            )
            with pytest.raises(RuntimeError):
                bob_inbound_session.decrypt(encrypted2)


@pytest.mark.asyncio
async def test_decrypt_olm_encrypted_room_key(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            (
                alice_device_keys,
                alice_otks,
                alice_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, alice)

            (
                bob_device_keys,
                bob_otks,
                bob_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, bob)

            # store the device keys so that the device tracker can get them
            alice.storage["device_tracker.cache.@alice:example.org"] = bob.storage[
                "device_tracker.cache.@alice:example.org"
            ] = {"device_keys": {"ABCDEFG": alice_device_keys}}

            alice.storage["device_tracker.cache.@bob:example.org"] = bob.storage[
                "device_tracker.cache.@bob:example.org"
            ] = {"device_keys": {"HIJKLMN": bob_device_keys}}

            alice_room_state_tracker = rooms.RoomStateTracker(alice)
            alice_device_tracker = devices.DeviceTracker(alice)
            outbound_session = megolm.OutboundMegolmSession(
                alice,
                "!room_id",
                alice_room_state_tracker,
                alice_device_tracker,
                alice_device_keys_manager,
                b"\x00" * 32,
            )

            [
                (bob_device_keys, bob_room_key)
            ] = await outbound_session.get_session_key_for_sending()

            # use the first `signed_curve25519` one-time key
            otk = [
                key
                for id, key in bob_otks.items()
                if id.startswith("signed_curve25519:")
            ][0]

            alice_olm_channel = olm.OlmChannel.create_outbound_channel(
                alice,
                alice_device_keys_manager,
                bob_device_keys,
                otk,
            )

            room_key_encrypted = alice_olm_channel.encrypt("m.room_key", bob_room_key)

            bob_device_tracker = devices.DeviceTracker(bob)

            bob_encryption_manager = megolm.OlmMegolmEncryptionManager(
                bob,
                b"\x00" * 32,
                bob_device_keys_manager,
                bob_device_tracker,
            )

            await bob.publisher.publish(
                client.ToDeviceEvents(
                    [
                        events.Event(
                            sender="@alice:example.org",
                            type="m.room.encrypted",
                            content=room_key_encrypted,
                        )
                    ]
                )
            )

            assert (
                f"inbound_megolm_session.!room_id.@alice:example.org.{outbound_session.session_id}"
                in bob.storage
            )


@pytest.mark.asyncio
async def test_decrypt_megolm_encrypted_event(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            (
                alice_device_keys,
                alice_otks,
                alice_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, alice)

            (
                bob_device_keys,
                bob_otks,
                bob_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, bob)

            # store the device keys so that the device tracker can get them
            alice.storage["device_tracker.cache.@alice:example.org"] = bob.storage[
                "device_tracker.cache.@alice:example.org"
            ] = {"device_keys": {"ABCDEFG": alice_device_keys}}

            alice.storage["device_tracker.cache.@bob:example.org"] = bob.storage[
                "device_tracker.cache.@bob:example.org"
            ] = {"device_keys": {"HIJKLMN": bob_device_keys}}

            alice_room_state_tracker = rooms.RoomStateTracker(alice)
            alice_device_tracker = devices.DeviceTracker(alice)
            outbound_session = megolm.OutboundMegolmSession(
                alice,
                "!room_id",
                alice_room_state_tracker,
                alice_device_tracker,
                alice_device_keys_manager,
                b"\x00" * 32,
            )

            [
                (bob_device_keys, bob_room_key)
            ] = await outbound_session.get_session_key_for_sending()

            megolm.InboundMegolmSession.from_room_key(
                bob,
                "@alice:example.org",
                alice_device_keys["keys"]["ed25519:ABCDEFG"],
                bob_room_key,
                b"\x00" * 32,
            )

            encrypted_content = outbound_session.encrypt(
                "m.room.message",
                {"msgtype": "m.text", "body": "Hello World!"},
            )

            bob_device_tracker = devices.DeviceTracker(bob)
            bob_encryption_manager = megolm.OlmMegolmEncryptionManager(
                bob,
                b"\x00" * 32,
                bob_device_keys_manager,
                bob_device_tracker,
            )

            decrypted = await bob_encryption_manager.decrypt_room_event(
                events.RoomEvent(
                    sender="@alice:example.org",
                    type="m.room.encrypted",
                    room_id="!room_id",
                    event_id="$event_id",
                    content=encrypted_content,
                    origin_server_ts=1234567890123,
                )
            )

            assert decrypted == events.RoomEvent(
                sender="@alice:example.org",
                type="m.room.message",
                room_id="!room_id",
                event_id="$event_id",
                content={"msgtype": "m.text", "body": "Hello World!"},
                origin_server_ts=1234567890123,
            )


@pytest.mark.asyncio
async def test_megolm_decryption_error_handling(mock_aioresponse):
    async with Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as client:
        device_keys_manager = devices.DeviceKeysManager(client, b"\x00" * 32)
        device_tracker = devices.DeviceTracker(client)

        encryption_manager = megolm.OlmMegolmEncryptionManager(
            client,
            b"\x00" * 32,
            device_keys_manager,
            device_tracker,
        )

        with pytest.raises(
            error.UnableToDecryptError, match="Not encrypted or malformed event"
        ):
            await encryption_manager.decrypt_room_event(
                events.RoomEvent(
                    sender="@alice:example.org",
                    type="m.room.message",
                    room_id="!room_id",
                    event_id="$event_id",
                    content={"body": "Hello world!", "msgtype": "m.text"},
                    origin_server_ts=1234567890123,
                )
            )

        with pytest.raises(
            error.UnableToDecryptError, match="Not encrypted or malformed event"
        ):
            await encryption_manager.decrypt_room_event(
                events.RoomEvent(
                    sender="@alice:example.org",
                    type="m.room.encrypted",
                    room_id="!room_id",
                    event_id="$event_id",
                    content={"algorithm": "org.example.unknown"},
                    origin_server_ts=1234567890123,
                )
            )

        with pytest.raises(error.UnableToDecryptError, match="Unknown session ID"):
            await encryption_manager.decrypt_room_event(
                events.RoomEvent(
                    sender="@alice:example.org",
                    type="m.room.encrypted",
                    room_id="!room_id",
                    event_id="$event_id",
                    content={
                        "algorithm": megolm.MEGOLM_ALGORITHM,
                        "session_id": "nonexistent",
                        "ciphertext": "nothing",
                    },
                    origin_server_ts=1234567890123,
                )
            )


@pytest.mark.asyncio
async def test_room_encryption_event(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        (
            device_keys,
            otks,
            device_keys_manager,
        ) = await _create_device_keys(mock_aioresponse, c)

        device_tracker = devices.DeviceTracker(c)

        encryption_manager = megolm.OlmMegolmEncryptionManager(
            c,
            b"\x00" * 32,
            device_keys_manager,
            device_tracker,
        )

        subscriber = Mock()
        encryption_manager.publisher.subscribe(
            (
                megolm.EncryptionEnabled,
                megolm.UnknownRoomEncryptionAlgorithm,
                megolm.EncryptionDisabled,
                megolm.EncryptionAlgorithmChanged,
            ),
            subscriber,
        )

        assert encryption_manager.is_room_encrypted("!room1") == False
        assert encryption_manager.is_room_encrypted("!room2") == False

        await c.publisher.publish(
            client.RoomTimelineUpdates(
                "!room1",
                [
                    events.StateEvent(
                        type="m.room.encryption",
                        state_key="",
                        sender="@bob:example.org",
                        content={"algorithm": megolm.MEGOLM_ALGORITHM},
                        event_id="$enable_encryption",
                        room_id="!room1",
                        origin_server_ts=1234567890000,
                    )
                ],
                False,
                None,
            )
        )

        assert subscriber.call_count == 1
        assert subscriber.call_args == mock.call(
            megolm.EncryptionEnabled("!room1", megolm.MEGOLM_ALGORITHM)
        )
        assert encryption_manager.is_room_encrypted("!room1") == True

        subscriber.reset_mock()
        await c.publisher.publish(
            client.RoomTimelineUpdates(
                "!room1",
                [
                    events.StateEvent(
                        type="m.room.encryption",
                        state_key="",
                        sender="@bob:example.org",
                        content={"algorithm": "a.different.algorithm"},
                        event_id="$enable_encryption",
                        room_id="!room1",
                        origin_server_ts=1234567890001,
                    )
                ],
                False,
                None,
            )
        )

        assert subscriber.call_count == 1
        assert subscriber.call_args == mock.call(
            megolm.EncryptionAlgorithmChanged(
                "!room1", megolm.MEGOLM_ALGORITHM, "a.different.algorithm"
            ),
        )
        with pytest.raises(error.EncryptionAlgorithmChangedError):
            encryption_manager.is_room_encrypted("!room1")

        subscriber.reset_mock()
        await c.publisher.publish(
            client.RoomTimelineUpdates(
                "!room1",
                [
                    events.StateEvent(
                        type="m.room.encryption",
                        state_key="",
                        sender="@bob:example.org",
                        content={},
                        event_id="$enable_encryption",
                        room_id="!room1",
                        origin_server_ts=1234567890002,
                    )
                ],
                False,
                None,
            )
        )

        assert subscriber.call_count == 1
        assert subscriber.call_args == mock.call(
            megolm.EncryptionDisabled("!room1", megolm.MEGOLM_ALGORITHM)
        )
        with pytest.raises(error.EncryptionDisabledError):
            encryption_manager.is_room_encrypted("!room1")

        subscriber.reset_mock()
        await c.publisher.publish(
            client.RoomTimelineUpdates(
                "!room1",
                [
                    events.StateEvent(
                        type="m.room.encryption",
                        state_key="",
                        sender="@bob:example.org",
                        content={"algorithm": megolm.MEGOLM_ALGORITHM},
                        event_id="$enable_encryption",
                        room_id="!room1",
                        origin_server_ts=1234567890003,
                    )
                ],
                False,
                None,
            )
        )

        assert subscriber.call_count == 1
        assert subscriber.call_args == mock.call(
            megolm.EncryptionEnabled("!room1", megolm.MEGOLM_ALGORITHM)
        )
        assert encryption_manager.is_room_encrypted("!room1") == True

        subscriber.reset_mock()
        await c.publisher.publish(
            client.RoomTimelineUpdates(
                "!room2",
                [
                    events.StateEvent(
                        type="m.room.encryption",
                        state_key="",
                        sender="@bob:example.org",
                        content={"algorithm": "an.unknown.algorithm"},
                        event_id="$enable_encryption",
                        room_id="!room2",
                        origin_server_ts=1234567890004,
                    )
                ],
                False,
                None,
            )
        )

        assert subscriber.call_count == 1
        assert subscriber.call_args == mock.call(
            megolm.UnknownRoomEncryptionAlgorithm("!room2", "an.unknown.algorithm")
        )
        with pytest.raises(error.UnknownEncryptionAlgorithmError):
            encryption_manager.is_room_encrypted("!room2")


@pytest.mark.asyncio
async def test_send_olm_encrypted_events(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            async with client.Client(
                storage={
                    "access_token": "anaccesstoken",
                    "user_id": "@carol:example.org",
                    "device_id": "OPQRSTU",
                },
                callbacks={},
                base_client_url="https://matrix-client.example.org/_matrix/client/",
            ) as carol:
                (
                    alice_device_keys,
                    alice_otks,
                    alice_device_keys_manager,
                ) = await _create_device_keys(mock_aioresponse, alice)

                (
                    bob_device_keys,
                    bob_otks,
                    bob_device_keys_manager,
                ) = await _create_device_keys(mock_aioresponse, bob)

                (
                    carol_device_keys,
                    carol_otks,
                    carol_device_keys_manager,
                ) = await _create_device_keys(mock_aioresponse, carol)

                alice_device_tracker = devices.DeviceTracker(alice)

                encryption_manager = megolm.OlmMegolmEncryptionManager(
                    alice,
                    b"\x00" * 32,
                    alice_device_keys_manager,
                    alice_device_tracker,
                )

                to_device: dict[typing.Tuple[str, str], typing.Any] = {}

                def send_to_device_callback(url, **kwargs):
                    nonlocal to_device

                    for user_id, device_messages in kwargs["json"]["messages"].items():
                        for device_id, message in device_messages.items():
                            to_device.setdefault((user_id, device_id), []).append(
                                message
                            )

                    return aioresponses.CallbackResult(
                        status=200,
                        body="{}",
                        headers={"Content-Type": "application/json"},
                    )

                send_to_device_url_pattern = re.compile(
                    r"^https://matrix-client.example.org/_matrix/client/v3/sendToDevice/m.room.encrypted/.*$"
                )
                mock_aioresponse.put(
                    send_to_device_url_pattern,
                    callback=send_to_device_callback,
                    repeat=True,
                )

                def keys_claim_1_callback(url, **kwargs):
                    nonlocal bob_otks

                    if kwargs["json"]["one_time_keys"] == {
                        "@bob:example.org": {"HIJKLMN": "signed_curve25519"}
                    }:
                        otk_id, otk = [
                            (id, key)
                            for id, key in bob_otks.items()
                            if id.startswith("signed_curve25519:")
                        ][0]

                        return aioresponses.CallbackResult(
                            status=200,
                            body=json.dumps(
                                {
                                    "one_time_keys": {
                                        "@bob:example.org": {"HIJKLMN": {otk_id: otk}}
                                    }
                                }
                            ),
                            headers={"Content-Type": "application/json"},
                        )
                    else:
                        return aioresponses.CallbackResult(
                            status=400,
                            body="unexpected response",
                        )

                mock_aioresponse.post(
                    "https://matrix-client.example.org/_matrix/client/v3/keys/claim",
                    callback=keys_claim_1_callback,
                )

                await encryption_manager._encrypt_and_send_to_device(
                    [(bob_device_keys, "m.room.message", {"content": "Hello world!"})]
                )

                def keys_claim_2_callback(url, **kwargs):
                    nonlocal carol_otks

                    if kwargs["json"]["one_time_keys"] == {
                        "@carol:example.org": {"OPQRSTU": "signed_curve25519"}
                    }:
                        otk_id, otk = [
                            (id, key)
                            for id, key in carol_otks.items()
                            if id.startswith("signed_curve25519:")
                        ][0]

                        return aioresponses.CallbackResult(
                            status=200,
                            body=json.dumps(
                                {
                                    "one_time_keys": {
                                        "@carol:example.org": {"OPQRSTU": {otk_id: otk}}
                                    }
                                }
                            ),
                            headers={"Content-Type": "application/json"},
                        )
                    else:
                        return aioresponses.CallbackResult(
                            status=400,
                            body="unexpected response",
                        )

                mock_aioresponse.post(
                    "https://matrix-client.example.org/_matrix/client/v3/keys/claim",
                    callback=keys_claim_2_callback,
                )

                await encryption_manager._encrypt_and_send_to_device(
                    [
                        (bob_device_keys, "m.room.message", {"content": "Hello!"}),
                        (carol_device_keys, "m.room.message", {"content": "Hello!"}),
                    ]
                )

                bob_events = to_device[("@bob:example.org", "HIJKLMN")]

                assert len(bob_events) == 2

                (
                    bob_channel,
                    bob_decrypted_event_0,
                ) = olm.OlmChannel.create_from_encrypted_event(
                    bob,
                    bob_device_keys_manager,
                    "@alice:example.org",
                    alice_device_keys_manager.identity_key,
                    bob_events[0],
                    partner_device_id="ABCDEFG",
                    partner_fingerprint_key=alice_device_keys_manager.fingerprint_key,
                )

                assert bob_decrypted_event_0["content"] == {"content": "Hello world!"}

                assert bob_channel.decrypt(bob_events[1])["content"] == {
                    "content": "Hello!"
                }

                carol_events = to_device[("@carol:example.org", "OPQRSTU")]

                assert len(carol_events) == 1

                (
                    carol_channel,
                    carol_decrypted_event_0,
                ) = olm.OlmChannel.create_from_encrypted_event(
                    carol,
                    carol_device_keys_manager,
                    "@alice:example.org",
                    alice_device_keys_manager.identity_key,
                    carol_events[0],
                    partner_device_id="ABCDEFG",
                    partner_fingerprint_key=alice_device_keys_manager.fingerprint_key,
                )

                assert carol_decrypted_event_0["content"] == {"content": "Hello!"}


@pytest.mark.asyncio
async def test_send_megolm_encrypted_event_to_room(mock_aioresponse):
    room_state = {
        "m.room.encryption": {
            "": {
                "room_id": "!room_id",
                "type": "m.room.encryption",
                "state_key": "",
                "sender": "@alice:example.org",
                "event_id": "$encryption_event",
                "origin_server_ts": 1234567890123,
                "content": {
                    "algorithm": megolm.MEGOLM_ALGORITHM,
                    "rotation_period_msgs": 2,
                },
            },
        },
        "m.room.member": {
            "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
            "@bob:example.org": BOB_ROOM_MEMBERSHIP,
        },
    }
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": room_state,
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
                "room_state_tracker": {
                    "!room_id": room_state,
                },
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            (
                alice_device_keys,
                alice_otks,
                alice_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, alice)

            (
                bob_device_keys,
                bob_otks,
                bob_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, bob)

            bob.storage["device_tracker.cache.@alice:example.org"] = alice.storage[
                "device_tracker.cache.@alice:example.org"
            ] = {
                "device_keys": {
                    "ABCDEFG": alice_device_keys,
                }
            }

            bob.storage["device_tracker.cache.@bob:example.org"] = alice.storage[
                "device_tracker.cache.@bob:example.org"
            ] = {
                "device_keys": {
                    "HIJKLMN": bob_device_keys,
                }
            }

            alice_device_tracker = devices.DeviceTracker(alice)

            alice_encryption_manager = megolm.OlmMegolmEncryptionManager(
                alice,
                b"\x00" * 32,
                alice_device_keys_manager,
                alice_device_tracker,
            )

            bob_device_tracker = devices.DeviceTracker(alice)

            bob_encryption_manager = megolm.OlmMegolmEncryptionManager(
                bob,
                b"\x00" * 32,
                bob_device_keys_manager,
                bob_device_tracker,
            )

            async def send_to_device_callback(url, **kwargs):
                nonlocal bob

                messages = kwargs["json"]["messages"]

                to_bob = messages.get("@bob:example.org", {}).get("HIJKLMN")
                if to_bob:
                    await bob.publisher.publish(
                        client.ToDeviceEvents(
                            [
                                events.Event(
                                    sender="@alice:example.org",
                                    type="m.room.encrypted",
                                    content=to_bob,
                                )
                            ]
                        )
                    )

                return aioresponses.CallbackResult(
                    status=200,
                    body="{}",
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            send_to_device_url_pattern = re.compile(
                r"^https://matrix-client.example.org/_matrix/client/v3/sendToDevice/m.room.encrypted/.*$"
            )
            mock_aioresponse.put(
                send_to_device_url_pattern,
                callback=send_to_device_callback,
                repeat=True,
            )

            room_events = []

            def send_event_callback(url, **kwargs):
                nonlocal bob, room_events

                match = re.search(
                    "^/_matrix/client/v3/rooms/([^/]+)/send/([^/]+)/",
                    url.raw_path,
                )

                room_id = urllib.parse.unquote(match.group(1))
                event_type = urllib.parse.unquote(match.group(2))

                room_events.append((room_id, event_type, kwargs["json"]))

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"event_id": "$event_id"}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            send_event_url_pattern = re.compile(
                r"^https://matrix-client.example.org/_matrix/client/v3/rooms/[^/]+/send/.*$"
            )
            mock_aioresponse.put(
                send_event_url_pattern,
                callback=send_event_callback,
                repeat=True,
            )

            def keys_claim_callback(url, **kwargs):
                nonlocal bob_otks

                if kwargs["json"]["one_time_keys"] == {
                    "@bob:example.org": {"HIJKLMN": "signed_curve25519"}
                }:
                    otk_id, otk = [
                        (id, key)
                        for id, key in bob_otks.items()
                        if id.startswith("signed_curve25519:")
                    ][0]

                    return aioresponses.CallbackResult(
                        status=200,
                        body=json.dumps(
                            {
                                "one_time_keys": {
                                    "@bob:example.org": {"HIJKLMN": {otk_id: otk}}
                                }
                            }
                        ),
                        headers={"Content-Type": "application/json"},
                    )
                else:
                    return aioresponses.CallbackResult(
                        status=400,
                        body="unexpected response",
                    )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/claim",
                callback=keys_claim_callback,
            )

            await alice_encryption_manager.encrypt_and_send_room_event(
                "!room_id",
                "m.room.message",
                {
                    "body": "Hello World!",
                    "msgtype": "m.text",
                },
            )

            assert len(room_events) == 1

            room_id, event_type, content = room_events[0]

            decrypted = await bob_encryption_manager.decrypt_room_event(
                events.RoomEvent(
                    type=event_type,
                    sender="@alice:example.org",
                    room_id=room_id,
                    content=content,
                    event_id="$event_id",
                    origin_server_ts=1234567890123,
                )
            )

            assert decrypted.content == {
                "body": "Hello World!",
                "msgtype": "m.text",
            }


@pytest.mark.asyncio
async def test_pre_send_megolm_session(mock_aioresponse):
    room_state = {
        "m.room.encryption": {
            "": {
                "room_id": "!room_id",
                "type": "m.room.encryption",
                "state_key": "",
                "sender": "@alice:example.org",
                "event_id": "$encryption_event",
                "origin_server_ts": 1234567890123,
                "content": {
                    "algorithm": megolm.MEGOLM_ALGORITHM,
                    "rotation_period_msgs": 2,
                },
            },
        },
        "m.room.member": {
            "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
            "@bob:example.org": BOB_ROOM_MEMBERSHIP,
        },
    }
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": room_state,
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
                "room_state_tracker": {
                    "!room_id": room_state,
                },
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            (
                alice_device_keys,
                alice_otks,
                alice_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, alice)

            (
                bob_device_keys,
                bob_otks,
                bob_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, bob)

            bob.storage["device_tracker.cache.@alice:example.org"] = alice.storage[
                "device_tracker.cache.@alice:example.org"
            ] = {
                "device_keys": {
                    "ABCDEFG": alice_device_keys,
                }
            }

            bob.storage["device_tracker.cache.@bob:example.org"] = alice.storage[
                "device_tracker.cache.@bob:example.org"
            ] = {
                "device_keys": {
                    "HIJKLMN": bob_device_keys,
                }
            }

            alice_device_tracker = devices.DeviceTracker(alice)

            alice_encryption_manager = megolm.OlmMegolmEncryptionManager(
                alice,
                b"\x00" * 32,
                alice_device_keys_manager,
                alice_device_tracker,
            )

            bob_device_tracker = devices.DeviceTracker(alice)

            bob_encryption_manager = megolm.OlmMegolmEncryptionManager(
                bob,
                b"\x00" * 32,
                bob_device_keys_manager,
                bob_device_tracker,
            )

            to_device_count = 0

            async def send_to_device_callback(url, **kwargs):
                nonlocal bob, to_device_count

                messages = kwargs["json"]["messages"]

                to_bob = messages.get("@bob:example.org", {}).get("HIJKLMN")
                if to_bob:
                    to_device_count = to_device_count + 1
                    await bob.publisher.publish(
                        client.ToDeviceEvents(
                            [
                                events.Event(
                                    sender="@alice:example.org",
                                    type="m.room.encrypted",
                                    content=to_bob,
                                )
                            ]
                        )
                    )

                return aioresponses.CallbackResult(
                    status=200,
                    body="{}",
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            send_to_device_url_pattern = re.compile(
                r"^https://matrix-client.example.org/_matrix/client/v3/sendToDevice/m.room.encrypted/.*$"
            )
            mock_aioresponse.put(
                send_to_device_url_pattern,
                callback=send_to_device_callback,
                repeat=True,
            )

            room_events = []

            def send_event_callback(url, **kwargs):
                nonlocal bob, room_events

                match = re.search(
                    "^/_matrix/client/v3/rooms/([^/]+)/send/([^/]+)/",
                    url.raw_path,
                )

                room_id = urllib.parse.unquote(match.group(1))
                event_type = urllib.parse.unquote(match.group(2))

                room_events.append((room_id, event_type, kwargs["json"]))

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"event_id": "$event_id"}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            send_event_url_pattern = re.compile(
                r"^https://matrix-client.example.org/_matrix/client/v3/rooms/[^/]+/send/.*$"
            )
            mock_aioresponse.put(
                send_event_url_pattern,
                callback=send_event_callback,
                repeat=True,
            )

            def keys_claim_callback(url, **kwargs):
                nonlocal bob_otks

                if kwargs["json"]["one_time_keys"] == {
                    "@bob:example.org": {"HIJKLMN": "signed_curve25519"}
                }:
                    otk_id, otk = [
                        (id, key)
                        for id, key in bob_otks.items()
                        if id.startswith("signed_curve25519:")
                    ][0]

                    return aioresponses.CallbackResult(
                        status=200,
                        body=json.dumps(
                            {
                                "one_time_keys": {
                                    "@bob:example.org": {"HIJKLMN": {otk_id: otk}}
                                }
                            }
                        ),
                        headers={"Content-Type": "application/json"},
                    )
                else:
                    return aioresponses.CallbackResult(
                        status=400,
                        body="unexpected response",
                    )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/claim",
                callback=keys_claim_callback,
            )

            await alice_encryption_manager.pre_send_megolm_session("!room_id")

            assert to_device_count == 1

            await alice_encryption_manager.encrypt_and_send_room_event(
                "!room_id",
                "m.room.message",
                {
                    "body": "Hello World!",
                    "msgtype": "m.text",
                },
            )

            assert to_device_count == 1
            assert len(room_events) == 1

            room_id, event_type, content = room_events[0]

            decrypted = await bob_encryption_manager.decrypt_room_event(
                events.RoomEvent(
                    type=event_type,
                    sender="@alice:example.org",
                    room_id=room_id,
                    content=content,
                    event_id="$event_id",
                    origin_server_ts=1234567890123,
                )
            )

            assert decrypted.content == {
                "body": "Hello World!",
                "msgtype": "m.text",
            }


@pytest.mark.asyncio
async def test_discard_session(mock_aioresponse):
    room_state = {
        "m.room.encryption": {
            "": {
                "room_id": "!room_id",
                "type": "m.room.encryption",
                "state_key": "",
                "sender": "@alice:example.org",
                "event_id": "$encryption_event",
                "origin_server_ts": 1234567890123,
                "content": {
                    "algorithm": megolm.MEGOLM_ALGORITHM,
                    "rotation_period_msgs": 2,
                },
            },
        },
        "m.room.member": {
            "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
            "@bob:example.org": BOB_ROOM_MEMBERSHIP,
        },
    }
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "room_state_tracker": {
                "!room_id": {
                    "m.room.encryption": {
                        "": ROOM_ENCRYPTION_EVENT,
                    },
                    "m.room.member": {
                        "@alice:example.org": ALICE_ROOM_MEMBERSHIP,
                        "@bob:example.org": BOB_ROOM_MEMBERSHIP,
                    },
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
                "room_state_tracker": {
                    "!room_id": room_state,
                },
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            (
                alice_device_keys,
                alice_otks,
                alice_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, alice)

            (
                bob_device_keys,
                bob_otks,
                bob_device_keys_manager,
            ) = await _create_device_keys(mock_aioresponse, bob)

            bob.storage["device_tracker.cache.@alice:example.org"] = alice.storage[
                "device_tracker.cache.@alice:example.org"
            ] = {
                "device_keys": {
                    "ABCDEFG": alice_device_keys,
                }
            }

            bob.storage["device_tracker.cache.@bob:example.org"] = alice.storage[
                "device_tracker.cache.@bob:example.org"
            ] = {
                "device_keys": {
                    "HIJKLMN": bob_device_keys,
                }
            }

            alice_device_tracker = devices.DeviceTracker(alice)

            alice_encryption_manager = megolm.OlmMegolmEncryptionManager(
                alice,
                b"\x00" * 32,
                alice_device_keys_manager,
                alice_device_tracker,
            )

            bob_device_tracker = devices.DeviceTracker(alice)

            bob_encryption_manager = megolm.OlmMegolmEncryptionManager(
                bob,
                b"\x00" * 32,
                bob_device_keys_manager,
                bob_device_tracker,
            )

            async def send_to_device_callback(url, **kwargs):
                nonlocal bob

                messages = kwargs["json"]["messages"]

                to_bob = messages.get("@bob:example.org", {}).get("HIJKLMN")
                if to_bob:
                    await bob.publisher.publish(
                        client.ToDeviceEvents(
                            [
                                events.Event(
                                    sender="@alice:example.org",
                                    type="m.room.encrypted",
                                    content=to_bob,
                                )
                            ]
                        )
                    )

                return aioresponses.CallbackResult(
                    status=200,
                    body="{}",
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            send_to_device_url_pattern = re.compile(
                r"^https://matrix-client.example.org/_matrix/client/v3/sendToDevice/m.room.encrypted/.*$"
            )
            mock_aioresponse.put(
                send_to_device_url_pattern,
                callback=send_to_device_callback,
                repeat=True,
            )

            room_events = []

            def send_event_callback(url, **kwargs):
                nonlocal bob, room_events

                match = re.search(
                    "^/_matrix/client/v3/rooms/([^/]+)/send/([^/]+)/",
                    url.raw_path,
                )

                room_id = urllib.parse.unquote(match.group(1))
                event_type = urllib.parse.unquote(match.group(2))

                room_events.append((room_id, event_type, kwargs["json"]))

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"event_id": "$event_id"}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            send_event_url_pattern = re.compile(
                r"^https://matrix-client.example.org/_matrix/client/v3/rooms/[^/]+/send/.*$"
            )
            mock_aioresponse.put(
                send_event_url_pattern,
                callback=send_event_callback,
                repeat=True,
            )

            def keys_claim_callback(url, **kwargs):
                nonlocal bob_otks

                if kwargs["json"]["one_time_keys"] == {
                    "@bob:example.org": {"HIJKLMN": "signed_curve25519"}
                }:
                    otk_id, otk = [
                        (id, key)
                        for id, key in bob_otks.items()
                        if id.startswith("signed_curve25519:")
                    ][0]

                    return aioresponses.CallbackResult(
                        status=200,
                        body=json.dumps(
                            {
                                "one_time_keys": {
                                    "@bob:example.org": {"HIJKLMN": {otk_id: otk}}
                                }
                            }
                        ),
                        headers={"Content-Type": "application/json"},
                    )
                else:
                    return aioresponses.CallbackResult(
                        status=400,
                        body="unexpected response",
                    )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/claim",
                callback=keys_claim_callback,
            )

            await alice_encryption_manager.encrypt_and_send_room_event(
                "!room_id",
                "m.room.message",
                {
                    "body": "Hello World!",
                    "msgtype": "m.text",
                },
            )

            alice_encryption_manager.discard_session("!room_id")

            await alice_encryption_manager.encrypt_and_send_room_event(
                "!room_id",
                "m.room.message",
                {
                    "body": "Hello again!",
                    "msgtype": "m.text",
                },
            )

            (_, _, event_content0) = room_events[0]
            (_, _, event_content1) = room_events[1]

            assert event_content0["session_id"] != event_content1["session_id"]
