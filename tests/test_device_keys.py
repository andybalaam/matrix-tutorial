# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import asyncio
import aioresponses
import cryptography
import json
import pytest

from matrixlib import client
from matrixlib import devices


@pytest.mark.asyncio
async def test_device_keys_upload(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:

        def callback(url, **kwargs):
            assert "device_keys" in kwargs["json"]

            return aioresponses.CallbackResult(
                status=200,
                body='{"one_time_key_counts":{"signed_curve25519":100}}',
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            callback=callback,
        )
        manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        await asyncio.sleep(0.1)
        mock_aioresponse.assert_called()


@pytest.mark.asyncio
async def test_sign_and_verify(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        device_keys = None

        def callback(url, **kwargs):
            nonlocal device_keys
            device_keys = kwargs["json"]["device_keys"]

            return aioresponses.CallbackResult(
                status=200,
                body='{"one_time_key_counts":{"signed_curve25519":100}}',
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            callback=callback,
        )
        manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        await asyncio.sleep(0.1)

        devices.verify_json_ed25519(
            manager.fingerprint_key,
            "@alice:example.org",
            "ABCDEFG",
            device_keys,
        )

        # should ignore anything in "unsigned"
        device_keys["unsigned"] = {"foo": "bar"}
        devices.verify_json_ed25519(
            manager.fingerprint_key,
            "@alice:example.org",
            "ABCDEFG",
            device_keys,
        )

        # other modifications should result in a bad signature
        device_keys["new_key"] = 1
        with pytest.raises(cryptography.exceptions.InvalidSignature):
            devices.verify_json_ed25519(
                manager.fingerprint_key,
                "@alice:example.org",
                "ABCDEFG",
                device_keys,
            )


@pytest.mark.asyncio
async def test_otk_tracking(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        otk_count = 0

        async def callback1(url, **kwargs):
            # manager will make an initial request to upload device keys and otks
            nonlocal otk_count

            assert "one_time_keys" in kwargs["json"]

            otk_count = len(kwargs["json"]["one_time_keys"])

            # pretend a sync response came in
            await c.publisher.publish(
                client.OneTimeKeysCount(
                    {"signed_curve25519": 0},
                    False,
                )
            )

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "one_time_key_counts": {
                            "signed_curve25519": otk_count,
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            callback=callback1,
        )

        def callback2(url, **kwargs):
            # manager should make an empty request to get the current count
            nonlocal otk_count

            assert kwargs["json"] == {}

            otk_count = otk_count - 7

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "one_time_key_counts": {
                            "signed_curve25519": otk_count,
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            callback=callback2,
        )

        max_reached_event = asyncio.Event()

        async def callback3(url, **kwargs):
            # manager will make upload additional keys until max is reached
            nonlocal otk_count

            # ensure that the client has stored the correct count of one-time keys
            assert c.storage["olm_account.keys_on_server"]["one_time_keys"] == otk_count

            assert "one_time_keys" in kwargs["json"]

            otk_count = otk_count + len(kwargs["json"]["one_time_keys"])

            if otk_count == manager.account.max_number_of_one_time_keys:
                max_reached_event.set()

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "one_time_key_counts": {
                            "signed_curve25519": otk_count,
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
            callback=callback3,
            repeat=True,
        )

        manager = devices.DeviceKeysManager(c, b"\x00" * 32)
        await max_reached_event.wait()
