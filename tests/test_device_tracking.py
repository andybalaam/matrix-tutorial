# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import asyncio
import aioresponses
import json
import pytest

from matrixlib import client
from matrixlib import devices


@pytest.mark.asyncio
async def test_basic_device_tracking(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "device_tracker.tracked_users": {
                "@bob:example.org": True,
                "@carol:example.org": True,
            },
            "device_tracker.cache.@bob:example.org": {
                "device_keys": {
                    "HIJKLMN": {
                        "algorithms": [],
                        "device_id": "HIJKLMN",
                        "keys": {
                            "curve25519:HIJKLMN": "some+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                },
            },
            "device_tracker.cache.@carol:example.org": {
                "device_keys": {
                    "OPQRSTU": {
                        "algorithms": [],
                        "device_id": "OPQRSTU",
                        "keys": {
                            "curve25519:OPQRSTU": "some+other+key",
                        },
                        "user_id": "@carol:example.org",
                    },
                },
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        tracker = devices.DeviceTracker(c)

        assert await tracker.get_device_keys(
            ["@bob:example.org", "@carol:example.org"]
        ) == {
            "@bob:example.org": devices.UserDeviceKeysResult(
                {
                    "HIJKLMN": {
                        "algorithms": [],
                        "device_id": "HIJKLMN",
                        "keys": {
                            "curve25519:HIJKLMN": "some+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                }
            ),
            "@carol:example.org": devices.UserDeviceKeysResult(
                {
                    "OPQRSTU": {
                        "algorithms": [],
                        "device_id": "OPQRSTU",
                        "keys": {
                            "curve25519:OPQRSTU": "some+other+key",
                        },
                        "user_id": "@carol:example.org",
                    },
                },
            ),
        }

        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?timeout=30000",
            status=200,
            body=json.dumps(
                {
                    "device_lists": {
                        "changed": ["@bob:example.org"],
                    },
                    "next_batch": "token1",
                }
            ),
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/sync?since=token1&timeout=30000",
            status=200,
            body='{"next_batch":"token1"}',
            headers={
                "content-type": "application/json",
            },
            repeat=True,
        )

        def subscriber(msg) -> None:
            c.stop_sync()

        c.publisher.subscribe((client.DeviceChanges, client.SyncFailed), subscriber)

        c.start_sync()

        try:
            await c.sync_task
        except asyncio.CancelledError:
            pass

        def callback(url, **kwargs):
            assert kwargs["json"] == {"device_keys": {"@bob:example.org": []}}

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "device_keys": {
                            "@bob:example.org": {
                                "VWXYZAB": {
                                    "algorithms": [],
                                    "device_id": "VWXYZAB",
                                    "keys": {
                                        "curve25519:HIJKLMN": "some+new+key",
                                    },
                                    "user_id": "@bob:example.org",
                                },
                            },
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/query",
            callback=callback,
        )

        assert await tracker.get_device_keys(
            ["@bob:example.org", "@carol:example.org"]
        ) == {
            "@bob:example.org": devices.UserDeviceKeysResult(
                {
                    "VWXYZAB": {
                        "algorithms": [],
                        "device_id": "VWXYZAB",
                        "keys": {
                            "curve25519:HIJKLMN": "some+new+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                },
            ),
            "@carol:example.org": devices.UserDeviceKeysResult(
                {
                    "OPQRSTU": {
                        "algorithms": [],
                        "device_id": "OPQRSTU",
                        "keys": {
                            "curve25519:OPQRSTU": "some+other+key",
                        },
                        "user_id": "@carol:example.org",
                    },
                },
            ),
        }


@pytest.mark.asyncio
async def test_concurrent_device_requests(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "device_tracker.tracked_users": {
                "@bob:example.org": True,
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        tracker = devices.DeviceTracker(c)

        second_request_task = None

        async def callback(url, **kwargs):
            assert kwargs["json"] == {"device_keys": {"@bob:example.org": []}}

            nonlocal second_request_task
            second_request_task = asyncio.create_task(
                tracker.get_device_keys(["@bob:example.org"])
            )

            # give the second request some time to execute to make sure that it blocks
            # on the future
            await asyncio.sleep(0.2)

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "device_keys": {
                            "@bob:example.org": {
                                "HIJKLMN": {
                                    "algorithms": [],
                                    "device_id": "HIJKLMN",
                                    "keys": {
                                        "curve25519:HIJKLMN": "some+key",
                                    },
                                    "user_id": "@bob:example.org",
                                },
                            },
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/query",
            callback=callback,
        )

        assert await tracker.get_device_keys(["@bob:example.org"]) == {
            "@bob:example.org": devices.UserDeviceKeysResult(
                {
                    "HIJKLMN": {
                        "algorithms": [],
                        "device_id": "HIJKLMN",
                        "keys": {
                            "curve25519:HIJKLMN": "some+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                },
            ),
        }

        assert await second_request_task == {
            "@bob:example.org": devices.UserDeviceKeysResult(
                {
                    "HIJKLMN": {
                        "algorithms": [],
                        "device_id": "HIJKLMN",
                        "keys": {
                            "curve25519:HIJKLMN": "some+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                },
            ),
        }


@pytest.mark.asyncio
async def test_update_during_device_request(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "device_tracker.tracked_users": {
                "@bob:example.org": True,
            },
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        tracker = devices.DeviceTracker(c)

        async def callback1(url, **kwargs):
            assert kwargs["json"] == {"device_keys": {"@bob:example.org": []}}

            await c.publisher.publish(client.DeviceChanges(["@bob:example.org"], []))

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "device_keys": {
                            "@bob:example.org": {
                                "HIJKLMN": {
                                    "algorithms": [],
                                    "device_id": "HIJKLMN",
                                    "keys": {
                                        "curve25519:HIJKLMN": "some+key",
                                    },
                                    "user_id": "@bob:example.org",
                                },
                            },
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/query",
            callback=callback1,
        )

        def callback2(url, **kwargs):
            assert kwargs["json"] == {"device_keys": {"@bob:example.org": []}}

            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "device_keys": {
                            "@bob:example.org": {
                                "VWXYZAB": {
                                    "algorithms": [],
                                    "device_id": "VWXYZAB",
                                    "keys": {
                                        "curve25519:HIJKLMN": "some+new+key",
                                    },
                                    "user_id": "@bob:example.org",
                                },
                            },
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/query",
            callback=callback2,
        )

        assert await tracker.get_device_keys(["@bob:example.org"]) == {
            "@bob:example.org": devices.UserDeviceKeysResult(
                {
                    "HIJKLMN": {
                        "algorithms": [],
                        "device_id": "HIJKLMN",
                        "keys": {
                            "curve25519:HIJKLMN": "some+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                },
            ),
        }

        assert await tracker.get_device_keys(["@bob:example.org"]) == {
            "@bob:example.org": devices.UserDeviceKeysResult(
                {
                    "VWXYZAB": {
                        "algorithms": [],
                        "device_id": "VWXYZAB",
                        "keys": {
                            "curve25519:HIJKLMN": "some+new+key",
                        },
                        "user_id": "@bob:example.org",
                    },
                },
            ),
        }
