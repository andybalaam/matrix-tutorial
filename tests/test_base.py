# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import aiohttp
import pytest
import time
import typing
import unittest.mock as mock

from matrixlib import client
from matrixlib import error
from matrixlib import schema


@pytest.mark.asyncio
async def test_check_response(mock_aioresponse):
    async with aiohttp.ClientSession() as session:
        mock_aioresponse.get(
            "http://example.org/ok",
            status=200,
            body="{}",
            headers={
                "content-type": "application/json",
            },
        )
        async with session.get("http://example.org/ok") as resp:
            assert await client.check_response(resp) == (200, {})

        mock_aioresponse.get(
            "http://example.org/not_json",
            status=200,
            body="OK",
            headers={
                "content-type": "text/plain",
            },
        )
        with pytest.raises(error.NotMatrixServerError):
            async with session.get("http://example.org/not_json") as resp:
                await client.check_response(resp)

        mock_aioresponse.get(
            "http://example.org/not_json_content_type",
            status=200,
            body="{}",
            headers={
                "content-type": "text/plain",
            },
        )
        with pytest.raises(error.NotMatrixServerError):
            async with session.get("http://example.org/not_json_content_type") as resp:
                await client.check_response(resp)

        mock_aioresponse.get(
            "http://example.org/matrix_error",
            status=400,
            body='{"errcode":"M_UNKNOWN","error":"Unknown"}',
            headers={
                "content-type": "application/json",
            },
        )
        with pytest.raises(error.MatrixError):
            async with session.get("http://example.org/matrix_error") as resp:
                await client.check_response(resp)


@pytest.mark.asyncio
async def test_retry(mock_aioresponse):
    async with aiohttp.ClientSession() as session:
        mock_aioresponse.get("http://example.org/", status=504, body="Timeout")
        mock_aioresponse.get("http://example.org/", status=504, body="Timeout")
        mock_aioresponse.get("http://example.org/", status=200, body="OK")
        # mock asyncio.sleep so that the test doesn't actually need to wait for
        # multiple seconds
        with mock.patch("asyncio.sleep") as sleep:
            async with await client.retry(
                10_000, session.get, "http://example.org/"
            ) as resp:
                # check that we got the final result
                assert resp.status == 200
                assert await resp.text() == "OK"
                # check that our mocked asyncio.sleep was called and that we're
                # backing off exponentially
                assert sleep.call_args_list == [mock.call(2), mock.call(4)]

        mock_aioresponse.get(
            "http://example.org/",
            status=429,
            body='{"errcode":"M_LIMIT_EXCEEDED","retry_after_ms":100}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get("http://example.org/", status=200, body="OK")
        with mock.patch("asyncio.sleep") as sleep:
            resp = await client.retry(10_000, session.get, "http://example.org/")
            async with resp:
                # check that we got the final result
                assert resp.status == 200
                assert await resp.text() == "OK"
                # check that we waited some time before retrying
                assert sleep.call_args == mock.call(1)

        mock_aioresponse.get(
            "http://example.org/",
            status=429,
            body='{"errcode":"M_LIMIT_EXCEEDED"}',
            headers={
                "content-type": "application/json",
                "retry-after": "1",
            },
        )
        mock_aioresponse.get("http://example.org/", status=200, body="OK")
        with mock.patch("asyncio.sleep") as sleep:
            resp = await client.retry(10_000, session.get, "http://example.org/")
            async with resp:
                # check that we got the final result
                assert resp.status == 200
                assert await resp.text() == "OK"
                # check that we waited some time before retrying
                assert sleep.call_args == mock.call(1)

        mock_aioresponse.get(
            "http://example.org/",
            status=429,
            body='{"errcode":"M_LIMIT_EXCEEDED","retry_after_ms":100000000000}',
            headers={
                "content-type": "application/json",
            },
        )
        start_time = time.monotonic_ns()
        resp = await client.retry(10_000, session.get, "http://example.org/")
        async with resp:
            # check that we got the final result
            assert resp.status == 429
            # check that we didn't wait (the request time should be less than 1s)
            assert time.monotonic_ns() < start_time + 1_000_000_000

        mock_aioresponse.get(
            "http://example.org/",
            status=200,
            body="{}",
            headers={
                "content-type": "application/json",
            },
        )
        start_time = time.monotonic_ns()
        resp = await client.retry(10_000, session.get, "http://example.org/")
        async with resp:
            # check that we got the final result
            assert resp.status == 200
            # check that we didn't wait (the request time should be less than 1s)
            assert time.monotonic_ns() < start_time + 1_000_000_000

        mock_aioresponse.get(
            "http://example.org/",
            status=404,
            body='{"errcode":"M_NOT_FOUND"}',
            headers={
                "content-type": "application/json",
            },
        )
        start_time = time.monotonic_ns()
        resp = await client.retry(10_000, session.get, "http://example.org/")
        async with resp:
            # check that we got the final result
            assert resp.status == 404
            # check that we didn't wait (the request time should be less than 1s)
            assert time.monotonic_ns() < start_time + 1_000_000_000


def test_schema():
    assert schema.is_valid(1, typing.Any)
    assert schema.is_valid(1, int)
    assert not schema.is_valid(1, str)
    assert schema.is_valid([1], list)

    assert schema.is_valid({"foo": "bar"}, {"foo": str})
    assert not schema.is_valid({"foo": 1}, {"foo": str})
    assert not schema.is_valid({}, {"foo": str})
    assert not schema.is_valid(1, {})

    assert schema.is_valid({"foo": "bar"}, {"foo": schema.Optional(str)})
    assert not schema.is_valid({"foo": 1}, {"foo": schema.Optional(str)})
    assert schema.is_valid({}, {"foo": schema.Optional(str)})

    assert schema.is_valid(1, schema.Union(str, int, bool))
    assert schema.is_valid("foo", schema.Union(str, int, bool))
    assert schema.is_valid(True, schema.Union(str, int, bool))
    assert not schema.is_valid([], schema.Union(str, int, bool))
    assert not schema.is_valid({}, schema.Union(str, int, bool))

    assert schema.is_valid([1, 2], list[int])
    assert not schema.is_valid([1, "foo"], list[int])
    assert not schema.is_valid(1, list[int])
    assert schema.is_valid({"foo": True, "bar": False}, dict[str, bool])
    assert not schema.is_valid({"foo": True, "bar": 1}, dict[str, bool])
    assert not schema.is_valid(1, dict[str, bool])

    assert schema.is_valid([1, True], schema.Array(schema.Union(int, bool)))
    assert not schema.is_valid([1, "foo"], schema.Array(schema.Union(int, bool)))
    assert schema.is_valid(
        {"foo": 1, "bar": True}, schema.Object(schema.Union(int, bool))
    )
    assert not schema.is_valid(
        {"foo": 1, "bar": "baz"}, schema.Object(schema.Union(int, bool))
    )


@pytest.mark.asyncio
async def test_discover(mock_aioresponse):
    mock_aioresponse.get(
        "https://example.org/.well-known/matrix/client",
        status=200,
        body='{"m.homeserver":{"base_url":"https://matrix-client.example.org"}}',
        headers={
            "content-type": "application/json",
        },
    )
    mock_aioresponse.get(
        "https://matrix-client.example.org/_matrix/client/versions",
        status=200,
        body='{"versions":["v1.1"]}',
        headers={
            "content-type": "application/json",
        },
    )
    assert await client.discover("example.org") == (
        "https://matrix-client.example.org/_matrix/client/",
        {"m.homeserver": {"base_url": "https://matrix-client.example.org"}},
        {"versions": ["v1.1"]},
    )

    mock_aioresponse.get(
        "https://example.org/.well-known/matrix/client",
        status=200,
        body='{"m.homeserver":{"base_url":"https://matrix-client.example.org"}}',
        headers={
            "content-type": "text/plain",
        },
    )
    mock_aioresponse.get(
        "https://matrix-client.example.org/_matrix/client/versions",
        status=200,
        body='{"versions":["v1.1"]}',
        headers={
            "content-type": "application/json",
        },
    )
    assert await client.discover("example.org") == (
        "https://matrix-client.example.org/_matrix/client/",
        {"m.homeserver": {"base_url": "https://matrix-client.example.org"}},
        {"versions": ["v1.1"]},
    )
