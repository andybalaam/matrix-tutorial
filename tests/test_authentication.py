# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import json
import pytest
import time

from matrixlib import client
from matrixlib import error


@pytest.mark.asyncio
async def test_basic_authenticated_requests(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            body='{"displayname":"Alice"}',
            headers={
                "content-type": "application/json",
            },
        )
        resp = await c.authenticated(
            c.http_session.get, c.url("v3/profile/%40alice:example.org")
        )
        async with resp:
            mock_aioresponse.assert_called_with(
                "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
                method="GET",
                headers={"Authorization": "Bearer anaccesstoken"},
            )
            assert resp.status == 200
            assert await resp.json() == {"displayname": "Alice"}


@pytest.mark.asyncio
async def test_refresh_token(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "refresh_token": "a_refresh_token",
            "access_token_valid_until": time.time_ns() - 1000,
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/refresh",
            body=json.dumps(
                {
                    "access_token": "another_access_token",
                    "expires_in_ms": 60000,
                    "refresh_token": "another_refresh_token",
                },
            ),
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            body='{"displayname":"Alice"}',
            headers={
                "content-type": "application/json",
            },
        )

        resp = await c.authenticated(
            c.http_session.get, c.url("v3/profile/%40alice:example.org")
        )
        async with resp:
            mock_aioresponse.assert_called_with(
                "https://matrix-client.example.org/_matrix/client/v3/refresh",
                method="POST",
                json={"refresh_token": "a_refresh_token"},
            )
            mock_aioresponse.assert_called_with(
                "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
                method="GET",
                headers={"Authorization": "Bearer another_access_token"},
            )
            assert resp.status == 200
            assert await resp.json() == {"displayname": "Alice"}
            assert c.storage["refresh_token"] == "another_refresh_token"

    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "refresh_token": "arefreshtoken",
            "access_token_valid_until": time.time_ns() + 60_000_000_000,
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            status=401,
            body='{"errcode":"M_UNKNOWN_TOKEN","soft_logout":true}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/refresh",
            body='{"access_token":"anotheraccesstoken","expires_in_ms":60000,"refresh_token":"another_refresh_token"}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            body='{"displayname":"Alice"}',
            headers={
                "content-type": "application/json",
            },
        )

        resp = await c.authenticated(
            c.http_session.get, c.url("v3/profile/%40alice:example.org")
        )
        async with resp:
            mock_aioresponse.assert_called_with(
                "https://matrix-client.example.org/_matrix/client/v3/refresh",
                method="POST",
                json={"refresh_token": "arefreshtoken"},
            )
            mock_aioresponse.assert_called_with(
                "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
                method="GET",
                headers={"Authorization": "Bearer anotheraccesstoken"},
            )
            assert resp.status == 200
            assert await resp.json() == {"displayname": "Alice"}
            assert c.storage["refresh_token"] == "another_refresh_token"


@pytest.mark.asyncio
async def test_soft_logout(mock_aioresponse):
    # our re-log in callback will just log in using a hard-coded password
    async def re_log_in(cli: client.Client):
        await cli.re_log_in("P4ssw0rd")

    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={"re_log_in": re_log_in},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            status=401,
            body='{"errcode":"M_UNKNOWN_TOKEN","soft_logout":true}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/login",
            status=200,
            payload={
                "user_id": "@alice:example.org",
                "device_id": "ABCDEFG",
                "access_token": "anewaccesstoken",
            },
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            body='{"displayname":"Alice"}',
            headers={
                "content-type": "application/json",
            },
        )

        resp = await c.authenticated(
            c.http_session.get, c.url("v3/profile/%40alice:example.org")
        )
        async with resp:
            # make sure that the request finally succeeded
            assert resp.status == 200
            assert await resp.json() == {"displayname": "Alice"}
            # make sure that we re-logged in and got a new access token
            assert c.storage["access_token"] == "anewaccesstoken"

    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
            "refresh_token": "arefreshtoken",
            "access_token_valid_until": time.time_ns() + 60_000_000_000,
        },
        callbacks={"re_log_in": re_log_in},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            status=401,
            body='{"errcode":"M_UNKNOWN_TOKEN","soft_logout":true}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/refresh",
            status=401,
            body='{"errcode":"M_UNKNOWN_TOKEN","soft_logout":true}',
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/login",
            status=200,
            payload={
                "user_id": "@alice:example.org",
                "device_id": "ABCDEFG",
                "access_token": "anewaccesstoken",
            },
            headers={
                "content-type": "application/json",
            },
        )
        mock_aioresponse.get(
            "https://matrix-client.example.org/_matrix/client/v3/profile/%40alice:example.org",
            body='{"displayname":"Alice"}',
            headers={
                "content-type": "application/json",
            },
        )

        resp = await c.authenticated(
            c.http_session.get, c.url("v3/profile/%40alice:example.org")
        )
        async with resp:
            # make sure that we tried to refresh using the refresh token
            mock_aioresponse.assert_called_with(
                "https://matrix-client.example.org/_matrix/client/v3/refresh",
                method="POST",
                json={"refresh_token": "arefreshtoken"},
            )

            # make sure that the request finally succeeded
            assert resp.status == 200
            assert await resp.json() == {"displayname": "Alice"}
            # make sure we re-logged in and got a new access token
            assert c.storage["access_token"] == "anewaccesstoken"
