# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import json
import pytest
import re

from matrixlib import client
from matrixlib import events
from matrixlib import schema


def test_event_parsing():
    event = {
        "sender": "@alice:example.org",
        "type": "m.room.message",
        "event_id": "$an_event_id",
        "room_id": "!a_room_id",
        "content": {
            "body": "Hello world!",
            "msgtype": "m.text",
        },
        "origin_server_ts": 123567890000,
        "unknown_property": "foo",
    }
    assert events.Event.is_valid(event)
    assert events.RoomEvent.is_valid(event)
    assert not events.StateEvent.is_valid(event)
    parsed_event = events.RoomEvent(**event)
    assert parsed_event.sender == event["sender"]
    assert parsed_event.type == event["type"]
    assert parsed_event.event_id == event["event_id"]
    assert parsed_event.room_id == event["room_id"]
    assert parsed_event.content == event["content"]
    assert parsed_event.origin_server_ts == event["origin_server_ts"]
    assert parsed_event.unsigned == {}
    assert parsed_event == events.RoomEvent(**event)
    assert not (parsed_event != events.RoomEvent(**event))
    assert parsed_event != events.Event(**event)

    event_copy = event.copy()
    del event_copy["unknown_property"]
    event_copy["unsigned"] = {}
    assert parsed_event.to_dict() == event_copy


@pytest.mark.asyncio
async def test_txn_id():
    async with client.Client(
        storage={},
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        first = c.make_txn_id()
        second = c.make_txn_id()
        assert first != second


@pytest.mark.asyncio
async def test_send_event(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        pattern = re.compile(
            r"^https://matrix-client.example.org/_matrix/client/v3/rooms/!roomid/send/m.room.message/.*$"
        )
        mock_aioresponse.put(
            pattern,
            status=200,
            body='{"event_id":"$event_id"}',
            headers={
                "content-type": "application/json",
            },
        )
        event_id, _txn_id = await c.send_event(
            "!roomid", "m.room.message", '{"body":"Hello World!"}'
        )
        assert event_id == "$event_id"


@pytest.mark.asyncio
async def test_send_to_device_event(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:
        pattern = re.compile(
            r"^https://matrix-client.example.org/_matrix/client/v3/sendToDevice/m.room.message/.*$"
        )
        mock_aioresponse.put(
            pattern,
            status=200,
            body="{}",
            headers={
                "content-type": "application/json",
            },
        )
        await c.send_to_device(
            "m.room.message",
            json.dumps({"@alice:example.org": {"HIJKLMNO": {"body": "Hello World!"}}}),
        )
