# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

import asyncio
import aioresponses
import json
import pytest
import vodozemac

from matrixlib import client
from matrixlib import devices
from matrixlib import error
from matrixlib import olm


@pytest.mark.asyncio
async def test_olm(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            alice_device_keys = None
            alice_otks = None

            def alice_callback(url, **kwargs):
                nonlocal alice_device_keys, alice_otks
                alice_device_keys = kwargs["json"]["device_keys"]
                alice_otks = kwargs["json"]["one_time_keys"]

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                callback=alice_callback,
            )
            alice_device_keys_manager = devices.DeviceKeysManager(alice, b"\x00" * 32)
            await asyncio.sleep(0.1)

            bob_device_keys = None
            bob_otks = None

            def bob_callback(url, **kwargs):
                nonlocal bob_device_keys, bob_otks
                bob_device_keys = kwargs["json"]["device_keys"]
                bob_otks = kwargs["json"]["one_time_keys"]

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                callback=bob_callback,
            )
            bob_device_keys_manager = devices.DeviceKeysManager(bob, b"\x00" * 32)
            await asyncio.sleep(0.1)

            # use the first `signed_curve25519` one-time key
            otk = [
                key
                for id, key in bob_otks.items()
                if id.startswith("signed_curve25519:")
            ][0]

            alice_channel = olm.OlmChannel.create_outbound_channel(
                alice,
                alice_device_keys_manager,
                bob_device_keys,
                otk,
            )

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Hello world!"}
            )

            (
                bob_channel,
                alice_msg_decrypted,
            ) = olm.OlmChannel.create_from_encrypted_event(
                bob,
                bob_device_keys_manager,
                "@alice:example.org",
                alice_device_keys_manager.identity_key,
                alice_msg_encrypted,
                partner_device_id="ABCDEFG",
                partner_fingerprint_key=alice_device_keys_manager.fingerprint_key,
            )

            assert alice_msg_decrypted["content"]["body"] == "Hello world!"
            # FIXME: check other fields too

            bob_msg_encrypted = bob_channel.encrypt(
                "m.room.message", {"body": "Bonjour!"}
            )

            bob_msg_decrypted = alice_channel.decrypt(bob_msg_encrypted)

            assert bob_msg_decrypted["content"]["body"] == "Bonjour!"

            # use the first `signed_curve25519` one-time key
            otk = [
                key
                for id, key in alice_otks.items()
                if id.startswith("signed_curve25519:")
            ][0]

            bob_channel.add_outbound_olm_session(otk)

            bob_msg2_encrypted = bob_channel.encrypt(
                "m.room.message", {"body": "Guten Tag!"}
            )

            bob_msg2_decrypted = alice_channel.decrypt(bob_msg2_encrypted)

            assert bob_msg2_decrypted["content"]["body"] == "Guten Tag!"

            assert len(alice_channel.sessions) == 2


@pytest.mark.asyncio
async def test_olm_error_checking(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            alice_device_keys = None
            alice_otks = None

            def alice_callback(url, **kwargs):
                nonlocal alice_device_keys, alice_otks
                alice_device_keys = kwargs["json"]["device_keys"]
                alice_otks = kwargs["json"]["one_time_keys"]

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                callback=alice_callback,
            )
            alice_device_keys_manager = devices.DeviceKeysManager(alice, b"\x00" * 32)
            await asyncio.sleep(0.1)

            bob_device_keys = None
            bob_otks = None

            def bob_callback(url, **kwargs):
                nonlocal bob_device_keys, bob_otks
                bob_device_keys = kwargs["json"]["device_keys"]
                bob_otks = kwargs["json"]["one_time_keys"]

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                callback=bob_callback,
            )
            bob_device_keys_manager = devices.DeviceKeysManager(bob, b"\x00" * 32)
            await asyncio.sleep(0.1)

            # use the first `signed_curve25519` one-time key
            otk = [
                key
                for id, key in bob_otks.items()
                if id.startswith("signed_curve25519:")
            ][0]

            alice_channel = olm.OlmChannel.create_outbound_channel(
                alice,
                alice_device_keys_manager,
                bob_device_keys,
                otk,
            )

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Wrong recipient"}
            )

            ciphertext = alice_msg_encrypted["ciphertext"][
                bob_device_keys_manager.identity_key
            ]
            del alice_msg_encrypted["ciphertext"][bob_device_keys_manager.identity_key]
            alice_msg_encrypted["ciphertext"]["something"] = ciphertext

            with pytest.raises(
                RuntimeError, match="The message is not encrypted for us"
            ):
                olm.OlmChannel.create_from_encrypted_event(
                    bob,
                    bob_device_keys_manager,
                    "@alice:example.org",
                    alice_device_keys_manager.identity_key,
                    alice_msg_encrypted,
                    partner_device_id="ABCDEFG",
                    partner_fingerprint_key=alice_device_keys_manager.fingerprint_key,
                )

            # wrong sender user ID
            alice_channel.client.storage["user_id"] = "@carol:example.org"

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Wrong sender user ID"}
            )

            bob_channel, exc = olm.OlmChannel.create_from_encrypted_event(
                bob,
                bob_device_keys_manager,
                "@alice:example.org",
                alice_device_keys_manager.identity_key,
                alice_msg_encrypted,
                partner_device_id="ABCDEFG",
                partner_fingerprint_key=alice_device_keys_manager.fingerprint_key,
            )

            assert isinstance(exc, RuntimeError)
            assert str(exc) == "Invalid message"

            # set the value back to the right one
            alice_channel.client.storage["user_id"] = "@alice:example.org"

            # wrong recipient user ID
            alice_channel.partner_user_id = "@carol:example.org"

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Wrong recipient user ID"}
            )

            with pytest.raises(RuntimeError, match="Invalid message"):
                bob_channel.decrypt(alice_msg_encrypted)

            # set the value back to the right one
            alice_channel.partner_user_id = "@bob:example.org"

            # wrong recipient fingerprint key
            alice_channel.partner_fingerprint_key = "not the right key"

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Wrong recipient fingerprint key"}
            )

            with pytest.raises(RuntimeError, match="Invalid message"):
                bob_channel.decrypt(alice_msg_encrypted)

            # set the value back to the right one
            alice_channel.partner_fingerprint_key = (
                bob_device_keys_manager.fingerprint_key
            )

            # wrong sender fingerprint key
            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Wrong sender fingerprint key"}
            )

            bob_channel.partner_fingerprint_key = "a different key"

            with pytest.raises(RuntimeError, match="Mismatched fingerprint key"):
                bob_channel.decrypt(alice_msg_encrypted)

            # set the value back to the right one
            bob_channel.partner_fingerprint_key = (
                alice_device_keys_manager.fingerprint_key
            )

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Hello"}
            )

            alice_msg_encrypted["ciphertext"][bob_device_keys_manager.identity_key] = {
                "type": 0,
                "body": "Cannot decrypt this",
            }

            with pytest.raises(error.UnableToDecryptError):
                bob_channel.decrypt(alice_msg_encrypted)


@pytest.mark.asyncio
async def test_load_olm_channel(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as alice:
        async with client.Client(
            storage={
                "access_token": "anaccesstoken",
                "user_id": "@bob:example.org",
                "device_id": "HIJKLMN",
            },
            callbacks={},
            base_client_url="https://matrix-client.example.org/_matrix/client/",
        ) as bob:
            alice_device_keys = None
            alice_otks = None

            def alice_callback(url, **kwargs):
                nonlocal alice_device_keys, alice_otks
                alice_device_keys = kwargs["json"]["device_keys"]
                alice_otks = kwargs["json"]["one_time_keys"]

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                callback=alice_callback,
            )
            alice_device_keys_manager = devices.DeviceKeysManager(alice, b"\x00" * 32)
            await asyncio.sleep(0.1)

            bob_device_keys = None
            bob_otks = None

            def bob_callback(url, **kwargs):
                nonlocal bob_device_keys, bob_otks
                bob_device_keys = kwargs["json"]["device_keys"]
                bob_otks = kwargs["json"]["one_time_keys"]

                return aioresponses.CallbackResult(
                    status=200,
                    body='{"one_time_key_counts":{"signed_curve25519":100}}',
                    headers={
                        "Content-Type": "application/json",
                    },
                )

            mock_aioresponse.post(
                "https://matrix-client.example.org/_matrix/client/v3/keys/upload",
                callback=bob_callback,
            )
            bob_device_keys_manager = devices.DeviceKeysManager(bob, b"\x00" * 32)
            await asyncio.sleep(0.1)

            otk = [
                key
                for id, key in bob_otks.items()
                if id.startswith("signed_curve25519:")
            ][0]

            alice_channel = olm.OlmChannel.create_outbound_channel(
                alice,
                alice_device_keys_manager,
                bob_device_keys,
                otk,
            )

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Hello world!"}
            )

            bob_channel, _ = olm.OlmChannel.create_from_encrypted_event(
                bob,
                bob_device_keys_manager,
                "@alice:example.org",
                alice_device_keys_manager.identity_key,
                alice_msg_encrypted,
                partner_device_id="ABCDEFG",
                partner_fingerprint_key=alice_device_keys_manager.fingerprint_key,
            )

            bob_channel = olm.OlmChannel.create_from_storage(
                bob,
                bob_device_keys_manager,
                "@alice:example.org",
                alice_device_keys_manager.identity_key,
            )

            alice_msg_encrypted = alice_channel.encrypt(
                "m.room.message", {"body": "Decrypt after load"}
            )

            bob_msg_encrypted = bob_channel.encrypt(
                "m.room.message", {"body": "Encrypt after load"}
            )

            alice_msg_decrypted = bob_channel.decrypt(alice_msg_encrypted)

            assert alice_msg_decrypted["content"]["body"] == "Decrypt after load"

            bob_msg_decrypted = alice_channel.decrypt(bob_msg_encrypted)

            assert bob_msg_decrypted["content"]["body"] == "Encrypt after load"


@pytest.mark.asyncio
async def test_claim_otks(mock_aioresponse):
    async with client.Client(
        storage={
            "access_token": "anaccesstoken",
            "user_id": "@alice:example.org",
            "device_id": "ABCDEFG",
        },
        callbacks={},
        base_client_url="https://matrix-client.example.org/_matrix/client/",
    ) as c:

        def callback1(url, **kwargs):
            assert kwargs["json"] == {
                "one_time_keys": {
                    "@bob:bob.example": {
                        "ABCDEFG": "signed_curve25519",
                        "HIJKLMN": "signed_curve25519",
                    },
                    "@carol:carol.example": {
                        "OPQRSTU": "signed_curve25519",
                    },
                }
            }
            return aioresponses.CallbackResult(
                status=200,
                body=json.dumps(
                    {
                        "one_time_keys": {
                            "@bob:bob.example": {
                                "ABCDEFG": {
                                    "signed_curve25519:AAAAAA": {
                                        "key": "some+key",
                                        "signatures": {
                                            "@bob:bob.example": {
                                                "ed25519:ABCDEFG": "some+signature",
                                            },
                                        },
                                    },
                                },
                                "HIJKLMN": {
                                    "signed_curve25519:AAAAAA": {
                                        "key": "some+other+key",
                                        "signatures": {
                                            "@bob:bob.example": {
                                                "ed25519:ABCDEFG": "some+other+signature",
                                            },
                                        },
                                    },
                                },
                            },
                        },
                        "failures": {
                            "carol.example": "ignored",
                        },
                    }
                ),
                headers={
                    "Content-Type": "application/json",
                },
            )

        mock_aioresponse.post(
            "https://matrix-client.example.org/_matrix/client/v3/keys/claim",
            callback=callback1,
        )

        assert await c.claim_otks(
            "signed_curve25519",
            {
                "@bob:bob.example": ["ABCDEFG", "HIJKLMN"],
                "@carol:carol.example": ["OPQRSTU"],
            },
        ) == (
            {
                "@bob:bob.example": {
                    "ABCDEFG": (
                        "signed_curve25519:AAAAAA",
                        {
                            "key": "some+key",
                            "signatures": {
                                "@bob:bob.example": {
                                    "ed25519:ABCDEFG": "some+signature",
                                },
                            },
                        },
                    ),
                    "HIJKLMN": (
                        "signed_curve25519:AAAAAA",
                        {
                            "key": "some+other+key",
                            "signatures": {
                                "@bob:bob.example": {
                                    "ed25519:ABCDEFG": "some+other+signature",
                                },
                            },
                        },
                    ),
                },
            },
            ["carol.example"],
        )
