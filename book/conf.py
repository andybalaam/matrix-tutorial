# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#

import os
import sys
sys.path.insert(0, os.path.abspath('../_theme/src'))

# -- Project information -----------------------------------------------------

project = 'Matrix Client Tutorial'
copyright = '2023-2024, Hubert Chathi'
author = 'Hubert Chathi'
version = '0.0.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'literate_sphinx',
    'myst_parser',
    'sphinxawesome_theme',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.todo',
    # 'sphinx.ext.viewcode',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'venv', 'src']

myst_heading_anchors = 3

myst_url_schemes = {
    "http": None,
    "https": None,
    "matrix": None,
    "spec": "https://spec.matrix.org/unstable/{{path}}/#{{fragment}}",
}

myst_enable_extensions = [
    "colon_fence",
    "replacements",
    "smartquotes",
]


# -- Options for HTML output -------------------------------------------------

html_title = project

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinxawesome_theme_forked'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_use_index = True

html_theme_options = {
    'show_prev_next': True,
}

pygments_style = 'default'

# -- Options for extensions --------------------------------------------------

autoclass_content = 'both'

todo_include_todos = True
