# Matrix Client Tutorial

:::{warning}
This book is a work in progress.  Most of the information that it contains
should be correct, but it is not complete.
:::

Welcome to the Matrix.  [Matrix](https://matrix.org/) is an open protocol for
decentralised communication.  Matrix allows users on different servers to
communicate with each other, similar to how people on different email servers
can email each other.

This book is an introduction to creating a client using the [Matrix
Client-Server API](spec:client-server-api).  It will show how to make HTTP
calls to a Matrix homeserver, and discuss issues that clients will need to
consider, such as reliability and security issues.  It does not attempt to be a
comprehensive guide to the entire Client-Server API, but will point you to the
relevant portions of spec for further details.

You should read this book if:

- you are creating a Matrix library/SDK,
- you are writing a Matrix client without the use of a library/SDK, or
- you want to get a better understanding of how Matrix works.

If you are creating a Matrix client using a library/SDK, some of the
information in this book may still be useful, but some things will already be
handled by your library/SDK.

We assume that you have:

- an understanding of JSON encoding,
- an understanding of HTTP requests, including HTTP status codes, headers, and
  different types of requests (`GET`, `POST`, etc.),
- experience writing asynchronous code in your preferred programming language.

This book is written in a [literate
programming](https://en.wikipedia.org/wiki/Literate_programming) style, which
interleaves code blocks and text.  In short, code blocks are named, and can
reference other code blocks using the syntax `{{other code block name}}`, which
will insert the code blocks with the given name in place of the reference.
Multiple code blocks can have the same name, in which case they are inserted in
the order that they appear in the book (though in many cases, the order does
not matter).  This allows us to, for example, have a reference such as `{{class
Foo methods}}`, and define the methods of the `Foo` class in multiple chunks.

The code output of this book will be a library that a Matrix client could use.
The actual user interface for a client is out of scope for this book, although
for some topics, there may be discussion on how clients may wish to display
things to the user.

The code is written in Python using [`aiohttp`](https://docs.aiohttp.org/), but
the rest of the content should apply any other programming languages and most
of the code should be easy to port to another language.

:::{note}
The main purpose of the example code here is to illustrate how the Matrix
Client-Server API is used, rather than to develop a fully-featured library.
While the library generated should be usable for simple situations, it may make
some tradeoffs differently from what a "real" Matrix library would make.  These
tradeoffs will be discussed in the text where appropriate.
:::

The source for this book, including the example code, is available at
<https://gitlab.com/uhoreg/matrix-tutorial>.  We will call our Matrix library
the very unimaginative name `matrixlib`.

```{literate-code} src/matrixlib/__init__.py
:file:

# {{copyright}}

"""Matrix client library example"""
```

The main entry point for our library will be a `client` module.  It will
contain helper functions and a [`Client` class](client.md), representing a
client connection.

```{literate-code} src/matrixlib/client.py
:file:

# {{copyright}}

"""General Matrix client functionality"""

import aiohttp
import asyncio
import logging
import math
import os
import re
import sys
import time
import typing
from urllib.parse import quote as urlquote, urljoin, urlparse

from . import error
from . import events
from . import pubsub
from . import schema


{{client module classes}}


{{client module functions}}
```

In the first chapter of this book, we will discover the basics of the Matrix
Client-Server API.  We will learn how to make requests to the homeserver, log
in and out, and send and receive messages.  Using this, we will create a simple
echo bot.

```{toctree}
---
maxdepth: 2
caption: "The basics"
---
base
discovery
client
login
authentication
events
sending
sync
```
% basic media repo

In the second(?) chapter, we will look at how to perform end-to-end encryption
in Matrix.  We will extend the echo bot from the previous part so that it works
in encrypted rooms.  If you do not need encryption, you may skip this part.

```{toctree}
---
maxdepth: 2
caption: "End-to-end encryption"
---
encryption
device_keys
device_tracking
megolm
olm
to_device
tying_together_megolm
```
% encrypted media
% cross-signing
% verification
% SSSS
% key backup

```{todo}
more topics, e.g.

- media repo
  - URL previews
- user management
  - registering
  - device management
  - displayname, avatar
  - 3PIDs
- interacting with rooms
  - create room
  - room versions and upgrades
  - spaces
  - room tags
  - room directory
  - DMs
  - invites, knocking
  - security issues
    - disambiguation, homographs
- more messaging features
  - reactions
  - edits
  - redactions
  - IM features (mentions, spoilers, etc)
  - typing notifications
  - read receipts
  - presence
- more on getting events
  - navigating within rooms (backfilling, jumping to a message, how to handle having
    multiple chunks of timeline)
  - sync filtering, lazy loading
  - push rules/notifications
- VoIP
- widgets
- how to test
- what client authors need to understand about federation
```

```{toctree}
---
maxdepth: 2
caption: "Appendices"
---
module_doc
```

% ## Indices and tables

% * {ref}`genindex`

```{literate-code} copyright
---
lang: text
---
Copyright Hubert Chathi <hubert@uhoreg.ca>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

SPDX-License-Identifier: Apache-2.0
```

The theme used is based on [Awesome Sphinx Theme](https://sphinxawesome.xyz/)
by Kai Welke.  The main text is set in [Inter](https://rsms.me/inter/) by
Rasmus Andersson, with [Fira Mono](https://github.com/mozilla/Fira) by Erik
Spiekermann and Ralph du Carrois used for monospaced text.

% ```{todolist}
% ```
