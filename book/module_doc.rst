Module documentation
====================

This is the documentation for the Python modules that we created in the book.

.. autosummary::

   matrixlib.client
   matrixlib.devices
   matrixlib.error
   matrixlib.events
   matrixlib.megolm
   matrixlib.olm
   matrixlib.pubsub
   matrixlib.rooms
   matrixlib.schema

.. toctree::
   :maxdepth: 1
   :glob:
   :hidden:

   module_doc_*
