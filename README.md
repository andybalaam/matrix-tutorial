# Matrix Client Tutorial

This repository contains the source for the Matrix Client Tutorial.  This
includes both the source for the book itself (inside the `book` directory), and
the sample code (inside the `src` directory for the library, and the `tests`
directory for the tests).

To use this repository, both for building the documentation and running the
code, you will need Python 3 and pip, and you'll probably want to use venv.  On
Debian-based systems, these can be installed by running as root:

```shell
apt install python3 python3-pip python3-venv
```

TODO: add instructions for other systems

If you are using venv, create a venv for the project and activate it:

```shell
python3 -m venv venv
. venv/bin/activate
```

You may also need to upgrade `pip`, depending on what version you have
installed:

```
pip install --upgrade pip
```

## Generating the documentation

Install the dependencies for building the documentation:

```shell
pip install .[docs]
```

If you have `make` installed, you can run `make html` to build the HTML
documentation.  The documentation will be written to `_build/html`.  You can
also build the documentation in other formats.  Running `make help` will list
the formats that are supported.

If you don't have `make` installed you can run the `sphinx-build` command
manually.  For example, `sphinx-build -b html book outdir` will write the
documentation to the `outdir` directory.

## Running the code

Install the dependencies:

```shell
pip install .[examples]
```

or

```shell
pip install .[examples,tests]
```

if you want to run the tests.

If you will be modifying the files, add the `-e` option to `pip` e.g.

```shell
pip install -e .[examples,tests]
```

The library is now available to Python (within the venv, if you set up a venv).
