# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Device-related functionality"""

import asyncio
from base64 import b64decode
from canonicaljson import encode_canonical_json
from cryptography.hazmat.primitives.asymmetric import ed25519
import sys
import typing
import vodozemac

from . import client
from . import schema


def verify_json_ed25519(
    signature_key: str,
    user_id: str,
    device_id: str,
    json_object: dict,
) -> None:
    """Verify a signed JSON object using a public key

    Arguments:

    - ``signature_key``: the public part of the key used to sign the object
    - ``user_id``: the ID of the user who signed the object
    - ``device_id``: the ID of the device used to sign the object
    - ``json_object``: the signed JSON object
    """
    to_verify = {
        key: value
        for key, value in json_object.items()
        if key not in ["signatures", "unsigned"]
    }
    canonical = encode_canonical_json(to_verify)

    signature_b64 = json_object["signatures"][user_id][f"ed25519:{device_id}"]
    # b64decode expects the input to be padded, but will happily ignore extra
    # padding, so we just add 2 "=", which is the maximum padding
    key_binary = b64decode(signature_key + "==")
    signature_binary = b64decode(signature_b64 + "==")
    ed25519.Ed25519PublicKey.from_public_bytes(key_binary).verify(
        signature_binary, canonical
    )


class DeviceKeysManager:
    """Manages a device's keys for end-to-end encryption"""

    def __init__(self, c: client.Client, key: bytes):
        """
        Arguments:

        ``c``:
          the client object
        ``key``:
          a 32-byte binary used to encrypt the objects in storage
        """
        self.client = c
        self.key = key

        if "olm_account" in c.storage:
            self.account = vodozemac.Account.from_pickle(c.storage["olm_account"], key)
        else:
            self.account = vodozemac.Account()
            c.storage["olm_account"] = self.account.pickle(key)

        self.upload_task = asyncio.create_task(self._upload_keys_task())

        c.publisher.subscribe(client.ClientClosed, self._client_closed_subscriber)

        c.publisher.subscribe(client.OneTimeKeysCount, self._otk_count_subscriber)
        self.lock = asyncio.Lock()

        self.otk_count_from_sync: typing.Optional[int] = None

    @property
    def identity_key(self) -> str:
        return self.account.curve25519_key

    @property
    def fingerprint_key(self) -> str:
        return self.account.ed25519_key

    async def _upload_keys_task(self) -> None:
        self.upload_keys_event = asyncio.Event()

        while True:
            self.upload_keys_event.clear()
            await self._upload_keys()
            await self.upload_keys_event.wait()

    async def _upload_keys(self) -> None:
        keys_on_server = self.client.storage.get("olm_account.keys_on_server", {})

        upload_body = {}

        if not keys_on_server.get("device_keys", False):
            device_keys = {
                "algorithms": [
                    "m.olm.v1.curve25519-aes-sha2",
                    "m.megolm.v1.aes-sha2",
                ],
                "user_id": self.client.user_id,
                "device_id": self.client.device_id,
                "keys": {
                    f"curve25519:{self.client.device_id}": self.account.curve25519_key,
                    f"ed25519:{self.client.device_id}": self.account.ed25519_key,
                },
            }
            self.sign_json(device_keys)

            upload_body["device_keys"] = device_keys

        async with self.lock:
            if self.otk_count_from_sync is not None:
                keys_on_server["one_time_keys"] = self.otk_count_from_sync
                self.client.storage["olm_account.keys_on_server"] = keys_on_server
                otk_count = self.otk_count_from_sync
                self.otk_count_from_sync = None
            else:
                otk_count = keys_on_server.get("one_time_keys", 0)

        otks_needed = self.account.max_number_of_one_time_keys - otk_count
        if otks_needed > 0:
            keys_available = len(self.account.one_time_keys)
            otks_needed = otks_needed - keys_available
            if otks_needed > 0:
                self.account.generate_one_time_keys(min(20, otks_needed))
                self.client.storage["olm_account"] = self.account.pickle(self.key)

            upload_body["one_time_keys"] = {
                f"signed_curve25519:{name}": self.sign_json({"key": public_key})
                for (name, public_key) in self.account.one_time_keys.items()
            }

        if upload_body != {}:
            async with await client.retry(
                60_000,  # retry for up to one minute
                self.client.authenticated,
                self.client.http_session.post,
                self.client.url("v3/keys/upload"),
                json=upload_body,
            ) as resp:
                try:
                    _, resp_body = await client.check_response(resp)
                except:
                    # if the key upload failed, wait a bit, then trigger another
                    # key upload
                    await asyncio.sleep(120)
                    self.upload_keys_event.set()
                    return

                keys_on_server["device_keys"] = True

                self.account.mark_keys_as_published()
                self.client.storage["olm_account"] = self.account.pickle(self.key)

                schema.ensure_valid(resp_body, {"one_time_key_counts": dict[str, int]})

                otk_count_from_resp = resp_body["one_time_key_counts"].get(
                    "signed_curve25519", 0
                )

                await self.lock.acquire()
                if (
                    self.otk_count_from_sync is None
                    or self.otk_count_from_sync >= otk_count_from_resp
                ):
                    keys_on_server["one_time_keys"] = otk_count_from_resp
                    self.otk_count_from_sync = None
                    self.lock.release()
                else:
                    self.otk_count_from_sync = None
                    self.lock.release()
                    async with await client.retry(
                        10_000,
                        self.client.authenticated,
                        self.client.http_session.post,
                        self.client.url("v3/keys/upload"),
                        json={},
                    ) as resp2:
                        _, resp2_body = await client.check_response(resp2)
                        schema.ensure_valid(
                            resp2_body, {"one_time_key_counts": dict[str, int]}
                        )
                        async with self.lock:
                            otk_count_from_resp = resp2_body["one_time_key_counts"].get(
                                "signed_curve25519", 0
                            )
                            if self.otk_count_from_sync != None:
                                keys_on_server["one_time_keys"] = min(
                                    otk_count_from_resp, self.otk_count_from_sync
                                )
                                self.otk_count_from_sync = None
                            else:
                                keys_on_server["one_time_keys"] = otk_count_from_resp

                if (
                    keys_on_server["one_time_keys"]
                    < self.account.max_number_of_one_time_keys
                ):
                    self.upload_keys_event.set()

                self.client.storage["olm_account.keys_on_server"] = keys_on_server

    def _client_closed_subscriber(self, _: client.ClientClosed) -> None:
        self.upload_task.cancel()

    def sign_json(self, json_object: dict) -> dict:
        """Sign a JSON object using the device's signing key

        The input object is modified to include the signature.
        """
        signatures = json_object.pop("signatures", {})
        unsigned = json_object.pop("unsigned", None)

        sig = self.account.sign(encode_canonical_json(json_object).decode("utf-8"))

        key_id = f"ed25519:{self.client.device_id}"
        signatures.setdefault(self.client.user_id, {})[key_id] = sig

        json_object["signatures"] = signatures
        if unsigned is not None:
            json_object["unsigned"] = unsigned

        return json_object

    async def _otk_count_subscriber(
        self, one_time_keys_count: client.OneTimeKeysCount
    ) -> None:
        if not one_time_keys_count.has_to_device:
            count = one_time_keys_count.otk_count.get("signed_curve25519", 0)
            if count < self.account.max_number_of_one_time_keys:
                async with self.lock:
                    self.otk_count_from_sync = count
                self.upload_keys_event.set()


class DeviceTracker:
    """Tracks user devices to reduce the number of queries required"""

    def __init__(self, c: client.Client):
        """
        Arguments:

        ``c``:
          the client object
        """
        self.client = c
        c.publisher.subscribe(client.DeviceChanges, self._subscriber)

        self.lock = asyncio.Lock()

        self.in_flight: dict[str, asyncio.Future] = {}

        self.do_not_cache: typing.Set[str] = set()

    async def _subscriber(self, changes: client.DeviceChanges) -> None:
        async with self.lock:
            tracked_users = self.client.storage.get(
                "device_tracker.tracked_users",
                {self.client.user_id: True},  # We always track our own devices
            )

            for user in changes.changed:
                tracked_users[user] = True
                self._delete_user_device_keys(user)
            for user in changes.left:
                if user in tracked_users:
                    del tracked_users[user]
                self._delete_user_device_keys(user)

            self.client.storage["device_tracker.tracked_users"] = tracked_users

    def _delete_user_device_keys(self, user):
        user_key = f"device_tracker.cache.{user}"
        if user_key in self.client.storage:
            del self.client.storage[user_key]

        if user in self.in_flight:
            self.do_not_cache.add(user)

    async def get_device_keys(
        self,
        users: typing.Iterable[str],
        force_download=False,
        timeout: typing.Optional[int] = None,
    ) -> dict[str, "UserDeviceKeysResult"]:
        """Get the device keys for the given users.

        Arguments:

        ``users``:
          the user IDs to fetch device keys for
        ``force_download``:
          whether to ignore the cache and force downloading of all device keys from
          the server
        ``timeout``:
          a timeout in milliseconds for the homeserver to wait for responses from
          remote homeservers

        Returns a dict mapping the user IDs to a ``UserDeviceKeysResult``.
        """
        ret = {}
        users_needing_download: dict[str, list] = {}

        async with self.lock:
            if not force_download:
                for user in users:
                    storage_key = f"device_tracker.cache.{user}"
                    if storage_key in self.client.storage:
                        ret[user] = UserDeviceKeysResult(
                            **self.client.storage[storage_key]
                        )
                    else:
                        users_needing_download[user] = []
            else:
                users_needing_download = {user: [] for user in users}

            loop = asyncio.get_running_loop()

            device_futures = {}

            for user in users_needing_download.keys():
                # If we already have a request in-flight for the user, we can use that
                # result instead of re-requesting.  Otherwise, record that we will be
                # making a request for that user.
                if user in self.in_flight:
                    device_futures[user] = self.in_flight[user]
                else:
                    self.in_flight[user] = loop.create_future()

            for user in device_futures.keys():
                # drop users from our request if we're using the result from a future
                del users_needing_download[user]

        if users_needing_download != {}:
            req_body: dict[str, typing.Any] = {"device_keys": users_needing_download}
            if timeout != None:
                req_body["timeout"] = timeout
            try:
                resp = await self.client.authenticated(
                    self.client.http_session.post,
                    self.client.url("v3/keys/query"),
                    json=req_body,
                )
                status, resp_body = await client.check_response(resp)
                schema.ensure_valid(
                    resp_body,
                    {
                        "device_keys": schema.Optional(dict[str, dict[str, dict]]),
                        "failures": schema.Optional(dict),
                        # FIXME: cross-signing keys
                    },
                )
            except:
                e = typing.cast(BaseException, sys.exc_info()[1])
                for user in users_needing_download.keys():
                    self.in_flight[user].set_exception(e)
                    del self.in_flight[user]

                raise

            user_device_keys = resp_body.get("device_keys", {})
            failures = resp_body.get("failures", {})
            tracked_users = self.client.storage.get("device_tracker.tracked_users", {})

            async with self.lock:
                for user in users_needing_download.keys():
                    device_keys = user_device_keys.get(user, {})

                    # only keep valid device keys
                    device_keys = {
                        device_id: device_key
                        for device_id, device_key in device_keys.items()
                        if (
                            schema.is_valid(device_key, DEVICE_KEY_SCHEMA)
                            and device_key["user_id"] == user
                            and device_key["device_id"] == device_id
                        )
                    }

                    user_device_info = UserDeviceKeysResult(device_keys)
                    # FIXME: process cross-signing keys

                    self._cache_result(user, tracked_users, failures, user_device_info)
                    ret[user] = user_device_info

                    self.in_flight[user].set_result(user_device_info)
                    del self.in_flight[user]

        for user, future in device_futures.items():
            ret[user] = await future

        return ret

    def _cache_result(self, user, tracked_users, failures, user_device_info):
        if self._should_cache_result(user, tracked_users, failures):
            self.client.storage[
                f"device_tracker.cache.{user}"
            ] = user_device_info._asdict()

    def _should_cache_result(
        self, user: str, tracked_users: dict, failures: dict
    ) -> bool:
        if user in self.do_not_cache:
            self.do_not_cache.remove(user)
            return False

        if user not in tracked_users:
            return False

        # find the user's server name
        split_user_id = user.split(":", 1)
        if len(split_user_id) != 2:
            # invalid user ID, so don't cache
            return False
        if split_user_id[1] in failures:
            return False

        return True


class UserDeviceKeysResult(typing.NamedTuple):
    """The return value of `DeviceTracker.get_device_keys` for a user"""

    device_keys: dict[str, dict]
    # FIXME: add cross-signing keys


UserDeviceKeysResult.device_keys.__doc__ = "mapping from device ID to device keys"


DEVICE_KEY_SCHEMA = {
    "algorithms": schema.Array(str),
    "device_id": str,
    "user_id": str,
    "keys": schema.Object(typing.Any),
    "signatures": schema.Optional(schema.Object(schema.Object(typing.Any))),
}
