# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Megolm-related functionality"""

import asyncio
from base64 import b64decode, b64encode
import json
import logging
import os
import time
import typing
import sys
import vodozemac
from cryptography.hazmat.primitives.ciphers.aead import AESGCM

from .client import Client
from . import client
from . import devices
from . import error
from . import events
from . import olm
from . import pubsub
from . import rooms
from . import schema


MEGOLM_ALGORITHM = "m.megolm.v1.aes-sha2"


class OutboundMegolmSession:
    """Manages a Megolm session for decrypting"""

    def __init__(
        self,
        c: Client,
        room_id: str,
        room_state_tracker: rooms.RoomStateTracker,
        device_tracker: devices.DeviceTracker,
        device_keys_manager: devices.DeviceKeysManager,
        key: typing.Optional[bytes] = None,
        session_id: typing.Optional[str] = None,
    ):
        """
        Arguments:

        ``c``:
          the client object
        ``room_id``:
          the ID of the room that the session belongs to
        ``room_state_tracker``:
          a ``RoomStateTracker`` object
        ``device_tracker``:
          a ``DeviceTracker`` object
        ``device_keys_manager``:
          a ``DeviceKeysManager`` object
        ``key``:
          a 32-byte binary used to encrypt the objects in storage
        ``session_id``:
          if given, will load a session from storage.  If omitted a new session
          will be created
        """
        self.client = c
        self.room_id = room_id
        self.room_state_tracker = room_state_tracker
        self.device_tracker = device_tracker
        self.device_keys_manager = device_keys_manager
        self.key = key if key else device_keys_manager.key
        if session_id:
            self.session_data = c.storage[
                f"outbound_megolm_session.{room_id}.{session_id}"
            ]
            self.session = vodozemac.GroupSession.from_pickle(
                self.session_data["pickle"], self.key
            )
        else:
            self.session = vodozemac.GroupSession()
            self.session_data = {
                "pickle": self.session.pickle(self.key),
                "creation_time": time.time_ns() / 1000,
                "sent_to_devices": {},
            }
            self._store_session_data()
        self.aesgcm = AESGCM(self.key)
        self.lock = asyncio.Lock()

    def _store_session_data(self, pickle=False) -> None:
        if pickle:
            self.session_data["pickle"] = self.session.pickle(self.key)
        name = f"outbound_megolm_session.{self.room_id}.{self.session.session_id}"
        self.client.storage[name] = self.session_data

    @property
    def session_id(self) -> str:
        """The ID of the Megolm session"""
        return self.session.session_id

    async def get_session_key_for_sending(self) -> list[typing.Tuple[dict, dict]]:
        """Get the devices that we need to send the Megolm session, along with the
        data to send.

        Returns a list of tuples.  The first item in the tuple is the device key
        to send to, and the second item in the tuple is the contents of an
        ``m.room_key`` event to send to the device.  The event should be sent
        encrypted.
        """
        async with self.lock:
            encryption_state = self.room_state_tracker.get_state(
                self.room_id, "m.room.encryption"
            )

            if encryption_state == None:
                raise RuntimeError("Room is not encrypted")

            encryption_state = typing.cast(events.StateEvent, encryption_state)

            rotation_period_ms = encryption_state.content.get(
                "rotation_period_ms", 7 * 24 * 60 * 60 * 1000
            )
            rotation_period_msgs = encryption_state.content.get(
                "rotation_period_msgs", 100
            )

            if (
                rotation_period_msgs <= self.session.message_index
                or time.time_ns() / 1000
                > self.session_data["creation_time"] + rotation_period_ms
            ):
                raise SessionExpiredException()
            room_key = {
                "algorithm": MEGOLM_ALGORITHM,
                "room_id": self.room_id,
                "session_id": self.session.session_id,
                "session_key": self.session.session_key,
            }
            history_visibility = self.room_state_tracker.get_state(
                self.room_id,
                "m.room.history_visibility",
            )
            if (
                history_visibility
                and history_visibility.content.get("history_visibility") == "join"
            ):
                allowed_membership = ["join"]
            else:
                allowed_membership = ["join", "invite"]

            member_events = self.room_state_tracker.get_all_state_for_type(
                self.room_id,
                "m.room.member",
            )
            members = [
                user_id
                for user_id, event in member_events.items()
                if event.content.get("membership") in allowed_membership
            ]

            recipient_keys = await self.device_tracker.get_device_keys(members)

            own_key_info = recipient_keys.get(typing.cast(str, self.client.user_id))
            if own_key_info:
                own_devices = own_key_info.device_keys
                own_devices.pop(typing.cast(str, self.client.device_id), None)
                if own_devices == {}:
                    del recipient_keys[typing.cast(str, self.client.user_id)]

            for user_id, device_info in self.session_data["sent_to_devices"].items():
                if user_id not in recipient_keys:
                    raise SessionExpiredException()
                recipient_device_keys = recipient_keys.get(
                    user_id, devices.UserDeviceKeysResult({})
                ).device_keys
                for device_id in device_info.keys():
                    if device_id not in recipient_device_keys:
                        raise SessionExpiredException()

            devices_to_send_to = []

            for user_id, key_info in recipient_keys.items():
                sent_to_devices = self.session_data["sent_to_devices"].setdefault(
                    user_id, {}
                )
                for device_id, device_key in key_info.device_keys.items():
                    sent_info = sent_to_devices.get(device_id, {})
                    status = sent_info.get("status")
                    if status == "pending":
                        [nonce, encrypted_key] = sent_info["session_key"]
                        devices_to_send_to.append(
                            (
                                device_key,
                                {
                                    "algorithm": MEGOLM_ALGORITHM,
                                    "room_id": self.room_id,
                                    "session_id": self.session.session_id,
                                    "session_key": self.aesgcm.decrypt(
                                        b64decode(nonce), b64decode(encrypted_key), None
                                    ).decode(),
                                },
                            )
                        )
                    elif status != "sent":
                        devices_to_send_to.append(
                            (
                                device_key,
                                room_key,
                            )
                        )
                        nonce = os.urandom(12)
                        sent_to_devices[device_id] = {
                            "status": "pending",
                            "session_key": [
                                b64encode(nonce).decode(),
                                b64encode(
                                    self.aesgcm.encrypt(
                                        nonce, room_key["session_key"].encode(), None
                                    )
                                ).decode(),
                            ],
                        }

            # Note: if storing things takes a long time, we could check if we actually made
            # any changes before saving the session data
            self._store_session_data()

            return devices_to_send_to

    def mark_as_sent(self, device_keys: typing.Iterable[dict]) -> None:
        """Indicate that the session has been sent to the given devices.

        Arguments:

        ``device_keys``:
          an interable of devicec keys
        """
        for device_key in device_keys:
            if "user_id" in device_key and "device_id" in device_key:
                sent_to_devices = self.session_data["sent_to_devices"].setdefault(
                    device_key["user_id"], {}
                )
                sent_to_devices[device_key["device_id"]] = {"status": "sent"}

        self._store_session_data()

    def encrypt(self, event_type: str, content: dict) -> dict:
        """Encrypt an event

        Arguments:

        ``event_type``:
          the type of the event (e.g. ``m.room.message``)
        ``content``:
          the event ``content``

        Returns the ``content`` of a ``m.room.encrypted`` event
        """
        plaintext = json.dumps(
            {
                "room_id": self.room_id,
                "type": event_type,
                "content": content,
            }
        )
        ciphertext = self.session.encrypt(plaintext)
        self._store_session_data(True)
        return {
            "algorithm": MEGOLM_ALGORITHM,
            "sender_key": self.device_keys_manager.identity_key,
            "device_id": self.client.device_id,
            "session_id": self.session.session_id,
            "ciphertext": ciphertext,
        }


class SessionExpiredException(Exception):
    """Indicates that the session has expired"""

    pass


class InboundMegolmSession:
    """Manages a Megolm session for decrypting"""

    client: Client
    user_id: str
    room_id: str
    key: bytes
    session: vodozemac.InboundGroupSession
    session_data: dict[str, typing.Any]

    def __init__(self):
        """Do not use initializer function.  Use the ``from_*`` methods instead"""
        raise RuntimeError("Use the from_* methods instead")  # pragma: no cover

    @classmethod
    def from_outbound_session(
        cls,
        c: Client,
        outbound: OutboundMegolmSession,
        key: typing.Optional[bytes] = None,
    ) -> "InboundMegolmSession":
        """Create an ``InboundMegolmSession`` from an ``OutboundMegolmSession``

        Arguments:

        ``c``:
          the client object
        ``outbound``:
          the ``OutboundMegolmSession`` to use
        ``key``:
          a 32-byte binary used to encrypt the objects in storage.  If not
          specified, uses the same key as used by ``outbound``
        """
        obj = cls.__new__(cls)

        obj.client = c
        obj.user_id = typing.cast(str, c.user_id)
        obj.room_id = outbound.room_id
        obj.key = key if key else outbound.key
        obj.session = vodozemac.InboundGroupSession(outbound.session.session_key)
        obj.session_data = {
            "pickle": obj.session.pickle(obj.key),
            "sender_key": outbound.device_keys_manager.identity_key,
            "authenticated": True,
            "event_ids": {},
        }
        obj._store_session_data()

        return obj

    def _store_session_data(self, pickle=False) -> None:
        if pickle:
            self.session_data["pickle"] = self.session.pickle(self.key)
        name = f"inbound_megolm_session.{self.room_id}.{self.user_id}.{self.session.session_id}"
        self.client.storage[name] = self.session_data

    @property
    def sender_key(self) -> str:
        """The identity key of the Megolm session's sender"""
        return self.session_data["sender_key"]

    @property
    def authenticated(self) -> bool:
        """Whether we know that the Megolm session comes from the associated ``sender_key``"""
        return self.session_data["authenticated"]

    @classmethod
    def from_room_key(
        cls,
        c: Client,
        user_id: str,
        sender_key: str,
        room_key_content: dict,
        key: bytes,
    ) -> "InboundMegolmSession":
        """Create an ``InboundMegolmSession`` from an ``m.room_key`` event

        Arguments:

        ``c``:
          the client object
        ``user_id``:
          the user ID of the sender of the ``m.room_key`` event
        ``sender_key``:
          the identity key of the sender of the ``m.room_key`` event
        ``room_key_content``:
          the ``content`` of the ``m.room_key`` event
        ``key``:
          a 32-byte binary used to encrypt the objects in storage
        """
        obj = cls.__new__(cls)

        if room_key_content["algorithm"] != MEGOLM_ALGORITHM:
            raise RuntimeError("Invalid algorithm")
        obj.client = c
        obj.user_id = user_id
        obj.room_id = room_key_content["room_id"]
        obj.key = key
        obj.session = vodozemac.InboundGroupSession(room_key_content["session_key"])
        if obj.session.session_id != room_key_content["session_id"]:
            raise RuntimeError("Mismatched session ID")
        obj.session_data = {
            "pickle": obj.session.pickle(obj.key),
            "sender_key": sender_key,
            "authenticated": True,
            "event_ids": {},
        }
        obj._store_session_data()

        return obj

    @classmethod
    def from_storage(
        cls,
        c: Client,
        room_id: str,
        user_id: str,
        session_id: str,
        key: bytes,
    ) -> typing.Optional["InboundMegolmSession"]:
        """Load a session from storage

        Arguments:

        ``c``:
          the client object
        ``room_id``:
          the ID of the room that the session belongs to
        ``user_id``:
          the ID of the user that the session belongs to
        ``session ID``:
          the ID of the Megolm session
        ``key``:
          a 32-byte binary used to encrypt the objects in storage
        """
        obj = cls.__new__(cls)

        obj.client = c
        obj.room_id = room_id
        obj.user_id = user_id
        obj.key = key
        name = f"inbound_megolm_session.{room_id}.{user_id}.{session_id}"
        obj.session_data = c.storage.get(name)

        if obj.session_data == None:
            return None

        obj.session = vodozemac.InboundGroupSession.from_pickle(
            obj.session_data["pickle"],
            obj.key,
        )

        return obj

    def decrypt(self, event: events.RoomEvent) -> dict:
        """Decrypt an ``m.room.encrypted`` event encrypted with Megolm

        Arguments:

        ``event``:
          the encrypted event

        Returns the decrypted event, which will be a dict that should have ``type``
        (the decrypted event type), ``content`` (the event content), and
        ``room_id`` (the ID of the room the event was sent to) properties.
        """
        # check cleartext
        schema.ensure_valid(
            event.content,
            {
                "algorithm": str,
                "sender_key": schema.Optional(str),
                "device_id": schema.Optional(str),
                "session_id": str,
                "ciphertext": str,
            },
        )
        if event.content["algorithm"] != MEGOLM_ALGORITHM:
            raise RuntimeError("Invalid algorithm")

        # decrypt ciphertext
        decrypted = self.session.decrypt(event.content["ciphertext"])
        plaintext = json.loads(decrypted.plaintext)
        if decrypted.message_index in self.session_data["event_ids"]:
            if (
                self.session_data["event_ids"][decrypted.message_index]
                != event.event_id
            ):
                raise RuntimeError("Replay attack detected")
        else:
            self.session_data["event_ids"][decrypted.message_index] = event.event_id
        self._store_session_data(True)

        # check plaintext
        schema.ensure_valid(
            plaintext,
            {
                "type": str,
                "content": dict,
                "room_id": str,
            },
        )
        if plaintext["room_id"] != self.room_id:
            raise RuntimeError("Mismatched room ID")

        return plaintext


class OlmMegolmEncryptionManager:
    """Manages encryption and decryption of room events"""

    def __init__(
        self,
        c: Client,
        key: bytes,
        device_keys_manager: typing.Optional[devices.DeviceKeysManager] = None,
        device_tracker: typing.Optional[devices.DeviceTracker] = None,
        room_state_tracker: typing.Optional[rooms.RoomStateTracker] = None,
    ):
        """
        Arguments:

        ``c``:
          the client object
        ``key``:
          a 32-byte binary used to encrypt the objects in storage
        ``device_keys_manager``:
          a ``devices.DeviceKeysManager`` object to use.  If none is provided, a
          new one will be created.
        ``device_tracker``:
          a ``devices.DeviceTracker`` object to use.  If none is provided, a new
          one will be created.
        """
        self.client = c
        self.key = key
        self.device_keys_manager = typing.cast(
            devices.DeviceKeysManager,
            device_keys_manager
            if device_keys_manager != None
            else devices.DeviceKeysManager(c, key),
        )
        self.device_tracker = typing.cast(
            devices.DeviceTracker,
            device_tracker if device_tracker != None else devices.DeviceTracker(c),
        )
        self.room_state_tracker = typing.cast(
            rooms.RoomStateTracker,
            room_state_tracker
            if room_state_tracker != None
            else rooms.RoomStateTracker(c),
        )

        c.publisher.subscribe(client.ToDeviceEvents, self._to_device_subscriber)

        self.lock = asyncio.Lock()

        self.publisher = pubsub.Publisher()

        c.publisher.subscribe(
            (client.RoomStateUpdates, client.RoomTimelineUpdates),
            self._state_subscriber,
        )

    async def _to_device_subscriber(self, msg: client.ToDeviceEvents) -> None:
        async with self.lock:
            for event in msg.events:
                if (
                    event.type == "m.room.encrypted"
                    and event.content.get("algorithm") == olm.OLM_ALGORITHM
                    and schema.is_valid(
                        event.content,
                        {
                            "sender_key": str,
                            "ciphertext": schema.Object({"type": int, "body": str}),
                        },
                    )
                ):
                    await self._decrypt_and_process_todevice(event)

    async def _decrypt_and_process_todevice(self, event: events.Event) -> None:
        sender_key = event.content["sender_key"]
        try:
            olm_channel = olm.OlmChannel.create_from_storage(
                self.client,
                self.device_keys_manager,
                event.sender,
                sender_key,
            )
            if isinstance(olm_channel, olm.OlmChannel):
                decrypted = olm_channel.decrypt(event.content)
            else:
                # FIXME: we should probably only query the cache, and never query
                # the server here, to avoid delays
                sender_devices = (
                    (await self.device_tracker.get_device_keys([event.sender]))
                    .get(event.sender, devices.UserDeviceKeysResult({}))
                    .device_keys
                )
                sender_device_id, sender_fingerprint_key = None, None
                for device_id, device in sender_devices.items():
                    if (
                        device.get("keys", {}).get(f"curve25519:{device_id}")
                        == sender_key
                    ):
                        sender_device_id = device_id
                        sender_fingerprint_key = device["keys"].get(
                            f"ed25519:{device_id}"
                        )

                (
                    olm_channel,
                    decrypted_or_err,
                ) = olm.OlmChannel.create_from_encrypted_event(
                    self.client,
                    self.device_keys_manager,
                    event.sender,
                    sender_key,
                    event.content,
                    sender_device_id,
                    sender_fingerprint_key,
                )

                if isinstance(decrypted_or_err, dict):
                    decrypted = decrypted_or_err
                else:
                    raise decrypted_or_err
            # FIXME: Olm unwedging - on error, create new olm session and send dummy event
        except:
            e = sys.exc_info()[1]
            logging.error(
                f"Unable to decrypt to-device event from {event.sender}:{sender_key}", e
            )
            return

        if decrypted["type"] == "m.room_key":
            try:
                session = InboundMegolmSession.from_room_key(
                    self.client,
                    event.sender,
                    sender_key,
                    decrypted["content"],
                    self.key,
                )
                await self.publisher.publish(session)
            except:
                e = sys.exc_info()[1]
                logging.error(
                    f"Unable to process room key from {event.sender}:{sender_key}", e
                )
        else:
            logging.info(
                f"Ignoring {decrypted['type']} event from {event.sender}:{sender_key}"
            )

    async def decrypt_room_event(self, encrypted: events.RoomEvent) -> events.RoomEvent:
        """Decrypt an event

        Returns the room event that would have been received if the event was not
        encrypted.

        Arguments:

        ``encrypted``:
          the encrypted room event
        """
        async with self.lock:
            content = encrypted.content
            if (
                encrypted.type != "m.room.encrypted"
                or content.get("algorithm") != MEGOLM_ALGORITHM
                or "session_id" not in content
            ):
                raise error.UnableToDecryptError("Not encrypted or malformed event")
            session = InboundMegolmSession.from_storage(
                self.client,
                encrypted.room_id,
                encrypted.sender,
                content["session_id"],
                self.key,
            )
            if not session:
                # FIXME: add the session ID
                raise error.UnableToDecryptError("Unknown session ID")
            decrypted = session.decrypt(encrypted)
            return events.RoomEvent(
                type=decrypted["type"],
                sender=encrypted.sender,
                content=decrypted["content"],
                unsigned=encrypted.unsigned,
                room_id=decrypted["room_id"],
                event_id=encrypted.event_id,
                origin_server_ts=encrypted.origin_server_ts,
            )

    async def _state_subscriber(
        self, msg: typing.Union[client.RoomStateUpdates, client.RoomTimelineUpdates]
    ) -> None:
        room_id = msg.room_id
        for e in msg.events:
            if (
                isinstance(e, events.StateEvent)
                and e.type == "m.room.encryption"
                and e.state_key == ""
            ):
                state = self.client.storage.get("room_encryption_state", {})
                [prev_alg, orig_alg] = state.get(room_id, [None, None])
                new_alg = e.content.get("algorithm")
                if type(new_alg) != str:
                    new_alg = None

                if prev_alg == new_alg:
                    # if the encryption algorithm isn't changing, we don't need to
                    # do anything
                    return
                elif new_alg is None:
                    await self.publisher.publish(EncryptionDisabled(room_id, orig_alg))
                elif orig_alg == new_alg:
                    # encryption is being restored to the original algorithm
                    await self.publisher.publish(EncryptionEnabled(room_id, new_alg))
                elif prev_alg is None:
                    if orig_alg is None:
                        # encryption is being enabled for the first time
                        # the new encryption algorithm will be the room's original
                        # encryption algorithm
                        orig_alg = new_alg

                        if new_alg == MEGOLM_ALGORITHM:
                            await self.publisher.publish(
                                EncryptionEnabled(room_id, new_alg)
                            )
                        else:
                            await self.publisher.publish(
                                UnknownRoomEncryptionAlgorithm(
                                    room_id, typing.cast(str, new_alg)
                                )
                            )
                    else:
                        # the encryption algorithm is being set to something
                        # other than the previous algorithm, and other than the
                        # original algorithm
                        await self.publisher.publish(
                            EncryptionAlgorithmChanged(room_id, orig_alg, new_alg)
                        )
                else:
                    await self.publisher.publish(
                        EncryptionAlgorithmChanged(room_id, orig_alg, new_alg)
                    )
                state[room_id] = [new_alg, orig_alg]
                self.client.storage["room_encryption_state"] = state

    def is_room_encrypted(self, room_id: str) -> bool:
        """
        Determines whether events sent to the given room should be encrypted.

        May raise

        * ``EncryptionDisabledError`` if encryption was enabled in the room in the
          past, but is now disabled
        * ``EncryptionAlgorithmChangedError`` if the encryption algorithm for the
          room changed from its initial algorithm
        * ``UnknownRoomEncryptionAlgorithmError`` if the encryption algorithm for
          the room is not known (that is, it is not Megolm)
        """
        state = self.client.storage.get("room_encryption_state", {})
        [curr_alg, orig_alg] = state.get(room_id, [None, None])
        if curr_alg is None:
            if orig_alg is None:
                return False
            else:
                raise error.EncryptionDisabledError()
        elif curr_alg != orig_alg:
            raise error.EncryptionAlgorithmChangedError(orig_alg, curr_alg)
        elif curr_alg == MEGOLM_ALGORITHM:
            return True
        else:
            raise error.UnknownEncryptionAlgorithmError(orig_alg)

    async def _encrypt_and_send_to_device(
        self,
        events: typing.Iterable[typing.Tuple[dict, str, dict]],
        otk_request_timeout: typing.Optional[int] = None,
        batch_size: int = 200,
    ) -> None:
        async with self.lock:
            send_queue: list[typing.Tuple[str, str, dict]] = []
            send_task: asyncio.Task[None] = asyncio.create_task(asyncio.sleep(0))

            events_to_encrypt: list[typing.Tuple[olm.OlmChannel, str, dict]] = []
            channels_to_create: list[typing.Tuple[dict, str, dict]] = []

            for device_key, event_type, event_content in events:
                if not schema.is_valid(
                    device_key, {"user_id": str, "device_id": str, "keys": dict}
                ):
                    logging.error("invalid device key", device_key)
                    continue
                user_id = device_key["user_id"]
                device_id = device_key["device_id"]
                identity_key_id = f"curve25519:{device_id}"
                if identity_key_id not in device_key["keys"]:
                    logging.error("device key does not have Olm identity key")
                    continue
                identity_key = device_key["keys"][identity_key_id]

                olm_channel = olm.OlmChannel.create_from_storage(
                    self.client, self.device_keys_manager, user_id, identity_key
                )

                if isinstance(olm_channel, olm.OlmChannel):
                    olm_channel.assert_partner_device_id(device_id)
                    events_to_encrypt.append((olm_channel, event_type, event_content))
                else:
                    channels_to_create.append((device_key, event_type, event_content))
            # create the `devices` parameter for `claim_otks`, which is a dict mapping
            # user IDs to a list of device IDs
            claim_otks_devices: dict[str, list[str]] = {}
            for device_key, _, _ in channels_to_create:
                claim_otks_devices.setdefault(device_key["user_id"], []).append(
                    device_key["device_id"]
                )

            keys_claim_task = (
                asyncio.create_task(
                    self.client.claim_otks(
                        "signed_curve25519",
                        claim_otks_devices,
                        otk_request_timeout,
                    )
                )
                if claim_otks_devices  # an empty dict is falsy in Python
                else asyncio.create_task(asyncio.sleep(0, ({}, [])))
            )
            for olm_channel, event_type, event_content in events_to_encrypt:
                encrypted = olm_channel.encrypt(event_type, event_content)
                send_queue.append(
                    (
                        olm_channel.partner_user_id,
                        typing.cast(str, olm_channel.partner_device_id),
                        encrypted,
                    )
                )
                if len(send_queue) >= batch_size:
                    send_task = asyncio.create_task(
                        self._flush_to_device_send_queue(send_task, send_queue)
                    )
                    send_queue = []
            if len(send_queue) > 0:
                done, pending = await asyncio.wait(
                    [keys_claim_task, send_task], return_when=asyncio.FIRST_COMPLETED
                )
                if keys_claim_task in pending:
                    send_task = asyncio.create_task(
                        self._flush_to_device_send_queue(send_task, send_queue)
                    )
                    send_queue = []
            await keys_claim_task

            otk_result, otk_failures = keys_claim_task.result()
            for device_key, event_type, event_content in channels_to_create:
                id_and_otk = otk_result.get(device_key["user_id"], {}).get(
                    device_key["device_id"]
                )
                if id_and_otk:
                    id, otk = id_and_otk
                    olm_channel = olm.OlmChannel.create_outbound_channel(
                        self.client,
                        self.device_keys_manager,
                        device_key,
                        otk,
                    )
                    encrypted = olm_channel.encrypt(event_type, event_content)
                    send_queue.append(
                        (
                            device_key["user_id"],
                            device_key["device_id"],
                            encrypted,
                        )
                    )
                    if len(send_queue) >= batch_size:
                        send_task = asyncio.create_task(
                            self._flush_to_device_send_queue(send_task, send_queue)
                        )
                        send_queue = []
            if len(send_queue) > 0:
                await self._flush_to_device_send_queue(send_task, send_queue)

    async def _flush_to_device_send_queue(
        self,
        send_task: asyncio.Task[None],
        send_queue: list[typing.Tuple[str, str, dict]],
    ) -> None:
        to_device_contents: dict[str, dict[str, dict]] = {}
        for user_id, device_id, contents in send_queue:
            to_device_contents.setdefault(user_id, {})[device_id] = contents

        # wait until the previous send is done, so that we don't have multiple
        # in-flight
        await send_task

        await asyncio.create_task(
            self.client.send_to_device("m.room.encrypted", to_device_contents)
        )

    async def _get_and_send_outbound_megolm_session(
        self,
        room_id: str,
        otk_request_timeout: typing.Optional[int] = None,
    ) -> OutboundMegolmSession:
        storage_key = f"outbound_megolm_session.{room_id}"
        if storage_key in self.client.storage:
            session_id = self.client.storage[storage_key]
            outbound_session = OutboundMegolmSession(
                self.client,
                room_id,
                room_state_tracker=self.room_state_tracker,
                device_tracker=self.device_tracker,
                device_keys_manager=self.device_keys_manager,
                key=self.key,
                session_id=session_id,  # passing in the session ID loads it from storage
            )
        else:
            outbound_session = OutboundMegolmSession(
                self.client,
                room_id,
                room_state_tracker=self.room_state_tracker,
                device_tracker=self.device_tracker,
                device_keys_manager=self.device_keys_manager,
                key=self.key,
            )
            InboundMegolmSession.from_outbound_session(
                self.client,
                outbound_session,
                key=self.key,
            )
            self.client.storage[storage_key] = outbound_session.session_id
        try:
            session_keys = await outbound_session.get_session_key_for_sending()
        except SessionExpiredException:
            outbound_session = OutboundMegolmSession(
                self.client,
                room_id,
                room_state_tracker=self.room_state_tracker,
                device_tracker=self.device_tracker,
                device_keys_manager=self.device_keys_manager,
                key=self.key,
            )
            InboundMegolmSession.from_outbound_session(
                self.client,
                outbound_session,
                key=self.key,
            )
            self.client.storage[storage_key] = outbound_session.session_id
            session_keys = await outbound_session.get_session_key_for_sending()

            # remove the old outbound session from storage
            del self.client.storage[
                f"outbound_megolm_session.{room_id}.{outbound_session.session_id}"
            ]

        if len(session_keys) != 0:
            keys_to_send = [
                (device_key, "m.room_key", content)
                for (device_key, content) in session_keys
            ]
            await self._encrypt_and_send_to_device(keys_to_send, otk_request_timeout)

            outbound_session.mark_as_sent(
                [device_key for (device_key, _) in session_keys]
            )

        return outbound_session

    async def encrypt_and_send_room_event(
        self,
        room_id: str,
        event_type: str,
        event_content: dict[str, typing.Any],
        retry_ms: int = 0,
        txn_id: typing.Optional[str] = None,
    ) -> None:
        """Send an event to a room

        Arguments:

        ``room_id``:
          the ID of the room to send to
        ``event_type``:
          the type of event to send
        ``event_content``:
          the content of the event
        ``retry_ms``:
          how long to retry sending, in milliseconds
        ``txn_id``:
          the transaction ID to use.  If none is specified, one is generated.
        """
        session = await self._get_and_send_outbound_megolm_session(room_id)
        encrypted_content = session.encrypt(event_type, event_content)
        await self.client.send_event(
            room_id,
            "m.room.encrypted",
            encrypted_content,
            retry_ms,
            txn_id,
        )

    async def pre_send_megolm_session(self, room_id: str) -> None:
        """Pre-send the Megolm session for a room.

        If the room does not yet have an outbound Megolm session, or the Megolm
        session needs to be rotated, a new one will be created.

        This can be used before a room event is ready to be sent, to reduce the
        number of devices that we need to send the Megolm session to

        Arguments:

        ``room_id``:
          the ID of the room
        """
        # FIXME: explain the timeout
        await self._get_and_send_outbound_megolm_session(room_id, 1000)

    def discard_session(self, room_id: str) -> None:
        """
        Discard the outbound Megolm session for a room

        This forces a new session to be created the next time we send an event to
        the room.

        Arguments:

        ``room_id``:
          the ID of the room
        """
        storage_key = f"outbound_megolm_session.{room_id}"
        if storage_key in self.client.storage:
            session_id = self.client.storage[storage_key]
            del self.client.storage[storage_key]
            del self.client.storage[f"outbound_megolm_session.{room_id}.{session_id}"]


class EncryptionEnabled(typing.NamedTuple):
    """A message indicating that encryption was enabled in a room"""

    room_id: str
    algorithm: str


class EncryptionDisabled(typing.NamedTuple):
    """A message indicating that encryption was disabled in a room"""

    room_id: str
    original_algorithm: str


EncryptionDisabled.original_algorithm.__doc__ = (
    "The encryption algorithm that was initially set for the room"
)


class UnknownRoomEncryptionAlgorithm(typing.NamedTuple):
    """A message indicating that the encryption algorithm for a room was
    initially set to an unknown algorithm"""

    room_id: str
    algorithm: str


class EncryptionAlgorithmChanged(typing.NamedTuple):
    """A message indicating that the encryption algorithm for a room changed"""

    room_id: str
    original_algorithm: str
    new_algorithm: str
