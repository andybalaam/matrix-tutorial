# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Error handling"""


class NotMatrixServerError(RuntimeError):
    """The server is not a Matrix server"""

    pass


class MatrixError(RuntimeError):
    """Wraps a Matrix API error"""

    def __init__(self, code, body):
        self.code = code
        self.body = body

    # FIXME: add a __str__ function


class InvalidResponseError(RuntimeError):
    """The server's response did not match the expected format"""

    pass


class UnableToDecryptError(RuntimeError):
    """We were unable to decrypt the message"""

    pass


class EncryptionDisabledError(RuntimeError):
    """Encryption was enabled in a room but is now disabled"""

    pass


class EncryptionAlgorithmChangedError(RuntimeError):
    """The encryption algorithm in a room was changed from its initial
    algorithm"""

    def __init__(self, initial_algorithm: str, new_algorithm: str):
        super()
        self.initial_algorithm = initial_algorithm
        self.new_algorithm = new_algorithm


class UnknownEncryptionAlgorithmError(RuntimeError):
    """The initial encryption algorithm for a room is not a known algorithm"""

    def __init__(self, algorithm: str):
        super()
        self.algorithm = algorithm
