# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Olm-related functionality"""

import asyncio
from base64 import b64decode, b64encode
import json
import os
import sys
import typing
import vodozemac

from .client import Client
from . import devices
from . import error
from . import rooms
from . import schema


OLM_ALGORITHM = "m.olm.v1.curve25519-aes-sha2"


class OlmChannel:
    """Manages a set of Olm sessions with another device"""

    client: Client
    device_keys_manager: devices.DeviceKeysManager
    key: bytes
    partner_user_id: str
    partner_device_id: typing.Optional[str]
    partner_identity_key: str
    partner_fingerprint_key: typing.Optional[str]
    sessions: list[vodozemac.Session]

    def __init__(self):
        """Do not use initializer function.  Use the ``create_*`` methods instead"""
        raise RuntimeError("Use the create_* methods instead")  # pragma: no cover

    @classmethod
    def create_outbound_channel(
        cls,
        c: Client,
        device_keys_manager: devices.DeviceKeysManager,
        recipient_device_keys: dict,
        recipient_one_time_key: dict,
        key: typing.Optional[bytes] = None,
    ) -> "OlmChannel":
        """Create a new channel with a new Olm session

        Arguments:

        ``c``:
          the client object
        ``device_keys_manager``:
          a ``DeviceKeysManager`` object
        ``recipient_device_keys``:
          the other party's device keys, as returned by ``/keys/query``
        ``recipient_one_time_key``:
          the other party's signed one-time key, as returned by ``/keys/claim``
        ``key``:
          a 32-byte binary used to encrypt the objects in storage.  If not
          specified, uses the same key as used by ``device_keys_manager``

        Returns a new ``OlmChannel`` with an Olm session
        """
        obj = cls.__new__(cls)

        obj.client = c
        obj.device_keys_manager = device_keys_manager
        obj.key = key if key else device_keys_manager.key

        obj.partner_user_id = recipient_device_keys["user_id"]
        obj.partner_device_id = recipient_device_keys["device_id"]
        partner_keys = recipient_device_keys["keys"]
        obj.partner_identity_key = partner_keys[f"curve25519:{obj.partner_device_id}"]
        obj.partner_fingerprint_key = partner_keys[f"ed25519:{obj.partner_device_id}"]

        # check the signature on the device keys
        devices.verify_json_ed25519(
            typing.cast(str, obj.partner_fingerprint_key),
            obj.partner_user_id,
            typing.cast(str, obj.partner_device_id),
            recipient_device_keys,
        )

        obj.sessions = []
        # note: add_outbound_olm_session will check the signature on the OTK, so we
        # don't need to check the signature separately
        obj.add_outbound_olm_session(recipient_one_time_key)

        obj._store_session_data()

        return obj

    def add_outbound_olm_session(self, recipient_one_time_key: dict) -> None:
        """Add a new Olm session to the channel

        Arguments:

        ``recipient_one_time_key``:
          the other party's signed one-time key, as returned by ``/keys/claim``
        """
        if self.partner_fingerprint_key == None or self.partner_device_id == None:
            raise RuntimeError("Unable to verify signature")

        devices.verify_json_ed25519(
            typing.cast(str, self.partner_fingerprint_key),
            self.partner_user_id,
            typing.cast(str, self.partner_device_id),
            recipient_one_time_key,
        )

        session = self.device_keys_manager.account.create_outbound_session(
            self.partner_identity_key,
            recipient_one_time_key["key"],
        )
        self.sessions.append(session)

    def _store_session_data(self, pickle=False) -> None:
        name = f"olm_session.{self.partner_user_id}.{self.partner_identity_key}"
        to_store: dict[str, typing.Any] = {
            "sessions": [session.pickle(self.key) for session in self.sessions]
        }
        if self.partner_device_id != None:
            to_store["partner_device_id"] = self.partner_device_id
        if self.partner_fingerprint_key != None:
            to_store["partner_fingerprint_key"] = self.partner_fingerprint_key
        self.client.storage[name] = to_store

    def encrypt(self, event_type: str, content: dict) -> dict:
        """Encrypt an event using Olm

        Arguments:

        ``event_type``:
          the type of the event (e.g. ``m.room.message``)
        ``content``:
          the event ``content``

        Returns the ``content`` of a ``m.room.encrypted`` event
        """
        if len(self.sessions) == 0:
            raise RuntimeError("No Olm session available")

        plaintext = json.dumps(
            {
                "type": event_type,
                "content": content,
                "sender": self.client.user_id,
                "recipient": self.partner_user_id,
                "recipient_keys": {"ed25519": self.partner_fingerprint_key},
                "keys": {"ed25519": self.device_keys_manager.fingerprint_key},
            }
        )
        # sessions[-1] is the last item in the list
        olm_message = self.sessions[-1].encrypt(plaintext)
        self._store_session_data()

        return {
            "algorithm": OLM_ALGORITHM,
            "sender_key": self.device_keys_manager.identity_key,
            "ciphertext": {
                self.partner_identity_key: {
                    "type": olm_message.message_type,
                    "body": olm_message.ciphertext,
                },
            },
        }

    def decrypt(self, event_content: dict) -> dict:
        """Decrypt an ``m.room.encrypted`` event encrypted with Olm

        Creates a new Olm session if necessary.

        Arguments:

        ``event_content``:
          the ``content`` of the ``m.room.encrypted`` event

        Returns the decrypted event, which will be a dict that should have ``type``
        (the decrypted event type), ``content`` (the event content), and
        information about the sender and recipient.
        """
        schema.ensure_valid(
            event_content,
            {
                "algorithm": str,
                "sender_key": str,
                "ciphertext": schema.Object({"type": int, "body": str}),
            },
        )

        if event_content["algorithm"] != OLM_ALGORITHM:
            raise RuntimeError("Invalid algorithm")

        olm_message_dict = event_content.get("ciphertext", {}).get(
            self.device_keys_manager.identity_key
        )
        if olm_message_dict == None:
            raise RuntimeError("The message is not encrypted for us")

        olm_message = vodozemac.OlmMessage(
            olm_message_dict["type"], olm_message_dict["body"]
        )

        for index in range(len(self.sessions) - 1, -1, -1):
            session = self.sessions[index]
            try:
                plaintext_str = session.decrypt(olm_message)
            except:
                continue

            # on successful decryption, make sure it's the last in our list
            if index != len(self.sessions) - 1:
                self.sessions[index], self.sessions[-1] = (
                    self.sessions[-1],
                    self.sessions[index],
                )
            self._store_session_data()

            plaintext = json.loads(plaintext_str)
            self._check_plaintext(plaintext)

            return plaintext

        if olm_message_dict["type"] == 0:
            try:
                (
                    session,
                    plaintext_str,
                ) = self.device_keys_manager.account.create_inbound_session(
                    self.partner_identity_key,
                    olm_message,
                )
            except:
                raise error.UnableToDecryptError()

            self.sessions.append(session)
            # FIXME: truncate self.sessions
            self._store_session_data()

            plaintext = json.loads(plaintext_str)
            self._check_plaintext(plaintext)

            return plaintext

        raise error.UnableToDecryptError()

    def _check_plaintext(self, plaintext: dict) -> None:
        schema.ensure_valid(
            plaintext,
            {
                "type": str,
                "content": dict,
                "sender": str,
                "recipient": str,
                "recipient_keys": {"ed25519": str},
                "keys": {"ed25519": str},
            },
        )
        if (
            plaintext["sender"] != self.partner_user_id
            or plaintext["recipient"] != self.client.user_id
            or plaintext["recipient_keys"]["ed25519"]
            != self.device_keys_manager.fingerprint_key
        ):
            raise RuntimeError("Invalid message")
        if self.partner_fingerprint_key:
            if plaintext["keys"]["ed25519"] != self.partner_fingerprint_key:
                raise RuntimeError("Mismatched fingerprint key")
        else:
            self.partner_fingerprint_key = plaintext["keys"]["ed25519"]
            self._store_session_data()

    @classmethod
    def create_from_encrypted_event(
        cls,
        c: Client,
        device_keys_manager: devices.DeviceKeysManager,
        partner_user_id: str,
        partner_identity_key: str,
        event_content: dict,
        partner_device_id: typing.Optional[str] = None,
        partner_fingerprint_key: typing.Optional[str] = None,
        key: typing.Optional[bytes] = None,
    ) -> typing.Tuple["OlmChannel", typing.Union[dict, BaseException]]:
        """Create a new channel from an encrypted message

        Arguments:

        ``c``:
          the client object
        ``device_keys_manager``:
          a ``DeviceKeysManager`` object
        ``partner_user_id``:
          the other party's user ID, as returned by ``/keys/query``
        ``partner_identity_key``:
          the other party's identity key
        ``event_content``:
          the content of the ``m.room.encrypted`` event
        ``partner_device_id``:
          the other party's device ID.  You will not be able to create a new
          outbound Olm session without an device ID.  The device ID can be set
          later by setting the ``OlmChannel`` object's ``partner_device_id``
          property
        ``partner_fingerprint_key``:
          the other party's fingerprint key.  If not provided, will be set to the
          fingerprint key provided in the plaintext.  However, the message may not
          be trusted unless it matches the device key obtained from the server, and
          this key should be provided if it is available.
        ``key``:
          a 32-byte binary used to encrypt the objects in storage.  If not
          specified, uses the same key as used by ``device_keys_manager``

        On success, returns a tuple consisting of the new OlmChannel object and the
        decrypted message.  On failure, either raises an exception, or returns a
        tuple consisting of the new OlmChannel object and the exception, depending
        on whether the OlmChannel could be created.
        """
        obj = cls.__new__(cls)

        obj.client = c
        obj.device_keys_manager = device_keys_manager
        obj.key = key if key else device_keys_manager.key

        obj.partner_user_id = partner_user_id
        obj.partner_device_id = partner_device_id
        obj.partner_identity_key = partner_identity_key
        obj.partner_fingerprint_key = partner_fingerprint_key

        obj.sessions = []

        try:
            decrypted = obj.decrypt(event_content)
        except error.UnableToDecryptError:
            raise
        except:
            if len(obj.sessions) == 0:
                raise
            else:
                e = typing.cast(BaseException, sys.exc_info()[1])
                return obj, e

        return obj, decrypted

    @classmethod
    def create_from_storage(
        cls,
        c: Client,
        device_keys_manager: devices.DeviceKeysManager,
        partner_user_id: str,
        partner_identity_key: str,
        key: typing.Optional[bytes] = None,
    ) -> typing.Optional["OlmChannel"]:
        """Loads an Olm channel from storage

        ``c``:
          the client object
        ``device_keys_manager``:
          a ``DeviceKeysManager`` object
        ``partner_user_id``:
          the other party's user ID, as returned by ``/keys/query``
        ``partner_identity_key``:
          the other party's identity key
        ``key``:
          a 32-byte binary used to encrypt the objects in storage.  If not
          specified, uses the same key as used by ``device_keys_manager``
        """
        name = f"olm_session.{partner_user_id}.{partner_identity_key}"
        stored = c.storage.get(name)
        if stored == None:
            return None

        obj = cls.__new__(cls)

        obj.client = c
        obj.device_keys_manager = device_keys_manager
        obj.key = key if key else device_keys_manager.key

        obj.partner_user_id = partner_user_id
        obj.partner_identity_key = partner_identity_key

        obj.partner_device_id = stored.get("partner_device_id")
        obj.partner_fingerprint_key = stored.get("partner_fingerprint_key")
        obj.sessions = [
            vodozemac.Session.from_pickle(session, obj.key)
            for session in stored["sessions"]
        ]

        return obj

    def assert_partner_device_id(self, device_id: str) -> None:
        if self.partner_device_id is None:
            self.partner_device_id = device_id
            self._store_session_data()
        elif self.partner_device_id != device_id:
            # FIXME:
            raise RuntimeError("Mismatched device ID")
