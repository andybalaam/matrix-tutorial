# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Room-related functionality"""

import typing

from . import client
from . import events


class RoomStateTracker:
    """Tracks room state"""

    def __init__(self, c: client.Client):
        self.client = c
        c.publisher.subscribe(
            (client.RoomStateUpdates, client.RoomTimelineUpdates), self._subscriber
        )

    def _subscriber(
        self, msg: typing.Union[client.RoomStateUpdates, client.RoomTimelineUpdates]
    ) -> None:
        state = self.client.storage.get("room_state_tracker", {})
        room_state = state.setdefault(msg.room_id, {})
        for e in msg.events:
            # loop through the state events and save them under room_state[type][state_key]
            if isinstance(e, events.StateEvent):
                room_state.setdefault(e.type, {})[e.state_key] = e.to_dict()
        self.client.storage["room_state_tracker"] = state

    def get_state(
        self,
        room_id: str,
        event_type: str,
        state_key: str = "",
    ) -> typing.Optional[events.StateEvent]:
        """Get the current state for the given event type and state key

        Returns ``None`` if no state is found.
        """
        state = self.client.storage.get("room_state_tracker", {})
        event = state.get(room_id, {}).get(event_type, {}).get(state_key)
        return events.StateEvent(**event) if event else None

    def get_all_state_for_type(
        self,
        room_id: str,
        event_type: str,
    ) -> dict[str, events.StateEvent]:
        """Get all the current state for the given event type

        Returns a ``dict`` mapping from state key to state event.
        """
        state = self.client.storage.get("room_state_tracker", {})
        state_events = state.get(room_id, {}).get(event_type, {})
        return {
            name: events.StateEvent(**value) for name, value in state_events.items()
        }
