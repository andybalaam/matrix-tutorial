# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""General Matrix client functionality"""

import aiohttp
import asyncio
import logging
import math
import os
import re
import sys
import time
import typing
from urllib.parse import quote as urlquote, urljoin, urlparse

from . import error
from . import events
from . import pubsub
from . import schema


class Client:
    """Represents a client connection"""

    def __init__(
        self,
        storage: "Storage",
        callbacks: dict = {},
        base_client_url: typing.Optional[str] = None,
        well_known: typing.Optional[dict[str, typing.Any]] = None,
        versions: typing.Optional[dict[str, typing.Any]] = None,
    ):
        """
        Arguments:

        ``storage``:
            The storage object to use
        ``callbacks``:
            Functions that will be called to perform processing needed by the
            client
        ``base_client_url``:
            The base URL to use to connect to the server, including the
            ``_matrix/client/`` suffix.  If omitted, the client must have
            previously logged in and stored the base URL in the storage
        ``well_known``:
            The contents of the ``.well-known`` file obtained from server
            discovery.
        ``versions``:
            The result of calling ``/_matrix/client/versions`` on the server,
            for example as returned by the ``discover`` function.
        """
        self.storage = storage
        if base_client_url:
            self.storage["base_client_url"] = base_client_url
            if well_known:
                self.storage["well_known"] = well_known
            if versions:
                self.storage["versions"] = versions
        else:
            if "base_client_url" not in storage:
                raise RuntimeError("No base URL available")

        self.callbacks = callbacks

        self.http_session = aiohttp.ClientSession()

        self.publisher = pubsub.Publisher()

        self.txn_count = 0

        self.pid = os.getpid()
        # alternatively: self.start_time = time.time_ns()

        self.sync_task: typing.Optional[asyncio.Task[None]] = None

    async def close(self) -> None:
        """Free up resources used by the client."""
        await self.http_session.close()

        await self.publisher.publish(ClientClosed())

        self.stop_sync()

    async def __aenter__(self) -> "Client":
        return self

    async def __aexit__(self, *ignored: typing.Any) -> None:
        await self.close()

    def url(self, endpoint: str) -> str:
        """Returns the full URL of a given endpoint"""
        return urljoin(self.storage["base_client_url"], endpoint)

    async def login_methods(self) -> list[str]:
        """Get a list of the login methods supported by the server."""
        async with self.http_session.get(self.url("v3/login")) as resp:
            code, body = await check_response(resp)
            try:
                return [flow["type"] for flow in body["flows"]]
            except KeyError:
                raise error.InvalidResponseError()

    async def log_in_with_password(
        self,
        identifier: typing.Union[str, dict[str, typing.Any]],
        password: str,
        identifier_type: str = "user",  # FIXME: make this an Union[{enum}, str]
        **kwargs: dict[str, typing.Any],
    ) -> dict[str, typing.Any]:
        """Log in to the server

        Arguments:

        ``identifier``:
          the identifier to use.  The format depends on the identifier type, but in
          most cases will be a string.
        ``password``:
          the password to use.
        ``identifier_type``:
          the type of identifer to use.  The default, ``'user'`` indicates that the
          identifier is a username or user ID.  Other recognized values are
          ``'email'`` and ``'phone'``.  Any other values will be passed unchanged
          to the login endpoint.  For ``'phone'``, ``identifier`` is either a
          ``dict`` with ``country`` (the two-letter country code that the phone
          number is from) and ``phone`` (the actual phone number) properties,
          or a the canonicalised phone number as a string.  For unknown values,
          the ``identifier`` should be a ``dict``.
        """
        if "access_token" in self.storage:
            raise RuntimeError("Already logged in")  # FIXME: use a custom exception

        login_identifier: typing.Union[str, dict[str, typing.Any]]

        if identifier_type == "user":
            login_identifier = {
                "type": "m.id.user",
                "user": identifier,
            }
        elif identifier_type == "email":
            login_identifier = {
                "type": "m.id.thirdparty",
                "medium": "email",
                "address": identifier,
            }
        elif identifier_type == "phone":
            if type(identifier) == str:
                login_identifier = {
                    "type": "m.id.thirdparty",
                    "medium": "msisdn",
                    "address": identifier,
                }
            else:
                login_identifier = typing.cast(dict[str, typing.Any], identifier)
                login_identifier["type"] = "m.id.phone"
        elif type(identifier) == dict:
            login_identifier = identifier
            login_identifier["type"] = identifier_type
        else:
            raise RuntimeError("Unsupported identifier")

        req_body = {
            "type": "m.login.password",
            "identifier": login_identifier,
            "password": password,
            "refresh_token": True,
        }

        for key in kwargs:
            req_body[key] = kwargs[key]

        return await self._do_log_in(req_body)

    async def _do_log_in(
        self, req_body: dict[str, typing.Any]
    ) -> dict[str, typing.Any]:
        async with self.http_session.post(self.url("v3/login"), json=req_body) as resp:
            code, resp_body = await check_response(resp)

        schema.ensure_valid(
            resp_body,
            {
                "access_token": str,
                "expires_in_ms": schema.Optional(int),
                "refresh_token": schema.Optional(str),
                "user_id": str,
                "device_id": str,
                "well_known": schema.Optional(
                    {
                        "m.homeserver": {
                            "base_url": str,
                        },
                    },
                ),
            },
        )

        self.storage["access_token"] = resp_body["access_token"]
        if "expires_in_ms" in resp_body:
            self.storage["access_token_valid_until"] = (
                time.time_ns() + resp_body["expires_in_ms"] * 1_000_000
            )
            # if the access token expires, we need a refresh token
            if "refresh_token" not in resp_body:
                raise error.InvalidResponseError()
            self.storage["refresh_token"] = resp_body["refresh_token"]

        self.storage["user_id"] = resp_body["user_id"]

        self.storage["device_id"] = resp_body["device_id"]

        if "well_known" in resp_body:
            well_known = resp_body["well_known"]
            self.storage["well_known_from_login"] = well_known
            base_url = urlparse(well_known["m.homeserver"]["base_url"])
            if base_url.scheme == "http" and base_url.scheme == "https":
                # ensure nonempty path ends with a "/" so that URL joining works
                if base_url.path != "" and not base_url.path.endswith("/"):
                    base_url.path += "/"

                self.storage["base_client_url"] = urljoin(
                    base_url.geturl(), "_matrix/client/"
                )

        return resp_body

    @property
    def user_id(self) -> typing.Union[str, None]:
        """The user ID of the logged-in user, or ``None`` if not logged in"""
        return self.storage["user_id"] if "user_id" in self.storage else None

    @property
    def device_id(self) -> typing.Union[str, None]:
        """The device ID of the logged-in user, or ``None`` if not logged in"""
        return self.storage["device_id"] if "device_id" in self.storage else None

    async def authenticated(self, req_func, *args, **kwargs) -> aiohttp.ClientResponse:
        """Make an authenticated request to the homeserver.

        Arguments:

        ``req_func``:
            the function to call to make the request.  e.g. to make an
            authenticated ``GET`` request, this would be ``self.http_session.get``.
        ``*args, **kwargs``:
            the arguments to pass to ``req_func``.
        """
        if "access_token" not in self.storage:
            raise RuntimeError("Not logged in")  # FIXME: use a custom exception

        if (
            "access_token_valid_until" in self.storage
            and self.storage["access_token_valid_until"] < time.time_ns()
        ):
            try:
                await self._refresh_access_token()
            except:
                # refresh failed, so we're logged out
                self.storage.clear()
                raise

        headers = kwargs.get("headers", {})
        headers["Authorization"] = "Bearer " + self.storage["access_token"]
        kwargs["headers"] = headers
        resp = await req_func(*args, **kwargs)

        if resp.status >= 400 and resp.content_type == "application/json":
            body = await resp.json()
            if body.get("errcode") == "M_UNKNOWN_TOKEN":
                if "refresh_token" in self.storage:
                    await self._refresh_access_token()
                else:
                    await self._handle_soft_logout(resp.status, body)

                # update was successful, so retry the request
                return await self.authenticated(req_func, *args, **kwargs)

        return resp

    async def _refresh_access_token(self) -> None:
        """Try to refresh an expired access token"""
        req_body = {"refresh_token": self.storage["refresh_token"]}
        async with self.http_session.post(
            self.url("v3/refresh"), json=req_body
        ) as resp:
            try:
                status, resp_body = await check_response(resp)
            except error.MatrixError as e:
                if e.body["errcode"] == "M_UNKNOWN_TOKEN":
                    return await self._handle_soft_logout(e.code, e.body)
                else:
                    raise
            # FIXME: if error.NotMatrixServerError, try again a few times, because
            # it may be a temporary condition

            if schema.is_valid(
                resp_body,
                {
                    "access_token": str,
                    "expires_in_ms": schema.Optional(int),
                    "refresh_token": schema.Optional(str),
                },
            ):
                # refresh successful; save the new values
                if "expires_in_ms" in resp_body:
                    if "refresh_token" not in resp_body:
                        raise error.InvalidResponseError()
                    self.storage["access_token_valid_until"] = (
                        time.time_ns() + resp_body["expires_in_ms"] * 1_000_000
                    )
                    self.storage["refresh_token"] = resp_body["refresh_token"]
                else:
                    del self.storage["access_token_valid_until"]
                    del self.storage["refresh_token"]
                self.storage["access_token"] = resp_body["access_token"]
            else:
                raise error.InvalidResponseError()

    async def _handle_soft_logout(self, status, body) -> None:
        if body.get("soft_logout"):
            await self.callbacks["re_log_in"](self)
        else:
            # we've been fully logged out, so we clear all our data and
            # raise an exception
            # FIXME: we should also emit an event to tell the client that we're logged out
            self.storage.clear()
            raise error.MatrixError(status, body)

    async def re_log_in(
        self,
        auth_parameter: typing.Union[str, dict[str, typing.Any]],
        login_type: str = "password",
    ) -> None:
        """Re-log in after getting soft logged out

        Arguments:

        ``auth_parameter``:
            The authentication parameter (e.g. password or token) to use.
        ``login_type``:
            The method of logging in.  The default value, ``'password'``, indicates
            that you are logging in with a password.  A value of ``'token'``
            indicates that you are logging in with a token.  An unknown value will
            set that as the ``type`` in the request body, and ``auth_parameter``
            must be a ``dict`` whose contents will be copied to the request body.
        """
        req_body = {
            "identifier": {
                "type": "m.id.user",
                "user": self.storage["user_id"],
            },
            "device_id": self.storage["device_id"],
            "refresh_token": True,
        }

        if login_type == "password":
            req_body["type"] = "m.login.password"
            req_body["password"] = auth_parameter
        elif login_type == "token":
            req_body["type"] = "m.login.token"
            req_body["token"] = auth_parameter
        elif type(auth_parameter) == dict:
            req_body["type"] = login_type
            for name in auth_parameter:
                req_body[name] = auth_parameter[name]
        else:
            raise RuntimeError("Unsupported authentication parameter")

        await self._do_log_in(req_body)

    async def log_out(self) -> None:
        """Log out the current session and clear out the storage."""
        async with await self.authenticated(
            self.http_session.post,
            self.url("v3/logout"),
            json={},
        ) as resp:
            await check_response(resp)
            self.storage.clear()

    def make_txn_id(self) -> str:
        """Generate a unique transaction ID"""
        self.txn_count = self.txn_count + 1
        return f"{self.pid}_{time.time_ns()}_{self.txn_count}"
        # alternatively: f"{self.start_time}_{time.time_ns()}_{self.txn_count}"

    async def send_event(
        self,
        room_id: str,
        event_type: str,
        event_content: dict[str, typing.Any],
        retry_ms: int = 0,
        txn_id: typing.Optional[str] = None,
    ) -> typing.Tuple[str, str]:
        """Send an event to a room

        Arguments:

        ``room_id``:
          the ID of the room to send to
        ``event_type``:
          the type of event to send
        ``event_content``:
          the content of the event
        ``retry_ms``:
          how long to retry sending, in milliseconds
        ``txn_id``:
          the transaction ID to use.  If none is specified, one is generated.
        """
        txn_id = txn_id or self.make_txn_id()
        url = self.url(
            f"v3/rooms/{urlquote(room_id, '')}/send/{urlquote(event_type, '')}/{txn_id}"
        )
        resp = await retry(
            retry_ms, self.authenticated, self.http_session.put, url, json=event_content
        )
        async with resp:
            status, resp_body = await check_response(resp)
            schema.ensure_valid(resp_body, {"event_id": str})
            return (resp_body["event_id"], txn_id)

    def start_sync(self):
        """Start the sync loop"""
        if self.sync_task != None:
            # sync already started, so do nothing
            return
        self.sync_task = asyncio.create_task(self._run_sync_loop())

    def stop_sync(self):
        """Stop the sync loop"""
        if self.sync_task == None:
            # no sync running, so do nothing
            return
        self.sync_task.cancel()
        self.sync_task = None

    async def _run_sync_loop(self) -> None:
        backoff = 2
        while True:
            params = {"timeout": "30000"}
            if "sync_token" in self.storage:
                params["since"] = self.storage["sync_token"]
            # TODO: add other params
            try:
                resp = await self.authenticated(
                    self.http_session.get,
                    self.url("v3/sync"),
                    params=params,
                )
            except asyncio.CancelledError:
                return
            except:
                e = sys.exc_info()[1]
                logging.error(
                    "Sync call failed (%r).  Waiting %d s and retrying.", e, backoff
                )
                await self.publisher.publish(SyncFailed(backoff))
                await asyncio.sleep(backoff)
                backoff = min(backoff * 2, 30)
                continue

            async with resp:
                try:
                    code, body = await check_response(resp)
                    schema.ensure_valid(
                        body,
                        {
                            "next_batch": str,
                            "rooms": schema.Optional(
                                {
                                    "join": schema.Optional(
                                        schema.Object(
                                            {
                                                "timeline": schema.Optional(
                                                    {
                                                        "events": schema.Array(
                                                            events.RoomEvent.schema_without_room_id()
                                                        ),
                                                        "limited": schema.Optional(
                                                            bool
                                                        ),
                                                        "prev_batch": schema.Optional(
                                                            str
                                                        ),
                                                    }
                                                ),
                                                "state": schema.Optional(
                                                    {
                                                        "events": schema.Optional(
                                                            schema.Array(
                                                                events.StateEvent.schema_without_room_id()
                                                            )
                                                        ),
                                                    }
                                                ),
                                                # FIXME: plus other properties
                                            }
                                        )
                                    ),
                                    "leave": schema.Optional(
                                        schema.Object(
                                            {
                                                "timeline": schema.Optional(
                                                    {
                                                        "events": schema.Array(
                                                            events.RoomEvent.schema_without_room_id()
                                                        ),
                                                        "limited": schema.Optional(
                                                            bool
                                                        ),
                                                        "prev_batch": schema.Optional(
                                                            str
                                                        ),
                                                    }
                                                ),
                                                "state": schema.Optional(
                                                    {
                                                        "events": schema.Optional(
                                                            schema.Array(
                                                                events.StateEvent.schema_without_room_id()
                                                            )
                                                        ),
                                                    }
                                                ),
                                            }
                                        )
                                    ),
                                    # FIXME: plus other properties
                                }
                            ),
                            "device_one_time_keys_count": schema.Optional(
                                dict[str, int]
                            ),
                            "device_lists": schema.Optional(
                                {
                                    "changed": schema.Optional(list[str]),
                                    "left": schema.Optional(list[str]),
                                }
                            ),
                            "to_device": schema.Optional(
                                {
                                    "events": schema.Optional(
                                        schema.Array(events.Event.schema())
                                    )
                                }
                            ),
                        },
                    )
                except asyncio.CancelledError:
                    return
                except:
                    e = sys.exc_info()[1]
                    logging.error(
                        "Sync call failed (%r).  Waiting %d s and retrying.", e, backoff
                    )
                    await self.publisher.publish(SyncFailed(backoff))
                    await asyncio.sleep(backoff)
                    backoff = min(backoff * 2, 30)
                    continue

                if self.sync_task == None:
                    # the sync was stopped, so we shouldn't process the result.  We
                    # should get a CancelledError if the sync was stopped, but check
                    # anyways, just in case
                    return

                if backoff != 2:
                    # the previous request failed
                    await self.publisher.publish(SyncResumed())
                    backoff = 2

                self.storage["sync_token"] = body["next_batch"]

                if "rooms" in body:
                    if "join" in body["rooms"]:
                        for room_id, room_data in body["rooms"]["join"].items():
                            if "state" in room_data:
                                await self.publisher.publish(
                                    RoomStateUpdates.create_from_sync(
                                        room_id, room_data["state"]
                                    ),
                                )
                            if "timeline" in room_data:
                                await self.publisher.publish(
                                    RoomTimelineUpdates.create_from_sync(
                                        room_id, room_data["timeline"]
                                    ),
                                )

                    if "leave" in body["rooms"]:
                        for room_id, room_data in body["rooms"]["leave"].items():
                            if "state" in room_data:
                                await self.publisher.publish(
                                    RoomStateUpdates.create_from_sync(
                                        room_id, room_data["state"]
                                    ),
                                )
                            if "timeline" in room_data:
                                await self.publisher.publish(
                                    RoomTimelineUpdates.create_from_sync(
                                        room_id, room_data["timeline"]
                                    ),
                                )
                            await self.publisher.publish(LeftRoom(room_id))

                await self.publisher.publish(
                    OneTimeKeysCount(
                        body.get("device_one_time_keys_count", {}),
                        body.get("to_device", {}).get("events", []) != [],
                    )
                )

                if "device_lists" in body:
                    await self.publisher.publish(
                        DeviceChanges(
                            body["device_lists"].get("changed", []),
                            body["device_lists"].get("left", []),
                        )
                    )

                if "to_device" in body and "events" in body["to_device"]:
                    await self.publisher.publish(
                        ToDeviceEvents(
                            [events.Event(**e) for e in body["to_device"]["events"]]
                        )
                    )

    async def claim_otks(
        self,
        algorithm: str,
        devices: dict[str, list[str]],
        timeout: typing.Optional[int] = None,
    ) -> typing.Tuple[dict[str, dict[str, typing.Tuple[str, typing.Any]]], list[str]]:
        """Claim one-time keys for the given devices.

        Arguments:

        ``algorithm``:
          the key algorithm for the one-time keys that we want to request
        ``devices``:
          the devices that we want to request one-time keys for.  This is given as
          a ``dict``, mapping user IDs to a list of device IDs
        ``timeout``:
          how long to wait, in milliseconds, for a response from remote servers

        Returns a tuple consisting of:

        - a dict mapping user IDs to device IDs to (key ID, one-time key) tuple.  A
          requested device may be missing, indicating that no one-time key for that
          device could be obtained
        - a list of remote servers that failed to respond in time
        """

        otk_map = {
            user_id: {device_id: algorithm for device_id in device_ids}
            for user_id, device_ids in devices.items()
        }
        body: dict[str, typing.Any] = {"one_time_keys": otk_map}
        if timeout != None:
            body["timeout"] = timeout
        url = self.url("v3/keys/claim")
        resp = await self.authenticated(self.http_session.post, url, json=body)
        async with resp:
            status, resp_body = await check_response(resp)
            schema.ensure_valid(
                resp_body,
                {
                    "one_time_keys": dict[str, dict[str, dict]],
                    "failures": schema.Optional(dict),
                },
            )

            one_time_keys = {}
            for user_id, device_otks in resp_body["one_time_keys"].items():
                user_otks = {}
                for device_id, device_otk_map in device_otks.items():
                    # `popitem` removes an item from a dict.  The device_otk_map
                    # is a map from key ID to key, and should only have one item
                    keyid, _ = user_otks[device_id] = device_otk_map.popitem()
                    if not keyid.startswith(algorithm + ":"):
                        raise error.InvalidResponseError()
                one_time_keys[user_id] = user_otks

            if "failures" in resp_body:
                # `list` returns a list of the keys in the dict.  The values in the
                # "failures" property are not used
                failures = list(resp_body["failures"])
            else:
                failures = []

            return one_time_keys, failures

    async def send_to_device(
        self,
        event_type: str,
        event_contents: dict[str, dict[str, dict]],
        retry_ms: int = 0,
        txn_id: typing.Optional[str] = None,
    ) -> None:
        """Send an events to devices

        Arguments:

        ``room_id``:
          the ID of the room to send to
        ``event_type``:
          the type of event to send
        ``event_contents``:
          a map from user ID to device ID to event content.  If "*" is used as the
          device ID, the event is sent to all of the user's devices.
        ``retry_ms``:
          how long to retry sending, in milliseconds
        """
        txn_id = txn_id or self.make_txn_id()
        url = self.url(f"v3/sendToDevice/{urlquote(event_type, '')}/{txn_id}")
        resp = await retry(
            retry_ms,
            self.authenticated,
            self.http_session.put,
            url,
            json={"messages": event_contents},
        )
        async with resp:
            status, resp_body = await check_response(resp)


class Storage(typing.Protocol):
    """Protocol for classes that persist data for the client"""

    def __getitem__(self, key: str) -> typing.Any:
        ...  # pragma: no cover

    def __setitem__(self, key: str, value: typing.Any) -> None:
        ...  # pragma: no cover

    def __delitem__(self, key: str) -> None:
        ...  # pragma: no cover

    def __contains__(self, key: str) -> bool:
        ...  # pragma: no cover

    def get(self, key: str, default: typing.Any = None) -> typing.Any:
        ...  # pragma: no cover

    def clear(self) -> None:
        ...  # pragma: no cover


class ClientClosed:
    """A message indicating that the client is closed"""

    def __eq__(self, other):
        return other.__class__ == ClientClosed


class SyncFailed(typing.NamedTuple):
    """A message indicating that a sync call has failed"""

    delay: int


SyncFailed.delay.__doc__ = "The amount of time that the loop will wait before retrying"


class SyncResumed:
    """A message indicating that the sync loop has resumed after a previous failure"""

    def __eq__(self, other):
        return other.__class__ == SyncResumed


class RoomStateUpdates(typing.NamedTuple):
    """A message giving state updates for a room from sync"""

    room_id: str
    events: list[events.StateEvent]

    @staticmethod
    def create_from_sync(room_id: str, state_updates: dict) -> "RoomStateUpdates":
        state_events = [
            events.StateEvent(room_id=room_id, **e)
            for e in state_updates.get("events", [])
        ]
        return RoomStateUpdates(room_id, state_events)


RoomStateUpdates.room_id.__doc__ = "The room ID"
RoomStateUpdates.events.__doc__ = "The state events"


class RoomTimelineUpdates(typing.NamedTuple):
    """A message giving timeline for a room from sync"""

    room_id: str
    events: list[events.RoomEvent]
    limited: bool
    prev_batch: typing.Optional[str]

    @staticmethod
    def create_from_sync(room_id: str, timeline_updates: dict) -> "RoomTimelineUpdates":
        timeline_events = [
            (
                events.StateEvent(room_id=room_id, **e)
                if "state_key" in e
                else events.RoomEvent(room_id=room_id, **e)
            )
            for e in timeline_updates.get("events", [])
        ]
        return RoomTimelineUpdates(
            room_id,
            timeline_events,
            timeline_updates.get("limited", False),
            timeline_updates.get("prev_batch", None),
        )


RoomTimelineUpdates.room_id.__doc__ = "The room ID"
RoomTimelineUpdates.events.__doc__ = "The state events"
RoomTimelineUpdates.limited.__doc__ = "Whether the timeline update was limited"
RoomTimelineUpdates.prev_batch.__doc__ = "The token to retrieve previous messages"


class LeftRoom(typing.NamedTuple):
    """A message indicating that the user has left a room"""

    room_id: str


LeftRoom.room_id.__doc__ = "The ID of the room that the user left"


class OneTimeKeysCount(typing.NamedTuple):
    """A message indicating the one-time keys count from the sync"""

    otk_count: dict[str, int]
    has_to_device: bool


OneTimeKeysCount.otk_count.__doc__ = (
    "Dict mapping algorithm name to one-time keys count"
)
OneTimeKeysCount.has_to_device.__doc__ = (
    "Whether any to-device messages were in the sync"
)


class DeviceChanges(typing.NamedTuple):
    """A message indicating that user's devices have changed"""

    changed: list[str]
    left: list[str]


DeviceChanges.changed.__doc__ = "Users whose devices have changed"
DeviceChanges.left.__doc__ = "Users who no longer share a room"


class ToDeviceEvents(typing.NamedTuple):
    """A message indicating that to-device events have been received"""

    events: list[events.Event]


ToDeviceEvents.events.__doc__ = "The to-device events"


async def check_response(
    resp: aiohttp.ClientResponse,
) -> tuple[int, dict[str, typing.Any]]:
    """Checks whether an HTTP response is an error.

    If successful, returns the HTTP code and the response body.  Raises and
    exception on error.
    """
    try:
        if resp.status < 200 or resp.status >= 400:
            result = await resp.json()
            if "errcode" not in result:
                raise error.NotMatrixServerError()
            raise error.MatrixError(resp.status, result)
        else:
            return (resp.status, await resp.json())
    except error.MatrixError:
        raise
    except:
        raise error.NotMatrixServerError()


async def retry(limit: int, req_func, *args, **kwargs) -> aiohttp.ClientResponse:
    """Retry a request until a time limit is reached.

    The request will be retried if a non-Matrix response is received, or if
    the request was rate limited.

    Note that this will not apply a timeout to the actual request: if the
    server responds slowly, then it may exceed the retry time limit.  To limit
    the time taken by the request, a `timeout` argument should be passed to
    the request function.

    Arguments:

    ``limit``:
        the time limit, in milliseconds
    ``req_func``:
        the function to call to make the request.  e.g. to make a ``GET``
        request, this could be ``session.get``, where ``session`` is an
        ``aiohttp.ClientSession``.
    ``*args, **kwargs``:
        the arguments to pass to ``req_func``.
    """
    end_time = time.monotonic_ns() + limit * 1_000_000
    backoff = 2

    while True:
        resp = await req_func(*args, **kwargs)
        # FIXME: handle no response (aiohttp.ClientConnectionError)
        if resp.status < 400:
            # not an error response, so return
            return resp

        try:
            # try to parse the error
            result = await resp.json()
        except:
            if time.monotonic_ns() > end_time:
                return resp

            await resp.release()
            # doesn't look like a Matrix server -- exponential backoff
            # but if the backoff would take us past our limit, wait until our
            # time limit and make one last attempt
            delay = min(backoff, (end_time - time.monotonic_ns()) / 1_000_000)
            await asyncio.sleep(delay)
            backoff = backoff * 2
            continue

        if time.monotonic_ns() > end_time:
            return resp
        elif (
            resp.status == 429  # Too Many Requests
            and "Retry-After" in resp.headers
            and re.match(r"^[\d]+$", resp.headers["Retry-After"])
        ):
            await resp.release()
            delay = int(resp.headers["Retry-After"])
            # we're rate limited -- wait for the requested amount
            # in this case, if the delay would take us past our limit, there's
            # no point in trying again because the server says it will still be
            # rejected, so just return
            if time.monotonic_ns() + delay * 1_000_000_000 > end_time:
                return resp
            await asyncio.sleep(delay)
            continue
        elif (
            "errcode" in result
            and result["errcode"] == "M_LIMIT_EXCEEDED"
            and type(result.get("retry_after_ms")) == int
        ):
            await resp.release()
            delay_ms = result["retry_after_ms"]
            if time.monotonic_ns() + delay_ms * 1_000_000 > end_time:
                return resp
            await asyncio.sleep(math.ceil(delay_ms / 1_000))
            continue
        else:
            # some other error, so return and let the application deal with it
            return resp


async def discover(server_name: str) -> typing.Optional[tuple[str, dict, dict]]:
    """Perform discovery on the given server name.

    On success, returns a tuple consisting of homeserver's base client-server
    URL (the homeserver's base URL with ``_matrix/client`` appended), the full
    contents of the JSON object returned from the ``.well-known`` file, and the
    full contents of the server's ``/_matrix/client/versions`` response.  On
    failure, returns ``None``.
    """

    # strip off port number
    port_match = re.match(r"^(.*):\d+$", server_name)
    if port_match:
        server_name = port_match.group(1)

    # FIXME: ensure server_name only has valid characters

    try:
        discovery_url = f"https://{server_name}/.well-known/matrix/client"
        async with aiohttp.ClientSession() as session:
            async with session.get(discovery_url) as discovery_resp:
                assert discovery_resp.status == 200
                result = await discovery_resp.json(
                    content_type=None
                )  # ignore the content type

                # check discovered URL
                base_url = urlparse(result["m.homeserver"]["base_url"])
                if base_url.scheme != "http" and base_url.scheme != "https":
                    return None
                # ensure nonempty path ends with a "/" so that URL joining works
                if base_url.path != "" and not base_url.path.endswith("/"):
                    base_url.path += "/"

                base_client_url = urljoin(base_url.geturl(), "_matrix/client/")

                versions_url = urljoin(base_client_url, "versions")
                async with session.get(versions_url) as versions_resp:
                    code, versions = await check_response(versions_resp)
                    assert schema.is_valid(versions, {"versions": list[str]})

                return (base_client_url, result, versions)
    except Exception:
        return None
