# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Classes that represent Matrix events"""

import typing

from . import schema


class Event:
    """Base class for Matrix events"""

    def __init__(
        self,
        type: str,
        sender: str,
        content: dict[str, typing.Any],
        unsigned: typing.Optional[dict[str, typing.Any]] = None,
        **kwargs,  # discard any unknown arguments
    ):
        self.type = type
        self.sender = sender
        self.content = content
        self.unsigned = unsigned if unsigned != None else {}

    @staticmethod
    def schema():
        return {
            "type": str,
            "sender": str,
            "content": dict[str, typing.Any],
            "unsigned": schema.Optional(
                {
                    "age": schema.Optional(int),
                    "prev_content": schema.Optional(dict[str, typing.Any]),
                }
            ),
        }

    @classmethod
    def is_valid(cls, data):
        # since this is a classmethod, it will call the correct `schema`
        # function for the class that this was called from
        return schema.is_valid(data, cls.schema())

    @classmethod
    def _eq_helper(cls, self, other: typing.Any) -> bool:
        return (
            self.__class__ == other.__class__
            and self.type == other.type
            and self.sender == other.sender
            and self.content == other.content
            and self.unsigned == other.unsigned
        )

    def __eq__(self, other):
        return self.__class__._eq_helper(self, other)

    def to_dict(self) -> dict:
        """Return a dict representation of the event, e.g. for serialization"""
        return {
            "type": self.type,
            "sender": self.sender,
            "content": self.content,
            "unsigned": self.unsigned,
        }


class RoomEvent(Event):
    """Represents an event sent in a room"""

    def __init__(self, event_id: str, room_id: str, origin_server_ts: int, **kwargs):
        Event.__init__(self, **kwargs)
        self.event_id = event_id
        self.room_id = room_id
        self.origin_server_ts = origin_server_ts

    @staticmethod
    def schema():
        return schema.Intersection(
            Event.schema(),
            {
                "event_id": str,
                "room_id": str,
                "origin_server_ts": int,
            },
        )

    @staticmethod
    def schema_without_room_id():
        return schema.Intersection(
            Event.schema(),
            {
                "event_id": str,
                "origin_server_ts": int,
            },
        )

    @classmethod
    def _eq_helper(cls, self, other: typing.Any) -> bool:
        return (
            Event._eq_helper(self, other)
            and self.event_id == other.event_id
            and self.room_id == other.room_id
            and self.origin_server_ts == other.origin_server_ts
        )

    def to_dict(self) -> dict:
        """Return a dict representation of the event, e.g. for serialization"""
        ret = super().to_dict()
        ret.update(
            {
                "event_id": self.event_id,
                "room_id": self.room_id,
                "origin_server_ts": self.origin_server_ts,
            }
        )
        return ret


class StateEvent(RoomEvent):
    """Represents a state room"""

    def __init__(self, state_key: str, **kwargs):
        RoomEvent.__init__(self, **kwargs)
        self.state_key = state_key

    @staticmethod
    def schema():
        return schema.Intersection(
            RoomEvent.schema(),
            {
                "state_key": str,
            },
        )

    @staticmethod
    def schema_without_room_id():
        return schema.Intersection(
            RoomEvent.schema_without_room_id(),
            {
                "state_key": str,
            },
        )

    @classmethod
    def _eq_helper(cls, self, other: typing.Any) -> bool:
        return RoomEvent._eq_helper(self, other) and self.state_key == other.state_key

    def to_dict(self) -> dict:
        """Return a dict representation of the event, e.g. for serialization"""
        ret = super().to_dict()
        ret.update(
            {
                "state_key": self.state_key,
            }
        )
        return ret
