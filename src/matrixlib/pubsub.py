# Copyright Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

"""Implementation of the publish-subscribe pattern"""

import inspect
import logging
import sys
import typing


class Publisher:
    """Publish messages that can be subscribed to.

    Subscribers indicate which message types they are interested in by
    specifying the class of the messages."""

    def __init__(self):
        self.subscribers = []
        self.id = 0

    def subscribe(
        self,
        message_type: typing.Union[type, typing.Tuple[type, ...]],
        subscriber: typing.Callable[
            [typing.Any], typing.Union[None, typing.Awaitable[None]]
        ],
    ) -> int:
        """Subscribe to a message type

        Returns an ID that can be used to unsubscribe
        """
        self.id += 1
        self.subscribers.append((message_type, subscriber, self.id))
        return self.id

    def unsubscribe(self, id: int) -> bool:
        """Unsubscribe from a message type

        Returns whether or not the subscriber was found.
        """
        idx = 0
        for _, _, subscriber_id in self.subscribers:
            if subscriber_id == id:
                del self.subscribers[idx]
                return True
            idx += 1
        return False

    async def publish(self, message: typing.Any) -> None:
        """Publish a message to the subscribers"""
        for message_type, subscriber, _ in self.subscribers:
            if issubclass(message.__class__, message_type):
                try:
                    res = subscriber(message)
                    if inspect.isawaitable(res):
                        await res
                except:
                    # we don't want a misbehaving subscriber to bring down the
                    # whole system, but we need to record the error somehow
                    e = sys.exc_info()[1]
                    logging.error("Subscriber threw an exception: %s", e)
